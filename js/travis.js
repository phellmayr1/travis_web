require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (Buffer){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['superagent'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('superagent'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.ApiClient = factory(root.superagent);
  }
}(this, function(superagent) {
  'use strict';

  /**
   * @module ApiClient
   * @version 0.0.1
   */

  /**
   * Manages low level client-server communications, parameter marshalling, etc. There should not be any need for an
   * application to use this class directly - the *Api and model classes provide the public API for the service. The
   * contents of this file should be regarded as internal but are documented for completeness.
   * @alias module:ApiClient
   * @class
   */
  var exports = function() {
    /**
     * The base URL against which to resolve every API call's (relative) path.
     * @type {String}
     * @default http://beta4travis-sfraungruber.rhcloud.com/
     */
    this.basePath = 'http://beta4travis-sfraungruber.rhcloud.com/'.replace(/\/+$/, '');

    /**
     * The authentication methods to be included for all API calls.
     * @type {Array.<String>}
     */
    this.authentications = {
    };
    /**
     * The default HTTP headers to be included for all API calls.
     * @type {Array.<String>}
     * @default {}
     */
    this.defaultHeaders = {};

    /**
     * The default HTTP timeout for all API calls.
     * @type {Number}
     * @default 60000
     */
    this.timeout = 60000;
  };

  /**
   * Returns a string representation for an actual parameter.
   * @param param The actual parameter.
   * @returns {String} The string representation of <code>param</code>.
   */
  exports.prototype.paramToString = function(param) {
    if (param == undefined || param == null) {
      return '';
    }
    if (param instanceof Date) {
      return param.toJSON();
    }
    return param.toString();
  };

  /**
   * Builds full URL by appending the given path to the base URL and replacing path parameter place-holders with parameter values.
   * NOTE: query parameters are not handled here.
   * @param {String} path The path to append to the base URL.
   * @param {Object} pathParams The parameter values to append.
   * @returns {String} The encoded path with parameter values substituted.
   */
  exports.prototype.buildUrl = function(path, pathParams) {
    if (!path.match(/^\//)) {
      path = '/' + path;
    }
    var url = this.basePath + path;
    var _this = this;
    url = url.replace(/\{([\w-]+)\}/g, function(fullMatch, key) {
      var value;
      if (pathParams.hasOwnProperty(key)) {
        value = _this.paramToString(pathParams[key]);
      } else {
        value = fullMatch;
      }
      return encodeURIComponent(value);
    });
    return url;
  };

  /**
   * Checks whether the given content type represents JSON.<br>
   * JSON content type examples:<br>
   * <ul>
   * <li>application/json</li>
   * <li>application/json; charset=UTF8</li>
   * <li>APPLICATION/JSON</li>
   * </ul>
   * @param {String} contentType The MIME content type to check.
   * @returns {Boolean} <code>true</code> if <code>contentType</code> represents JSON, otherwise <code>false</code>.
   */
  exports.prototype.isJsonMime = function(contentType) {
    return Boolean(contentType != null && contentType.match(/^application\/json(;.*)?$/i));
  };

  /**
   * Chooses a content type from the given array, with JSON preferred; i.e. return JSON if included, otherwise return the first.
   * @param {Array.<String>} contentTypes
   * @returns {String} The chosen content type, preferring JSON.
   */
  exports.prototype.jsonPreferredMime = function(contentTypes) {
    for (var i = 0; i < contentTypes.length; i++) {
      if (this.isJsonMime(contentTypes[i])) {
        return contentTypes[i];
      }
    }
    return contentTypes[0];
  };

  /**
   * Checks whether the given parameter value represents file-like content.
   * @param param The parameter to check.
   * @returns {Boolean} <code>true</code> if <code>param</code> represents a file. 
   */
  exports.prototype.isFileParam = function(param) {
    // fs.ReadStream in Node.js (but not in runtime like browserify)
    if (typeof window === 'undefined' &&
        typeof require === 'function' &&
        require('fs') &&
        param instanceof require('fs').ReadStream) {
      return true;
    }
    // Buffer in Node.js
    if (typeof Buffer === 'function' && param instanceof Buffer) {
      return true;
    }
    // Blob in browser
    if (typeof Blob === 'function' && param instanceof Blob) {
      return true;
    }
    // File in browser (it seems File object is also instance of Blob, but keep this for safe)
    if (typeof File === 'function' && param instanceof File) {
      return true;
    }
    return false;
  };

  /**
   * Normalizes parameter values:
   * <ul>
   * <li>remove nils</li>
   * <li>keep files and arrays</li>
   * <li>format to string with `paramToString` for other cases</li>
   * </ul>
   * @param {Object.<String, Object>} params The parameters as object properties.
   * @returns {Object.<String, Object>} normalized parameters.
   */
  exports.prototype.normalizeParams = function(params) {
    var newParams = {};
    for (var key in params) {
      if (params.hasOwnProperty(key) && params[key] != undefined && params[key] != null) {
        var value = params[key];
        if (this.isFileParam(value) || Array.isArray(value)) {
          newParams[key] = value;
        } else {
          newParams[key] = this.paramToString(value);
        }
      }
    }
    return newParams;
  };

  /**
   * Enumeration of collection format separator strategies.
   * @enum {String} 
   * @readonly
   */
  exports.CollectionFormatEnum = {
    /**
     * Comma-separated values. Value: <code>csv</code>
     * @const
     */
    CSV: ',',
    /**
     * Space-separated values. Value: <code>ssv</code>
     * @const
     */
    SSV: ' ',
    /**
     * Tab-separated values. Value: <code>tsv</code>
     * @const
     */
    TSV: '\t',
    /**
     * Pipe(|)-separated values. Value: <code>pipes</code>
     * @const
     */
    PIPES: '|',
    /**
     * Native array. Value: <code>multi</code>
     * @const
     */
    MULTI: 'multi'
  };

  /**
   * Builds a string representation of an array-type actual parameter, according to the given collection format.
   * @param {Array} param An array parameter.
   * @param {module:ApiClient.CollectionFormatEnum} collectionFormat The array element separator strategy.
   * @returns {String|Array} A string representation of the supplied collection, using the specified delimiter. Returns
   * <code>param</code> as is if <code>collectionFormat</code> is <code>multi</code>.
   */
  exports.prototype.buildCollectionParam = function buildCollectionParam(param, collectionFormat) {
    if (param == null) {
      return null;
    }
    switch (collectionFormat) {
      case 'csv':
        return param.map(this.paramToString).join(',');
      case 'ssv':
        return param.map(this.paramToString).join(' ');
      case 'tsv':
        return param.map(this.paramToString).join('\t');
      case 'pipes':
        return param.map(this.paramToString).join('|');
      case 'multi':
        // return the array directly as SuperAgent will handle it as expected
        return param.map(this.paramToString);
      default:
        throw new Error('Unknown collection format: ' + collectionFormat);
    }
  };

  /**
   * Applies authentication headers to the request.
   * @param {Object} request The request object created by a <code>superagent()</code> call.
   * @param {Array.<String>} authNames An array of authentication method names.
   */
  exports.prototype.applyAuthToRequest = function(request, authNames) {
    var _this = this;
    authNames.forEach(function(authName) {
      var auth = _this.authentications[authName];
      switch (auth.type) {
        case 'basic':
          if (auth.username || auth.password) {
            request.auth(auth.username || '', auth.password || '');
          }
          break;
        case 'apiKey':
          if (auth.apiKey) {
            var data = {};
            if (auth.apiKeyPrefix) {
              data[auth.name] = auth.apiKeyPrefix + ' ' + auth.apiKey;
            } else {
              data[auth.name] = auth.apiKey;
            }
            if (auth['in'] === 'header') {
              request.set(data);
            } else {
              request.query(data);
            }
          }
          break;
        case 'oauth2':
          if (auth.accessToken) {
            request.set({'Authorization': 'Bearer ' + auth.accessToken});
          }
          break;
        default:
          throw new Error('Unknown authentication type: ' + auth.type);
      }
    });
  };

  /**
   * Deserializes an HTTP response body into a value of the specified type.
   * @param {Object} response A SuperAgent response object.
   * @param {(String|Array.<String>|Object.<String, Object>|Function)} returnType The type to return. Pass a string for simple types
   * or the constructor function for a complex type. Pass an array containing the type name to return an array of that type. To
   * return an object, pass an object with one property whose name is the key type and whose value is the corresponding value type:
   * all properties on <code>data<code> will be converted to this type.
   * @returns A value of the specified type.
   */
  exports.prototype.deserialize = function deserialize(response, returnType) {
    if (response == null || returnType == null) {
      return null;
    }
    // Rely on SuperAgent for parsing response body.
    // See http://visionmedia.github.io/superagent/#parsing-response-bodies
    var data = response.body;
    if (data == null) {
      // SuperAgent does not always produce a body; use the unparsed response as a fallback
      data = response.text;
    }
    return exports.convertToType(data, returnType);
  };

  /**
   * Callback function to receive the result of the operation.
   * @callback module:ApiClient~callApiCallback
   * @param {String} error Error message, if any.
   * @param data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Invokes the REST service using the supplied settings and parameters.
   * @param {String} path The base URL to invoke.
   * @param {String} httpMethod The HTTP method to use.
   * @param {Object.<String, String>} pathParams A map of path parameters and their values.
   * @param {Object.<String, Object>} queryParams A map of query parameters and their values.
   * @param {Object.<String, Object>} headerParams A map of header parameters and their values.
   * @param {Object.<String, Object>} formParams A map of form parameters and their values.
   * @param {Object} bodyParam The value to pass as the request body.
   * @param {Array.<String>} authNames An array of authentication type names.
   * @param {Array.<String>} contentTypes An array of request MIME types.
   * @param {Array.<String>} accepts An array of acceptable response MIME types.
   * @param {(String|Array|ObjectFunction)} returnType The required type to return; can be a string for simple types or the
   * constructor for a complex type.
   * @param {module:ApiClient~callApiCallback} callback The callback function.
   * @returns {Object} The SuperAgent request object.
   */
  exports.prototype.callApi = function callApi(path, httpMethod, pathParams,
      queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, accepts,
      returnType, callback) {

    var _this = this;
    var url = this.buildUrl(path, pathParams);
    var request = superagent(httpMethod, url);

    // apply authentications
    this.applyAuthToRequest(request, authNames);

    // set query parameters
    request.query(this.normalizeParams(queryParams));

    // set header parameters
    request.set(this.defaultHeaders).set(this.normalizeParams(headerParams));

    // set request timeout
    request.timeout(this.timeout);

    var contentType = this.jsonPreferredMime(contentTypes);
    if (contentType) {
      request.type(contentType);
    } else if (!request.header['Content-Type']) {
      request.type('application/json');
    }

    if (contentType === 'application/x-www-form-urlencoded') {
      request.send(this.normalizeParams(formParams));
    } else if (contentType == 'multipart/form-data') {
      var _formParams = this.normalizeParams(formParams);
      for (var key in _formParams) {
        if (_formParams.hasOwnProperty(key)) {
          if (this.isFileParam(_formParams[key])) {
            // file field
            request.attach(key, _formParams[key]);
          } else {
            request.field(key, _formParams[key]);
          }
        }
      }
    } else if (bodyParam) {
      request.send(bodyParam);
    }

    var accept = this.jsonPreferredMime(accepts);
    if (accept) {
      request.accept(accept);
    }


    request.end(function(error, response) {
      if (callback) {
        var data = null;
        if (!error) {
          data = _this.deserialize(response, returnType);
        }
        callback(error, data, response);
      }
    });

    return request;
  };

  /**
   * Parses an ISO-8601 string representation of a date value.
   * @param {String} str The date value as a string.
   * @returns {Date} The parsed date object.
   */
  exports.parseDate = function(str) {
    return new Date(str.replace(/T/i, ' '));
  };

  /**
   * Converts a value to the specified type.
   * @param {(String|Object)} data The data to convert, as a string or object.
   * @param {(String|Array.<String>|Object.<String, Object>|Function)} type The type to return. Pass a string for simple types
   * or the constructor function for a complex type. Pass an array containing the type name to return an array of that type. To
   * return an object, pass an object with one property whose name is the key type and whose value is the corresponding value type:
   * all properties on <code>data<code> will be converted to this type.
   * @returns An instance of the specified type.
   */
  exports.convertToType = function(data, type) {
    switch (type) {
      case 'Boolean':
        return Boolean(data);
      case 'Integer':
        return parseInt(data, 10);
      case 'Number':
        return parseFloat(data);
      case 'String':
        return String(data);
      case 'Date':
        return this.parseDate(String(data));
      default:
        if (type === Object) {
          // generic object, return directly
          return data;
        } else if (typeof type === 'function') {
          // for model type like: User
          return type.constructFromObject(data);
        } else if (Array.isArray(type)) {
          // for array type like: ['String']
          var itemType = type[0];
          return data.map(function(item) {
            return exports.convertToType(item, itemType);
          });
        } else if (typeof type === 'object') {
          // for plain object type like: {'String': 'Integer'}
          var keyType, valueType;
          for (var k in type) {
            if (type.hasOwnProperty(k)) {
              keyType = k;
              valueType = type[k];
              break;
            }
          }
          var result = {};
          for (var k in data) {
            if (data.hasOwnProperty(k)) {
              var key = exports.convertToType(k, keyType);
              var value = exports.convertToType(data[k], valueType);
              result[key] = value;
            }
          }
          return result;
        } else {
          // for unknown type, return the data directly
          return data;
        }
    }
  };

  /**
   * Constructs a new map or array model from REST data.
   * @param data {Object|Array} The REST data.
   * @param obj {Object|Array} The target object or array.
   */
  exports.constructFromObject = function(data, obj, itemType) {
    if (Array.isArray(data)) {
      for (var i = 0; i < data.length; i++) {
        if (data.hasOwnProperty(i))
          obj[i] = exports.convertToType(data[i], itemType);
      }
    } else {
      for (var k in data) {
        if (data.hasOwnProperty(k))
          result[k] = exports.convertToType(data[k], itemType);
      }
    }
  };

  /**
   * The default API client implementation.
   * @type {module:ApiClient}
   */
  exports.instance = new exports();

  return exports;
}));

}).call(this,require("buffer").Buffer)
},{"buffer":58,"fs":57,"superagent":51}],2:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LoginRequestDTO', 'model/LoginResponseDTO', 'model/ErrorResponse'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/LoginRequestDTO'), require('../model/LoginResponseDTO'), require('../model/ErrorResponse'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.AuthenticationApi = factory(root.Travis.ApiClient, root.Travis.LoginRequestDTO, root.Travis.LoginResponseDTO, root.Travis.ErrorResponse);
  }
}(this, function(ApiClient, LoginRequestDTO, LoginResponseDTO, ErrorResponse) {
  'use strict';

  /**
   * Authentication service.
   * @module api/AuthenticationApi
   * @version 0.0.1
   */

  /**
   * Constructs a new AuthenticationApi. 
   * @alias module:api/AuthenticationApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the login operation.
     * @callback module:api/AuthenticationApi~loginCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LoginResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Login the user and request the sessionId
     * @param {module:model/LoginRequestDTO} loginRequestDTO loginType - facebook, googleplus, ...
     * @param {module:api/AuthenticationApi~loginCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/LoginResponseDTO}
     */
    this.login = function(loginRequestDTO, callback) {
      var postBody = loginRequestDTO;

      // verify the required parameter 'loginRequestDTO' is set
      if (loginRequestDTO == undefined || loginRequestDTO == null) {
        throw "Missing the required parameter 'loginRequestDTO' when calling login";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = LoginResponseDTO;

      return this.apiClient.callApi(
        '/user/login', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the logout operation.
     * @callback module:api/AuthenticationApi~logoutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Logout the user and set sessionId to NULL
     * @param {String} sessionId sessionId from the user
     * @param {module:api/AuthenticationApi~logoutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.logout = function(sessionId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling logout";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/user/logout', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

},{"../ApiClient":1,"../model/ErrorResponse":23,"../model/LoginRequestDTO":28,"../model/LoginResponseDTO":29}],3:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ErrorResponse', 'model/DiaryCommentResponseDTO', 'model/DiaryElementsResponseListDTO', 'model/DiaryCommentRequestDTO', 'model/DiaryCoordinatesRequestDTO', 'model/DiaryRequestDTO', 'model/DiaryResponseDTO', 'model/DiaryTextRequestDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorResponse'), require('../model/DiaryCommentResponseDTO'), require('../model/DiaryElementsResponseListDTO'), require('../model/DiaryCommentRequestDTO'), require('../model/DiaryCoordinatesRequestDTO'), require('../model/DiaryRequestDTO'), require('../model/DiaryResponseDTO'), require('../model/DiaryTextRequestDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryApi = factory(root.Travis.ApiClient, root.Travis.ErrorResponse, root.Travis.DiaryCommentResponseDTO, root.Travis.DiaryElementsResponseListDTO, root.Travis.DiaryCommentRequestDTO, root.Travis.DiaryCoordinatesRequestDTO, root.Travis.DiaryRequestDTO, root.Travis.DiaryResponseDTO, root.Travis.DiaryTextRequestDTO);
  }
}(this, function(ApiClient, ErrorResponse, DiaryCommentResponseDTO, DiaryElementsResponseListDTO, DiaryCommentRequestDTO, DiaryCoordinatesRequestDTO, DiaryRequestDTO, DiaryResponseDTO, DiaryTextRequestDTO) {
  'use strict';

  /**
   * Diary service.
   * @module api/DiaryApi
   * @version 0.0.1
   */

  /**
   * Constructs a new DiaryApi. 
   * @alias module:api/DiaryApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the deleteLike operation.
     * @callback module:api/DiaryApi~deleteLikeCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * delete - like a diary Entry
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} diaryEntryId The id of the diary entry
     * @param {module:api/DiaryApi~deleteLikeCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.deleteLike = function(sessionId, diaryEntryId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling deleteLike";
      }

      // verify the required parameter 'diaryEntryId' is set
      if (diaryEntryId == undefined || diaryEntryId == null) {
        throw "Missing the required parameter 'diaryEntryId' when calling deleteLike";
      }


      var pathParams = {
        'diaryEntryId': diaryEntryId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/diary/{diaryEntryId}/like', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getComments operation.
     * @callback module:api/DiaryApi~getCommentsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/DiaryCommentResponseDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * get all comments from a diary entry
     * @param {Integer} diaryEntryId The id of the diary entry
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 10)
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/DiaryApi~getCommentsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/DiaryCommentResponseDTO>}
     */
    this.getComments = function(diaryEntryId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'diaryEntryId' is set
      if (diaryEntryId == undefined || diaryEntryId == null) {
        throw "Missing the required parameter 'diaryEntryId' when calling getComments";
      }


      var pathParams = {
        'diaryEntryId': diaryEntryId
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize'],
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [DiaryCommentResponseDTO];

      return this.apiClient.callApi(
        '/diary/comment/{diaryEntryId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getDiaryElements operation.
     * @callback module:api/DiaryApi~getDiaryElementsCallback
     * @param {String} error Error message, if any.
     * @param {module:model/DiaryElementsResponseListDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * get diaryElements for diaryEntry
     * @param {Integer} diaryEntryId The diaryEntryId of diaryEntry
     * @param {module:api/DiaryApi~getDiaryElementsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/DiaryElementsResponseListDTO}
     */
    this.getDiaryElements = function(diaryEntryId, callback) {
      var postBody = null;

      // verify the required parameter 'diaryEntryId' is set
      if (diaryEntryId == undefined || diaryEntryId == null) {
        throw "Missing the required parameter 'diaryEntryId' when calling getDiaryElements";
      }


      var pathParams = {
        'diaryEntryId': diaryEntryId
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = DiaryElementsResponseListDTO;

      return this.apiClient.callApi(
        '/diary/elements/{diaryEntryId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postComment operation.
     * @callback module:api/DiaryApi~postCommentCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * add a comment to a diary entry
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} diaryEntryId The id of the diary entry
     * @param {module:model/DiaryCommentRequestDTO} diaryCommentRequestDTO DiaryEntry Comment
     * @param {module:api/DiaryApi~postCommentCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postComment = function(sessionId, diaryEntryId, diaryCommentRequestDTO, callback) {
      var postBody = diaryCommentRequestDTO;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postComment";
      }

      // verify the required parameter 'diaryEntryId' is set
      if (diaryEntryId == undefined || diaryEntryId == null) {
        throw "Missing the required parameter 'diaryEntryId' when calling postComment";
      }

      // verify the required parameter 'diaryCommentRequestDTO' is set
      if (diaryCommentRequestDTO == undefined || diaryCommentRequestDTO == null) {
        throw "Missing the required parameter 'diaryCommentRequestDTO' when calling postComment";
      }


      var pathParams = {
        'diaryEntryId': diaryEntryId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/diary/comment/{diaryEntryId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postCoordinatesElement operation.
     * @callback module:api/DiaryApi~postCoordinatesElementCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * insert a diary entry text
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} diaryEntryId The id of the diary entry
     * @param {module:model/DiaryCoordinatesRequestDTO} diaryCoordinatesRequestDTO location for diary entry
     * @param {module:api/DiaryApi~postCoordinatesElementCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postCoordinatesElement = function(sessionId, diaryEntryId, diaryCoordinatesRequestDTO, callback) {
      var postBody = diaryCoordinatesRequestDTO;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postCoordinatesElement";
      }

      // verify the required parameter 'diaryEntryId' is set
      if (diaryEntryId == undefined || diaryEntryId == null) {
        throw "Missing the required parameter 'diaryEntryId' when calling postCoordinatesElement";
      }

      // verify the required parameter 'diaryCoordinatesRequestDTO' is set
      if (diaryCoordinatesRequestDTO == undefined || diaryCoordinatesRequestDTO == null) {
        throw "Missing the required parameter 'diaryCoordinatesRequestDTO' when calling postCoordinatesElement";
      }


      var pathParams = {
        'diaryEntryId': diaryEntryId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/diary/coordinatesElement/{diaryEntryId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postDiaryEntry operation.
     * @callback module:api/DiaryApi~postDiaryEntryCallback
     * @param {String} error Error message, if any.
     * @param {module:model/DiaryResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * insert a diary entry
     * @param {String} sessionId The sessionId of the user
     * @param {module:model/DiaryRequestDTO} diaryRequestDTO Diary Entry
     * @param {module:api/DiaryApi~postDiaryEntryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/DiaryResponseDTO}
     */
    this.postDiaryEntry = function(sessionId, diaryRequestDTO, callback) {
      var postBody = diaryRequestDTO;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postDiaryEntry";
      }

      // verify the required parameter 'diaryRequestDTO' is set
      if (diaryRequestDTO == undefined || diaryRequestDTO == null) {
        throw "Missing the required parameter 'diaryRequestDTO' when calling postDiaryEntry";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = DiaryResponseDTO;

      return this.apiClient.callApi(
        '/diary', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postLike operation.
     * @callback module:api/DiaryApi~postLikeCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * like a diary Entry
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} diaryEntryId The id of the diary entry
     * @param {module:api/DiaryApi~postLikeCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postLike = function(sessionId, diaryEntryId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postLike";
      }

      // verify the required parameter 'diaryEntryId' is set
      if (diaryEntryId == undefined || diaryEntryId == null) {
        throw "Missing the required parameter 'diaryEntryId' when calling postLike";
      }


      var pathParams = {
        'diaryEntryId': diaryEntryId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/diary/{diaryEntryId}/like', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postPictureElement operation.
     * @callback module:api/DiaryApi~postPictureElementCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * insert a diary entry picture
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} diaryEntryId The id of the diary entry
     * @param {Integer} sort index for sort (0 .. n, 0 &#x3D; element on top in list)
     * @param {File} picture Picture
     * @param {module:api/DiaryApi~postPictureElementCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postPictureElement = function(sessionId, diaryEntryId, sort, picture, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postPictureElement";
      }

      // verify the required parameter 'diaryEntryId' is set
      if (diaryEntryId == undefined || diaryEntryId == null) {
        throw "Missing the required parameter 'diaryEntryId' when calling postPictureElement";
      }

      // verify the required parameter 'sort' is set
      if (sort == undefined || sort == null) {
        throw "Missing the required parameter 'sort' when calling postPictureElement";
      }

      // verify the required parameter 'picture' is set
      if (picture == undefined || picture == null) {
        throw "Missing the required parameter 'picture' when calling postPictureElement";
      }


      var pathParams = {
        'diaryEntryId': diaryEntryId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
        'sort': sort,
        'picture': picture
      };

      var authNames = [];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/diary/pictureElement/{diaryEntryId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postTextElement operation.
     * @callback module:api/DiaryApi~postTextElementCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * insert a diary entry text
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} diaryEntryId The id of the diary entry
     * @param {module:model/DiaryTextRequestDTO} diaryTextRequestDTO text for diary entry
     * @param {module:api/DiaryApi~postTextElementCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postTextElement = function(sessionId, diaryEntryId, diaryTextRequestDTO, callback) {
      var postBody = diaryTextRequestDTO;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postTextElement";
      }

      // verify the required parameter 'diaryEntryId' is set
      if (diaryEntryId == undefined || diaryEntryId == null) {
        throw "Missing the required parameter 'diaryEntryId' when calling postTextElement";
      }

      // verify the required parameter 'diaryTextRequestDTO' is set
      if (diaryTextRequestDTO == undefined || diaryTextRequestDTO == null) {
        throw "Missing the required parameter 'diaryTextRequestDTO' when calling postTextElement";
      }


      var pathParams = {
        'diaryEntryId': diaryEntryId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/diary/textElement/{diaryEntryId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

},{"../ApiClient":1,"../model/DiaryCommentRequestDTO":12,"../model/DiaryCommentResponseDTO":13,"../model/DiaryCoordinatesRequestDTO":15,"../model/DiaryElementsResponseListDTO":16,"../model/DiaryRequestDTO":19,"../model/DiaryResponseDTO":20,"../model/DiaryTextRequestDTO":22,"../model/ErrorResponse":23}],4:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/CoordinatesResponseDTO', 'model/ErrorResponse', 'model/UserMainPageEntryDTO', 'model/LocationCoordinateDTO', 'model/LocationSearchDTO', 'model/UserDTO', 'model/LocationResponseDTO', 'model/LocationDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/CoordinatesResponseDTO'), require('../model/ErrorResponse'), require('../model/UserMainPageEntryDTO'), require('../model/LocationCoordinateDTO'), require('../model/LocationSearchDTO'), require('../model/UserDTO'), require('../model/LocationResponseDTO'), require('../model/LocationDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.LocationsApi = factory(root.Travis.ApiClient, root.Travis.CoordinatesResponseDTO, root.Travis.ErrorResponse, root.Travis.UserMainPageEntryDTO, root.Travis.LocationCoordinateDTO, root.Travis.LocationSearchDTO, root.Travis.UserDTO, root.Travis.LocationResponseDTO, root.Travis.LocationDTO);
  }
}(this, function(ApiClient, CoordinatesResponseDTO, ErrorResponse, UserMainPageEntryDTO, LocationCoordinateDTO, LocationSearchDTO, UserDTO, LocationResponseDTO, LocationDTO) {
  'use strict';

  /**
   * Locations service.
   * @module api/LocationsApi
   * @version 0.0.1
   */

  /**
   * Constructs a new LocationsApi. 
   * @alias module:api/LocationsApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getCoordinates operation.
     * @callback module:api/LocationsApi~getCoordinatesCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CoordinatesResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * request coordinates for an id
     * @param {String} coordinatesId id of the coordinates
     * @param {module:api/LocationsApi~getCoordinatesCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/CoordinatesResponseDTO}
     */
    this.getCoordinates = function(coordinatesId, callback) {
      var postBody = null;

      // verify the required parameter 'coordinatesId' is set
      if (coordinatesId == undefined || coordinatesId == null) {
        throw "Missing the required parameter 'coordinatesId' when calling getCoordinates";
      }


      var pathParams = {
        'coordinatesId': coordinatesId
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = CoordinatesResponseDTO;

      return this.apiClient.callApi(
        '/coordinates/{coordinatesId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getDiaryForLocation operation.
     * @callback module:api/LocationsApi~getDiaryForLocationCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/UserMainPageEntryDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * get diaries for a location
     * @param {Integer} locationId Id from Location
     * @param {Object} opts Optional parameters
     * @param {String} opts.sessionId The sessionID of the user
     * @param {Integer} opts.userId userId to filter
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/LocationsApi~getDiaryForLocationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/UserMainPageEntryDTO>}
     */
    this.getDiaryForLocation = function(locationId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling getDiaryForLocation";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
        'userId': opts['userId'],
        'page': opts['page'],
        'pageSize': opts['pageSize'],
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': opts['sessionId']
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [UserMainPageEntryDTO];

      return this.apiClient.callApi(
        '/location/{locationId}/diary', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getLocationById operation.
     * @callback module:api/LocationsApi~getLocationByIdCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LocationCoordinateDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * get location for locationId
     * @param {Integer} locationId Id from Location
     * @param {Object} opts Optional parameters
     * @param {String} opts.sessionId The sessionId of the user
     * @param {module:api/LocationsApi~getLocationByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/LocationCoordinateDTO}
     */
    this.getLocationById = function(locationId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling getLocationById";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': opts['sessionId']
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = LocationCoordinateDTO;

      return this.apiClient.callApi(
        '/location/{locationId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getLocationSearchDeprecated operation.
     * @callback module:api/LocationsApi~getLocationSearchDeprecatedCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LocationSearchDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * search a location for the searchText
     * @param {String} searchText Text from the searchBox
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {module:api/LocationsApi~getLocationSearchDeprecatedCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/LocationSearchDTO}
     */
    this.getLocationSearchDeprecated = function(searchText, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'searchText' is set
      if (searchText == undefined || searchText == null) {
        throw "Missing the required parameter 'searchText' when calling getLocationSearchDeprecated";
      }


      var pathParams = {
        'searchText': searchText
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = LocationSearchDTO;

      return this.apiClient.callApi(
        '/location/search/{searchText}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getUserForLocation operation.
     * @callback module:api/LocationsApi~getUserForLocationCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/UserDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * get user for a location
     * @param {Integer} locationId Id from Location
     * @param {Object} opts Optional parameters
     * @param {String} opts.sessionId The sessionId of the user
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/LocationsApi~getUserForLocationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/UserDTO>}
     */
    this.getUserForLocation = function(locationId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling getUserForLocation";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize'],
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': opts['sessionId']
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [UserDTO];

      return this.apiClient.callApi(
        '/location/{locationId}/user', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postLocation operation.
     * @callback module:api/LocationsApi~postLocationCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LocationResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * insert a location
     * @param {module:model/LocationDTO} locationDTO Profile Tag
     * @param {module:api/LocationsApi~postLocationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/LocationResponseDTO}
     */
    this.postLocation = function(locationDTO, callback) {
      var postBody = locationDTO;

      // verify the required parameter 'locationDTO' is set
      if (locationDTO == undefined || locationDTO == null) {
        throw "Missing the required parameter 'locationDTO' when calling postLocation";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = LocationResponseDTO;

      return this.apiClient.callApi(
        '/location', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

},{"../ApiClient":1,"../model/CoordinatesResponseDTO":11,"../model/ErrorResponse":23,"../model/LocationCoordinateDTO":24,"../model/LocationDTO":25,"../model/LocationResponseDTO":26,"../model/LocationSearchDTO":27,"../model/UserDTO":47,"../model/UserMainPageEntryDTO":48}],5:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ErrorResponse'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorResponse'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.PictureApi = factory(root.Travis.ApiClient, root.Travis.ErrorResponse);
  }
}(this, function(ApiClient, ErrorResponse) {
  'use strict';

  /**
   * Picture service.
   * @module api/PictureApi
   * @version 0.0.1
   */

  /**
   * Constructs a new PictureApi. 
   * @alias module:api/PictureApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getNotificationToEveryOne operation.
     * @callback module:api/PictureApi~getNotificationToEveryOneCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Send a TestNotification to everyone
     * @param {Object} opts Optional parameters
     * @param {String} opts.title Title of notification (default to test)
     * @param {String} opts.message Message-body of notification (default to test-body)
     * @param {String} opts.apiKey API Key from server (default to AIzaSyDo2TGC1LaE92YE7vp5TMhJXi0Hfaea6KU)
     * @param {module:api/PictureApi~getNotificationToEveryOneCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.getNotificationToEveryOne = function(opts, callback) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'title': opts['title'],
        'message': opts['message'],
        'apiKey': opts['apiKey']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/notification', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getPicture operation.
     * @callback module:api/PictureApi~getPictureCallback
     * @param {String} error Error message, if any.
     * @param {File} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Request a picture
     * @param {String} pictureId id from the picture
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.cattastic cats, cats everywhere (default to false)
     * @param {module:api/PictureApi~getPictureCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {File}
     */
    this.getPicture = function(pictureId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'pictureId' is set
      if (pictureId == undefined || pictureId == null) {
        throw "Missing the required parameter 'pictureId' when calling getPicture";
      }


      var pathParams = {
        'pictureId': pictureId
      };
      var queryParams = {
        'cattastic': opts['cattastic']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['image/jpeg'];
      var returnType = File;

      return this.apiClient.callApi(
        '/picture/{pictureId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

},{"../ApiClient":1,"../model/ErrorResponse":23}],6:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ErrorResponse', 'model/TripResponseDTO', 'model/TripOverviewResponseDTO', 'model/TripRequestDTO', 'model/TripActivityRequestDTO', 'model/TripPlannerRequestDTO', 'model/TripPlannerResponseDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorResponse'), require('../model/TripResponseDTO'), require('../model/TripOverviewResponseDTO'), require('../model/TripRequestDTO'), require('../model/TripActivityRequestDTO'), require('../model/TripPlannerRequestDTO'), require('../model/TripPlannerResponseDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.PlannerApi = factory(root.Travis.ApiClient, root.Travis.ErrorResponse, root.Travis.TripResponseDTO, root.Travis.TripOverviewResponseDTO, root.Travis.TripRequestDTO, root.Travis.TripActivityRequestDTO, root.Travis.TripPlannerRequestDTO, root.Travis.TripPlannerResponseDTO);
  }
}(this, function(ApiClient, ErrorResponse, TripResponseDTO, TripOverviewResponseDTO, TripRequestDTO, TripActivityRequestDTO, TripPlannerRequestDTO, TripPlannerResponseDTO) {
  'use strict';

  /**
   * Planner service.
   * @module api/PlannerApi
   * @version 0.0.1
   */

  /**
   * Constructs a new PlannerApi. 
   * @alias module:api/PlannerApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the deleteAcceptTrip operation.
     * @callback module:api/PlannerApi~deleteAcceptTripCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Left a invite for a trip
     * @param {String} sessionId The sessionID of the user
     * @param {Integer} tripId The id of the trip
     * @param {module:api/PlannerApi~deleteAcceptTripCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.deleteAcceptTrip = function(sessionId, tripId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling deleteAcceptTrip";
      }

      // verify the required parameter 'tripId' is set
      if (tripId == undefined || tripId == null) {
        throw "Missing the required parameter 'tripId' when calling deleteAcceptTrip";
      }


      var pathParams = {
        'tripId': tripId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/tripPlanner/trip/{tripId}/accept', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the deleteInviteUser operation.
     * @callback module:api/PlannerApi~deleteInviteUserCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * delete an invited user from the trip
     * @param {String} sessionId The sessionID of the user
     * @param {Integer} tripId The id of the trip
     * @param {Integer} userId The id of the user to invite
     * @param {module:api/PlannerApi~deleteInviteUserCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.deleteInviteUser = function(sessionId, tripId, userId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling deleteInviteUser";
      }

      // verify the required parameter 'tripId' is set
      if (tripId == undefined || tripId == null) {
        throw "Missing the required parameter 'tripId' when calling deleteInviteUser";
      }

      // verify the required parameter 'userId' is set
      if (userId == undefined || userId == null) {
        throw "Missing the required parameter 'userId' when calling deleteInviteUser";
      }


      var pathParams = {
        'tripId': tripId
      };
      var queryParams = {
        'userId': userId
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/tripPlanner/trip/{tripId}/inviteUser', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the deleteTripActivity operation.
     * @callback module:api/PlannerApi~deleteTripActivityCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Delete an activity of a trip
     * @param {String} sessionId The sessionID of the user
     * @param {Integer} activityId The activityId to delete
     * @param {module:api/PlannerApi~deleteTripActivityCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.deleteTripActivity = function(sessionId, activityId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling deleteTripActivity";
      }

      // verify the required parameter 'activityId' is set
      if (activityId == undefined || activityId == null) {
        throw "Missing the required parameter 'activityId' when calling deleteTripActivity";
      }


      var pathParams = {
        'activityId': activityId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/tripPlanner/activity/{activityId}', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getTrip operation.
     * @callback module:api/PlannerApi~getTripCallback
     * @param {String} error Error message, if any.
     * @param {module:model/TripResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get trip for a tripId
     * @param {String} sessionId The sessionID of the user
     * @param {Integer} tripId The id of the trip
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/PlannerApi~getTripCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/TripResponseDTO}
     */
    this.getTrip = function(sessionId, tripId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling getTrip";
      }

      // verify the required parameter 'tripId' is set
      if (tripId == undefined || tripId == null) {
        throw "Missing the required parameter 'tripId' when calling getTrip";
      }


      var pathParams = {
        'tripId': tripId
      };
      var queryParams = {
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = TripResponseDTO;

      return this.apiClient.callApi(
        '/tripPlanner/trip/{tripId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getTripOverview operation.
     * @callback module:api/PlannerApi~getTripOverviewCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/TripOverviewResponseDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get a list of trips
     * @param {String} sessionId The sessionID of the user
     * @param {Object} opts Optional parameters
     * @param {String} opts.filter filter trips. &#39;admin&#39;, &#39;invited&#39;, &#39;over&#39;
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 10)
     * @param {module:api/PlannerApi~getTripOverviewCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/TripOverviewResponseDTO>}
     */
    this.getTripOverview = function(sessionId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling getTripOverview";
      }


      var pathParams = {
      };
      var queryParams = {
        'filter': opts['filter'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [TripOverviewResponseDTO];

      return this.apiClient.callApi(
        '/tripPlanner/trip', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postInviteUser operation.
     * @callback module:api/PlannerApi~postInviteUserCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Invite an user to the trip
     * @param {String} sessionId The sessionID of the user
     * @param {Integer} tripId The id of the trip
     * @param {Integer} userId The id of the user to invite
     * @param {module:api/PlannerApi~postInviteUserCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postInviteUser = function(sessionId, tripId, userId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postInviteUser";
      }

      // verify the required parameter 'tripId' is set
      if (tripId == undefined || tripId == null) {
        throw "Missing the required parameter 'tripId' when calling postInviteUser";
      }

      // verify the required parameter 'userId' is set
      if (userId == undefined || userId == null) {
        throw "Missing the required parameter 'userId' when calling postInviteUser";
      }


      var pathParams = {
        'tripId': tripId
      };
      var queryParams = {
        'userId': userId
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/tripPlanner/trip/{tripId}/inviteUser', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postNewTrip operation.
     * @callback module:api/PlannerApi~postNewTripCallback
     * @param {String} error Error message, if any.
     * @param {module:model/TripResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Creates a new trip
     * @param {String} sessionId The sessionID of the user
     * @param {module:model/TripRequestDTO} trip The infos of a trip
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/PlannerApi~postNewTripCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/TripResponseDTO}
     */
    this.postNewTrip = function(sessionId, trip, opts, callback) {
      opts = opts || {};
      var postBody = trip;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postNewTrip";
      }

      // verify the required parameter 'trip' is set
      if (trip == undefined || trip == null) {
        throw "Missing the required parameter 'trip' when calling postNewTrip";
      }


      var pathParams = {
      };
      var queryParams = {
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = TripResponseDTO;

      return this.apiClient.callApi(
        '/tripPlanner/trip', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postTrip operation.
     * @callback module:api/PlannerApi~postTripCallback
     * @param {String} error Error message, if any.
     * @param {module:model/TripResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Change data of a trip
     * @param {String} sessionId The sessionID of the user
     * @param {Integer} tripId The id of the trip
     * @param {module:model/TripRequestDTO} trip The data to change, null if should not change
     * @param {module:api/PlannerApi~postTripCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/TripResponseDTO}
     */
    this.postTrip = function(sessionId, tripId, trip, callback) {
      var postBody = trip;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postTrip";
      }

      // verify the required parameter 'tripId' is set
      if (tripId == undefined || tripId == null) {
        throw "Missing the required parameter 'tripId' when calling postTrip";
      }

      // verify the required parameter 'trip' is set
      if (trip == undefined || trip == null) {
        throw "Missing the required parameter 'trip' when calling postTrip";
      }


      var pathParams = {
        'tripId': tripId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = TripResponseDTO;

      return this.apiClient.callApi(
        '/tripPlanner/trip/{tripId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postTripActivity operation.
     * @callback module:api/PlannerApi~postTripActivityCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Adds a new activity to a trip
     * @param {String} sessionId The sessionID of the user
     * @param {Integer} tripId The id of the trip
     * @param {module:model/TripActivityRequestDTO} activity The activity to add
     * @param {module:api/PlannerApi~postTripActivityCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postTripActivity = function(sessionId, tripId, activity, callback) {
      var postBody = activity;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postTripActivity";
      }

      // verify the required parameter 'tripId' is set
      if (tripId == undefined || tripId == null) {
        throw "Missing the required parameter 'tripId' when calling postTripActivity";
      }

      // verify the required parameter 'activity' is set
      if (activity == undefined || activity == null) {
        throw "Missing the required parameter 'activity' when calling postTripActivity";
      }


      var pathParams = {
        'tripId': tripId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/tripPlanner/trip/{tripId}/activity', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postTripPlanner operation.
     * @callback module:api/PlannerApi~postTripPlannerCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/TripPlannerResponseDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Return a list of loactions for the match of the user requirements
     * @param {Array.<module:model/TripPlannerRequestDTO>} ratings The ratings from the user
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {module:api/PlannerApi~postTripPlannerCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/TripPlannerResponseDTO>}
     */
    this.postTripPlanner = function(ratings, opts, callback) {
      opts = opts || {};
      var postBody = ratings;

      // verify the required parameter 'ratings' is set
      if (ratings == undefined || ratings == null) {
        throw "Missing the required parameter 'ratings' when calling postTripPlanner";
      }


      var pathParams = {
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [TripPlannerResponseDTO];

      return this.apiClient.callApi(
        '/tripPlanner', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the putAcceptTrip operation.
     * @callback module:api/PlannerApi~putAcceptTripCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Accept for a trip
     * @param {String} sessionId The sessionID of the user
     * @param {Integer} tripId The id of the trip
     * @param {module:api/PlannerApi~putAcceptTripCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.putAcceptTrip = function(sessionId, tripId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling putAcceptTrip";
      }

      // verify the required parameter 'tripId' is set
      if (tripId == undefined || tripId == null) {
        throw "Missing the required parameter 'tripId' when calling putAcceptTrip";
      }


      var pathParams = {
        'tripId': tripId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/tripPlanner/trip/{tripId}/accept', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the putTripActivity operation.
     * @callback module:api/PlannerApi~putTripActivityCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Change an activity of a trip
     * @param {String} sessionId The sessionID of the user
     * @param {Integer} activityId The activityId to change
     * @param {module:model/TripActivityRequestDTO} activity The activity to add
     * @param {module:api/PlannerApi~putTripActivityCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.putTripActivity = function(sessionId, activityId, activity, callback) {
      var postBody = activity;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling putTripActivity";
      }

      // verify the required parameter 'activityId' is set
      if (activityId == undefined || activityId == null) {
        throw "Missing the required parameter 'activityId' when calling putTripActivity";
      }

      // verify the required parameter 'activity' is set
      if (activity == undefined || activity == null) {
        throw "Missing the required parameter 'activity' when calling putTripActivity";
      }


      var pathParams = {
        'activityId': activityId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/tripPlanner/activity/{activityId}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

},{"../ApiClient":1,"../model/ErrorResponse":23,"../model/TripActivityRequestDTO":39,"../model/TripOverviewResponseDTO":41,"../model/TripPlannerRequestDTO":42,"../model/TripPlannerResponseDTO":43,"../model/TripRequestDTO":44,"../model/TripResponseDTO":45}],7:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ErrorResponse', 'model/RatingCategoryResponseDTO', 'model/RatingCategoryRequestDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorResponse'), require('../model/RatingCategoryResponseDTO'), require('../model/RatingCategoryRequestDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.RatingApi = factory(root.Travis.ApiClient, root.Travis.ErrorResponse, root.Travis.RatingCategoryResponseDTO, root.Travis.RatingCategoryRequestDTO);
  }
}(this, function(ApiClient, ErrorResponse, RatingCategoryResponseDTO, RatingCategoryRequestDTO) {
  'use strict';

  /**
   * Rating service.
   * @module api/RatingApi
   * @version 0.0.1
   */

  /**
   * Constructs a new RatingApi. 
   * @alias module:api/RatingApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getRatingCategories operation.
     * @callback module:api/RatingApi~getRatingCategoriesCallback
     * @param {String} error Error message, if any.
     * @param {Array.<'String'>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get ratingCategories for a categoryGroup
     * @param {String} categoryGroup Group of the Categories (bsp. - LOCATION_ for location-Categories) 
     * @param {module:api/RatingApi~getRatingCategoriesCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<'String'>}
     */
    this.getRatingCategories = function(categoryGroup, callback) {
      var postBody = null;

      // verify the required parameter 'categoryGroup' is set
      if (categoryGroup == undefined || categoryGroup == null) {
        throw "Missing the required parameter 'categoryGroup' when calling getRatingCategories";
      }


      var pathParams = {
        'categoryGroup': categoryGroup
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ['String'];

      return this.apiClient.callApi(
        '/rating/ratingCategories/{categoryGroup}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getRatingLocation operation.
     * @callback module:api/RatingApi~getRatingLocationCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/RatingCategoryResponseDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get the ratings of a location
     * @param {Integer} locationId ID of the location
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.userId ID of the user, get only rating from user
     * @param {String} opts.category categroyGroup to filter (f.e. LOCATION)
     * @param {module:api/RatingApi~getRatingLocationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/RatingCategoryResponseDTO>}
     */
    this.getRatingLocation = function(locationId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling getRatingLocation";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
        'userId': opts['userId'],
        'category': opts['category']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [RatingCategoryResponseDTO];

      return this.apiClient.callApi(
        '/rating/location/{locationId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postRatingLocation operation.
     * @callback module:api/RatingApi~postRatingLocationCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add ratings to a location
     * @param {Integer} locationId ID of the location
     * @param {String} sessionId The sessionID of the user
     * @param {Array.<module:model/RatingCategoryRequestDTO>} ratings The ratings from the user
     * @param {module:api/RatingApi~postRatingLocationCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postRatingLocation = function(locationId, sessionId, ratings, callback) {
      var postBody = ratings;

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling postRatingLocation";
      }

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postRatingLocation";
      }

      // verify the required parameter 'ratings' is set
      if (ratings == undefined || ratings == null) {
        throw "Missing the required parameter 'ratings' when calling postRatingLocation";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/rating/location/{locationId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

},{"../ApiClient":1,"../model/ErrorResponse":23,"../model/RatingCategoryRequestDTO":37,"../model/RatingCategoryResponseDTO":38}],8:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LocationResponseDTO', 'model/ErrorResponse', 'model/MainSearchDTO', 'model/UserDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/LocationResponseDTO'), require('../model/ErrorResponse'), require('../model/MainSearchDTO'), require('../model/UserDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.SearchApi = factory(root.Travis.ApiClient, root.Travis.LocationResponseDTO, root.Travis.ErrorResponse, root.Travis.MainSearchDTO, root.Travis.UserDTO);
  }
}(this, function(ApiClient, LocationResponseDTO, ErrorResponse, MainSearchDTO, UserDTO) {
  'use strict';

  /**
   * Search service.
   * @module api/SearchApi
   * @version 0.0.1
   */

  /**
   * Constructs a new SearchApi. 
   * @alias module:api/SearchApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getLocationSearch operation.
     * @callback module:api/SearchApi~getLocationSearchCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/LocationResponseDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * search a location for the searchText
     * @param {String} searchText Text from the searchBox
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {module:api/SearchApi~getLocationSearchCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/LocationResponseDTO>}
     */
    this.getLocationSearch = function(searchText, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'searchText' is set
      if (searchText == undefined || searchText == null) {
        throw "Missing the required parameter 'searchText' when calling getLocationSearch";
      }


      var pathParams = {
        'searchText': searchText
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [LocationResponseDTO];

      return this.apiClient.callApi(
        '/search/location/{searchText}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getSearchMain operation.
     * @callback module:api/SearchApi~getSearchMainCallback
     * @param {String} error Error message, if any.
     * @param {module:model/MainSearchDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Search for the mainPage (for useres and places)
     * @param {String} searchText The text from the searchbox
     * @param {Object} opts Optional parameters
     * @param {String} opts.sessionId The sessionID of the user
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 10)
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/SearchApi~getSearchMainCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/MainSearchDTO}
     */
    this.getSearchMain = function(searchText, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'searchText' is set
      if (searchText == undefined || searchText == null) {
        throw "Missing the required parameter 'searchText' when calling getSearchMain";
      }


      var pathParams = {
        'searchText': searchText
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize'],
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': opts['sessionId']
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = MainSearchDTO;

      return this.apiClient.callApi(
        '/search/main/{searchText}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getUserSearch operation.
     * @callback module:api/SearchApi~getUserSearchCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/UserDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Returns a user for the search attributes
     * @param {String} searchText The text from the searchbox
     * @param {Object} opts Optional parameters
     * @param {String} opts.sessionId The sessionID of the user
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/SearchApi~getUserSearchCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/UserDTO>}
     */
    this.getUserSearch = function(searchText, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'searchText' is set
      if (searchText == undefined || searchText == null) {
        throw "Missing the required parameter 'searchText' when calling getUserSearch";
      }


      var pathParams = {
        'searchText': searchText
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize'],
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': opts['sessionId']
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [UserDTO];

      return this.apiClient.callApi(
        '/search/user/{searchText}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

},{"../ApiClient":1,"../model/ErrorResponse":23,"../model/LocationResponseDTO":26,"../model/MainSearchDTO":30,"../model/UserDTO":47}],9:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/UserMainPageEntryDTO', 'model/ErrorResponse', 'model/UserProfileDTO', 'model/UserDTO', 'model/DiaryEntryDTO', 'model/UserSearchDTO', 'model/LocationCoordinateDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/UserMainPageEntryDTO'), require('../model/ErrorResponse'), require('../model/UserProfileDTO'), require('../model/UserDTO'), require('../model/DiaryEntryDTO'), require('../model/UserSearchDTO'), require('../model/LocationCoordinateDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.UserInfoApi = factory(root.Travis.ApiClient, root.Travis.UserMainPageEntryDTO, root.Travis.ErrorResponse, root.Travis.UserProfileDTO, root.Travis.UserDTO, root.Travis.DiaryEntryDTO, root.Travis.UserSearchDTO, root.Travis.LocationCoordinateDTO);
  }
}(this, function(ApiClient, UserMainPageEntryDTO, ErrorResponse, UserProfileDTO, UserDTO, DiaryEntryDTO, UserSearchDTO, LocationCoordinateDTO) {
  'use strict';

  /**
   * UserInfo service.
   * @module api/UserInfoApi
   * @version 0.0.1
   */

  /**
   * Constructs a new UserInfoApi. 
   * @alias module:api/UserInfoApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getLikedDiaryEntries operation.
     * @callback module:api/UserInfoApi~getLikedDiaryEntriesCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/UserMainPageEntryDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * get the diaryEntries, which the user has liked
     * @param {Integer} id The ID of the user
     * @param {Object} opts Optional parameters
     * @param {String} opts.sessionId The sessionID of the user
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/UserInfoApi~getLikedDiaryEntriesCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/UserMainPageEntryDTO>}
     */
    this.getLikedDiaryEntries = function(id, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling getLikedDiaryEntries";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize'],
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': opts['sessionId']
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [UserMainPageEntryDTO];

      return this.apiClient.callApi(
        '/user/{id}/likedDiaryEntries', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getMyProfile operation.
     * @callback module:api/UserInfoApi~getMyProfileCallback
     * @param {String} error Error message, if any.
     * @param {module:model/UserProfileDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Request the profile informations about an user
     * @param {String} sessionId The sessionID of the user
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/UserInfoApi~getMyProfileCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/UserProfileDTO}
     */
    this.getMyProfile = function(sessionId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling getMyProfile";
      }


      var pathParams = {
      };
      var queryParams = {
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = UserProfileDTO;

      return this.apiClient.callApi(
        '/user/myProfile', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getProfile operation.
     * @callback module:api/UserInfoApi~getProfileCallback
     * @param {String} error Error message, if any.
     * @param {module:model/UserProfileDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Request the profile informations about an user
     * @param {String} sessionId The sessionID of the user
     * @param {String} id The ID of the user
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/UserInfoApi~getProfileCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/UserProfileDTO}
     */
    this.getProfile = function(sessionId, id, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling getProfile";
      }

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling getProfile";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = UserProfileDTO;

      return this.apiClient.callApi(
        '/user/{id}/profile', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getUserById operation.
     * @callback module:api/UserInfoApi~getUserByIdCallback
     * @param {String} error Error message, if any.
     * @param {module:model/UserDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Returns a user for an ID
     * @param {String} id The ID of the user
     * @param {module:api/UserInfoApi~getUserByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/UserDTO}
     */
    this.getUserById = function(id, callback) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling getUserById";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = UserDTO;

      return this.apiClient.callApi(
        '/user/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getUserDiaryEntries operation.
     * @callback module:api/UserInfoApi~getUserDiaryEntriesCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/DiaryEntryDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * get diaryEntris for userId
     * @param {Integer} id The userId for diary entries
     * @param {Object} opts Optional parameters
     * @param {String} opts.sessionId The sessionID of the user
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {module:api/UserInfoApi~getUserDiaryEntriesCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/DiaryEntryDTO>}
     */
    this.getUserDiaryEntries = function(id, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling getUserDiaryEntries";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {
        'sessionId': opts['sessionId']
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [DiaryEntryDTO];

      return this.apiClient.callApi(
        '/user/{id}/diary', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getUserFollower operation.
     * @callback module:api/UserInfoApi~getUserFollowerCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/UserDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * get users which follows this users
     * @param {Integer} id The ID of the user
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/UserInfoApi~getUserFollowerCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/UserDTO>}
     */
    this.getUserFollower = function(id, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling getUserFollower";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [UserDTO];

      return this.apiClient.callApi(
        '/user/{id}/follower', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getUserFollowing operation.
     * @callback module:api/UserInfoApi~getUserFollowingCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/UserDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * get users which this users follows
     * @param {Integer} id The ID of the user
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/UserInfoApi~getUserFollowingCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/UserDTO>}
     */
    this.getUserFollowing = function(id, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling getUserFollowing";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [UserDTO];

      return this.apiClient.callApi(
        '/user/{id}/following', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getUserMainPage operation.
     * @callback module:api/UserInfoApi~getUserMainPageCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/UserMainPageEntryDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Returns a user for the search attributes
     * @param {String} sessionId The sessionID of the user
     * @param {Object} opts Optional parameters
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {Integer} opts.pImgWidth Width of the profile image (default to 150)
     * @param {Integer} opts.pImgHeight Height of the profile image (default to 150)
     * @param {module:api/UserInfoApi~getUserMainPageCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/UserMainPageEntryDTO>}
     */
    this.getUserMainPage = function(sessionId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling getUserMainPage";
      }


      var pathParams = {
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize'],
        'pImgWidth': opts['pImgWidth'],
        'pImgHeight': opts['pImgHeight']
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [UserMainPageEntryDTO];

      return this.apiClient.callApi(
        '/user/mainPage', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getUserSearchDeprecated operation.
     * @callback module:api/UserInfoApi~getUserSearchDeprecatedCallback
     * @param {String} error Error message, if any.
     * @param {module:model/UserSearchDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Returns a user for the search attributes
     * @param {String} searchText The text from the searchbox
     * @param {Object} opts Optional parameters
     * @param {String} opts.sessionId The sessionID of the user
     * @param {Integer} opts.page page for paging (starts with 0) (default to 0)
     * @param {Integer} opts.pageSize pageSize for paging (default to 20)
     * @param {module:api/UserInfoApi~getUserSearchDeprecatedCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/UserSearchDTO}
     */
    this.getUserSearchDeprecated = function(searchText, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'searchText' is set
      if (searchText == undefined || searchText == null) {
        throw "Missing the required parameter 'searchText' when calling getUserSearchDeprecated";
      }


      var pathParams = {
        'searchText': searchText
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {
        'sessionId': opts['sessionId']
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = UserSearchDTO;

      return this.apiClient.callApi(
        '/user/search/{searchText}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getVisitedLocation operation.
     * @callback module:api/UserInfoApi~getVisitedLocationCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/LocationCoordinateDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * locations which the user has visited
     * @param {Integer} id The ID of the user
     * @param {module:api/UserInfoApi~getVisitedLocationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<module:model/LocationCoordinateDTO>}
     */
    this.getVisitedLocation = function(id, callback) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling getVisitedLocation";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [LocationCoordinateDTO];

      return this.apiClient.callApi(
        '/user/{id}/visitedLocation', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

},{"../ApiClient":1,"../model/DiaryEntryDTO":17,"../model/ErrorResponse":23,"../model/LocationCoordinateDTO":24,"../model/UserDTO":47,"../model/UserMainPageEntryDTO":48,"../model/UserProfileDTO":49,"../model/UserSearchDTO":50}],10:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ErrorResponse', 'model/PushNotificationDTO', 'model/ProfileLocationResponseDTO', 'model/ProfileTextDTO', 'model/ProfileTextResponseDTO', 'model/ProfileTagDTO', 'model/ProfileTagResponseDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorResponse'), require('../model/PushNotificationDTO'), require('../model/ProfileLocationResponseDTO'), require('../model/ProfileTextDTO'), require('../model/ProfileTextResponseDTO'), require('../model/ProfileTagDTO'), require('../model/ProfileTagResponseDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.UserUpdateApi = factory(root.Travis.ApiClient, root.Travis.ErrorResponse, root.Travis.PushNotificationDTO, root.Travis.ProfileLocationResponseDTO, root.Travis.ProfileTextDTO, root.Travis.ProfileTextResponseDTO, root.Travis.ProfileTagDTO, root.Travis.ProfileTagResponseDTO);
  }
}(this, function(ApiClient, ErrorResponse, PushNotificationDTO, ProfileLocationResponseDTO, ProfileTextDTO, ProfileTextResponseDTO, ProfileTagDTO, ProfileTagResponseDTO) {
  'use strict';

  /**
   * UserUpdate service.
   * @module api/UserUpdateApi
   * @version 0.0.1
   */

  /**
   * Constructs a new UserUpdateApi. 
   * @alias module:api/UserUpdateApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the deleteFollowUser operation.
     * @callback module:api/UserUpdateApi~deleteFollowUserCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Remove a user to follow
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} followUserId The ID of the user to unfollow
     * @param {module:api/UserUpdateApi~deleteFollowUserCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.deleteFollowUser = function(sessionId, followUserId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling deleteFollowUser";
      }

      // verify the required parameter 'followUserId' is set
      if (followUserId == undefined || followUserId == null) {
        throw "Missing the required parameter 'followUserId' when calling deleteFollowUser";
      }


      var pathParams = {
        'followUserId': followUserId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/user/followUser/{followUserId}', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the deleteProfileTag operation.
     * @callback module:api/UserUpdateApi~deleteProfileTagCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * set an user tag to inactive
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} tagId The ID of the tag
     * @param {module:api/UserUpdateApi~deleteProfileTagCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.deleteProfileTag = function(sessionId, tagId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling deleteProfileTag";
      }

      // verify the required parameter 'tagId' is set
      if (tagId == undefined || tagId == null) {
        throw "Missing the required parameter 'tagId' when calling deleteProfileTag";
      }


      var pathParams = {
        'tagId': tagId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/user/profileTag/{tagId}', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the deletePushNotification operation.
     * @callback module:api/UserUpdateApi~deletePushNotificationCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Deactivate push Notifications for the user
     * @param {String} sessionId The sessionId of the user
     * @param {module:api/UserUpdateApi~deletePushNotificationCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.deletePushNotification = function(sessionId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling deletePushNotification";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/user/pushNotification', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the deleteVisitedLocation operation.
     * @callback module:api/UserUpdateApi~deleteVisitedLocationCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Delete a location which the user has visited
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} locationId The ID of the visited location
     * @param {module:api/UserUpdateApi~deleteVisitedLocationCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.deleteVisitedLocation = function(sessionId, locationId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling deleteVisitedLocation";
      }

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling deleteVisitedLocation";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/user/visitedLocation/{locationId}', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getPushNotification operation.
     * @callback module:api/UserUpdateApi~getPushNotificationCallback
     * @param {String} error Error message, if any.
     * @param {module:model/PushNotificationDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Activate push Notifications for the user
     * @param {String} sessionId The sessionId of the user
     * @param {module:api/UserUpdateApi~getPushNotificationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/PushNotificationDTO}
     */
    this.getPushNotification = function(sessionId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling getPushNotification";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = PushNotificationDTO;

      return this.apiClient.callApi(
        '/user/pushNotification', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postCurrentLocation operation.
     * @callback module:api/UserUpdateApi~postCurrentLocationCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ProfileLocationResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update currentLocation for a user
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} locationId The ID of the location
     * @param {module:api/UserUpdateApi~postCurrentLocationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/ProfileLocationResponseDTO}
     */
    this.postCurrentLocation = function(sessionId, locationId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postCurrentLocation";
      }

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling postCurrentLocation";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ProfileLocationResponseDTO;

      return this.apiClient.callApi(
        '/user/currentLocation/{locationId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postFollowUser operation.
     * @callback module:api/UserUpdateApi~postFollowUserCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add a user to follow
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} followUserId The ID of the user to follow
     * @param {module:api/UserUpdateApi~postFollowUserCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postFollowUser = function(sessionId, followUserId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postFollowUser";
      }

      // verify the required parameter 'followUserId' is set
      if (followUserId == undefined || followUserId == null) {
        throw "Missing the required parameter 'followUserId' when calling postFollowUser";
      }


      var pathParams = {
        'followUserId': followUserId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/user/followUser/{followUserId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postHomeLocation operation.
     * @callback module:api/UserUpdateApi~postHomeLocationCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ProfileLocationResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update homeLocation for a user
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} locationId The ID of the location
     * @param {module:api/UserUpdateApi~postHomeLocationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/ProfileLocationResponseDTO}
     */
    this.postHomeLocation = function(sessionId, locationId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postHomeLocation";
      }

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling postHomeLocation";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ProfileLocationResponseDTO;

      return this.apiClient.callApi(
        '/user/homeLocation/{locationId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postNextLocation operation.
     * @callback module:api/UserUpdateApi~postNextLocationCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ProfileLocationResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update nextLocation for a user
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} locationId The ID of the location
     * @param {module:api/UserUpdateApi~postNextLocationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/ProfileLocationResponseDTO}
     */
    this.postNextLocation = function(sessionId, locationId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postNextLocation";
      }

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling postNextLocation";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ProfileLocationResponseDTO;

      return this.apiClient.callApi(
        '/user/nextLocation/{locationId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postProfileLongText operation.
     * @callback module:api/UserUpdateApi~postProfileLongTextCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ProfileTextResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Save the long text for a user
     * @param {String} sessionId The sessionId of the user
     * @param {module:model/ProfileTextDTO} profileTextDTO Profile Text
     * @param {module:api/UserUpdateApi~postProfileLongTextCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/ProfileTextResponseDTO}
     */
    this.postProfileLongText = function(sessionId, profileTextDTO, callback) {
      var postBody = profileTextDTO;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postProfileLongText";
      }

      // verify the required parameter 'profileTextDTO' is set
      if (profileTextDTO == undefined || profileTextDTO == null) {
        throw "Missing the required parameter 'profileTextDTO' when calling postProfileLongText";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ProfileTextResponseDTO;

      return this.apiClient.callApi(
        '/user/profileLongText', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postProfileShortText operation.
     * @callback module:api/UserUpdateApi~postProfileShortTextCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ProfileTextResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Save the short text for a user
     * @param {String} sessionId The sessionId of the user
     * @param {module:model/ProfileTextDTO} profileTextDTO Profile Text
     * @param {module:api/UserUpdateApi~postProfileShortTextCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/ProfileTextResponseDTO}
     */
    this.postProfileShortText = function(sessionId, profileTextDTO, callback) {
      var postBody = profileTextDTO;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postProfileShortText";
      }

      // verify the required parameter 'profileTextDTO' is set
      if (profileTextDTO == undefined || profileTextDTO == null) {
        throw "Missing the required parameter 'profileTextDTO' when calling postProfileShortText";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ProfileTextResponseDTO;

      return this.apiClient.callApi(
        '/user/profileShortText', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postProfileTag operation.
     * @callback module:api/UserUpdateApi~postProfileTagCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ProfileTagResponseDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Save a tag for an user
     * @param {String} sessionId The sessionId of the user
     * @param {module:model/ProfileTagDTO} profileTagDTO Profile Tag
     * @param {module:api/UserUpdateApi~postProfileTagCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/ProfileTagResponseDTO}
     */
    this.postProfileTag = function(sessionId, profileTagDTO, callback) {
      var postBody = profileTagDTO;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postProfileTag";
      }

      // verify the required parameter 'profileTagDTO' is set
      if (profileTagDTO == undefined || profileTagDTO == null) {
        throw "Missing the required parameter 'profileTagDTO' when calling postProfileTag";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ProfileTagResponseDTO;

      return this.apiClient.callApi(
        '/user/profileTag', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the postVisitedLocation operation.
     * @callback module:api/UserUpdateApi~postVisitedLocationCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add a location which the user has visited
     * @param {String} sessionId The sessionId of the user
     * @param {Integer} locationId The ID of the visited location
     * @param {module:api/UserUpdateApi~postVisitedLocationCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.postVisitedLocation = function(sessionId, locationId, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling postVisitedLocation";
      }

      // verify the required parameter 'locationId' is set
      if (locationId == undefined || locationId == null) {
        throw "Missing the required parameter 'locationId' when calling postVisitedLocation";
      }


      var pathParams = {
        'locationId': locationId
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/user/visitedLocation/{locationId}', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the putPushNotification operation.
     * @callback module:api/UserUpdateApi~putPushNotificationCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Activate push Notifications for the user
     * @param {String} sessionId The sessionId of the user
     * @param {module:model/PushNotificationDTO} pushNotificationDTO Push Notifications
     * @param {module:api/UserUpdateApi~putPushNotificationCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.putPushNotification = function(sessionId, pushNotificationDTO, callback) {
      var postBody = pushNotificationDTO;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling putPushNotification";
      }

      // verify the required parameter 'pushNotificationDTO' is set
      if (pushNotificationDTO == undefined || pushNotificationDTO == null) {
        throw "Missing the required parameter 'pushNotificationDTO' when calling putPushNotification";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/user/pushNotification', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the putUserBirthday operation.
     * @callback module:api/UserUpdateApi~putUserBirthdayCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Set the bithday of a user for the sessionId
     * @param {String} sessionId The sessionId of the user
     * @param {String} birthday The birthday of the user
     * @param {module:api/UserUpdateApi~putUserBirthdayCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.putUserBirthday = function(sessionId, birthday, callback) {
      var postBody = null;

      // verify the required parameter 'sessionId' is set
      if (sessionId == undefined || sessionId == null) {
        throw "Missing the required parameter 'sessionId' when calling putUserBirthday";
      }

      // verify the required parameter 'birthday' is set
      if (birthday == undefined || birthday == null) {
        throw "Missing the required parameter 'birthday' when calling putUserBirthday";
      }


      var pathParams = {
        'birthday': birthday
      };
      var queryParams = {
      };
      var headerParams = {
        'sessionId': sessionId
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;

      return this.apiClient.callApi(
        '/user/birthday/{birthday}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

},{"../ApiClient":1,"../model/ErrorResponse":23,"../model/ProfileLocationResponseDTO":31,"../model/ProfileTagDTO":32,"../model/ProfileTagResponseDTO":33,"../model/ProfileTextDTO":34,"../model/ProfileTextResponseDTO":35,"../model/PushNotificationDTO":36}],11:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.CoordinatesResponseDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The CoordinatesResponseDTO model module.
   * @module model/CoordinatesResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>CoordinatesResponseDTO</code>.
   * @alias module:model/CoordinatesResponseDTO
   * @class
   * @param id
   * @param latitude
   * @param longitude
   * @param altitude
   */
  var exports = function(id, latitude, longitude, altitude) {
    var _this = this;

    _this['id'] = id;
    _this['latitude'] = latitude;
    _this['longitude'] = longitude;
    _this['altitude'] = altitude;
  };

  /**
   * Constructs a <code>CoordinatesResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/CoordinatesResponseDTO} obj Optional instance to populate.
   * @return {module:model/CoordinatesResponseDTO} The populated <code>CoordinatesResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('latitude')) {
        obj['latitude'] = ApiClient.convertToType(data['latitude'], 'Number');
      }
      if (data.hasOwnProperty('longitude')) {
        obj['longitude'] = ApiClient.convertToType(data['longitude'], 'Number');
      }
      if (data.hasOwnProperty('altitude')) {
        obj['altitude'] = ApiClient.convertToType(data['altitude'], 'Number');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Number} latitude
   */
  exports.prototype['latitude'] = undefined;
  /**
   * @member {Number} longitude
   */
  exports.prototype['longitude'] = undefined;
  /**
   * @member {Number} altitude
   */
  exports.prototype['altitude'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],12:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryCommentRequestDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The DiaryCommentRequestDTO model module.
   * @module model/DiaryCommentRequestDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryCommentRequestDTO</code>.
   * @alias module:model/DiaryCommentRequestDTO
   * @class
   * @param text
   */
  var exports = function(text) {
    var _this = this;

    _this['text'] = text;
  };

  /**
   * Constructs a <code>DiaryCommentRequestDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryCommentRequestDTO} obj Optional instance to populate.
   * @return {module:model/DiaryCommentRequestDTO} The populated <code>DiaryCommentRequestDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],13:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/UserDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./UserDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryCommentResponseDTO = factory(root.Travis.ApiClient, root.Travis.UserDTO);
  }
}(this, function(ApiClient, UserDTO) {
  'use strict';




  /**
   * The DiaryCommentResponseDTO model module.
   * @module model/DiaryCommentResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryCommentResponseDTO</code>.
   * @alias module:model/DiaryCommentResponseDTO
   * @class
   * @param user
   * @param text
   * @param createdTime
   */
  var exports = function(user, text, createdTime) {
    var _this = this;

    _this['user'] = user;
    _this['text'] = text;
    _this['createdTime'] = createdTime;
  };

  /**
   * Constructs a <code>DiaryCommentResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryCommentResponseDTO} obj Optional instance to populate.
   * @return {module:model/DiaryCommentResponseDTO} The populated <code>DiaryCommentResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('user')) {
        obj['user'] = UserDTO.constructFromObject(data['user']);
      }
      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
      if (data.hasOwnProperty('createdTime')) {
        obj['createdTime'] = ApiClient.convertToType(data['createdTime'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {module:model/UserDTO} user
   */
  exports.prototype['user'] = undefined;
  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;
  /**
   * @member {String} createdTime
   */
  exports.prototype['createdTime'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./UserDTO":47}],14:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/CoordinatesResponseDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./CoordinatesResponseDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryCoordinatesElementDTO = factory(root.Travis.ApiClient, root.Travis.CoordinatesResponseDTO);
  }
}(this, function(ApiClient, CoordinatesResponseDTO) {
  'use strict';




  /**
   * The DiaryCoordinatesElementDTO model module.
   * @module model/DiaryCoordinatesElementDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryCoordinatesElementDTO</code>.
   * @alias module:model/DiaryCoordinatesElementDTO
   * @class
   * @param id
   * @param sort
   * @param diaryEntryId
   * @param coordinates
   */
  var exports = function(id, sort, diaryEntryId, coordinates) {
    var _this = this;

    _this['id'] = id;
    _this['sort'] = sort;
    _this['diaryEntryId'] = diaryEntryId;
    _this['coordinates'] = coordinates;
  };

  /**
   * Constructs a <code>DiaryCoordinatesElementDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryCoordinatesElementDTO} obj Optional instance to populate.
   * @return {module:model/DiaryCoordinatesElementDTO} The populated <code>DiaryCoordinatesElementDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('sort')) {
        obj['sort'] = ApiClient.convertToType(data['sort'], 'Integer');
      }
      if (data.hasOwnProperty('diaryEntryId')) {
        obj['diaryEntryId'] = ApiClient.convertToType(data['diaryEntryId'], 'Integer');
      }
      if (data.hasOwnProperty('coordinates')) {
        obj['coordinates'] = CoordinatesResponseDTO.constructFromObject(data['coordinates']);
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Integer} sort
   */
  exports.prototype['sort'] = undefined;
  /**
   * @member {Integer} diaryEntryId
   */
  exports.prototype['diaryEntryId'] = undefined;
  /**
   * @member {module:model/CoordinatesResponseDTO} coordinates
   */
  exports.prototype['coordinates'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./CoordinatesResponseDTO":11}],15:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryCoordinatesRequestDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The DiaryCoordinatesRequestDTO model module.
   * @module model/DiaryCoordinatesRequestDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryCoordinatesRequestDTO</code>.
   * @alias module:model/DiaryCoordinatesRequestDTO
   * @class
   * @param sort
   * @param latitude
   * @param longitude
   * @param altitude
   */
  var exports = function(sort, latitude, longitude, altitude) {
    var _this = this;

    _this['sort'] = sort;
    _this['latitude'] = latitude;
    _this['longitude'] = longitude;
    _this['altitude'] = altitude;
  };

  /**
   * Constructs a <code>DiaryCoordinatesRequestDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryCoordinatesRequestDTO} obj Optional instance to populate.
   * @return {module:model/DiaryCoordinatesRequestDTO} The populated <code>DiaryCoordinatesRequestDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('sort')) {
        obj['sort'] = ApiClient.convertToType(data['sort'], 'Integer');
      }
      if (data.hasOwnProperty('latitude')) {
        obj['latitude'] = ApiClient.convertToType(data['latitude'], 'Number');
      }
      if (data.hasOwnProperty('longitude')) {
        obj['longitude'] = ApiClient.convertToType(data['longitude'], 'Number');
      }
      if (data.hasOwnProperty('altitude')) {
        obj['altitude'] = ApiClient.convertToType(data['altitude'], 'Number');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} sort
   */
  exports.prototype['sort'] = undefined;
  /**
   * @member {Number} latitude
   */
  exports.prototype['latitude'] = undefined;
  /**
   * @member {Number} longitude
   */
  exports.prototype['longitude'] = undefined;
  /**
   * @member {Number} altitude
   */
  exports.prototype['altitude'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],16:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/DiaryCoordinatesElementDTO', 'model/DiaryPictureElementDTO', 'model/DiaryTextElementDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./DiaryCoordinatesElementDTO'), require('./DiaryPictureElementDTO'), require('./DiaryTextElementDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryElementsResponseListDTO = factory(root.Travis.ApiClient, root.Travis.DiaryCoordinatesElementDTO, root.Travis.DiaryPictureElementDTO, root.Travis.DiaryTextElementDTO);
  }
}(this, function(ApiClient, DiaryCoordinatesElementDTO, DiaryPictureElementDTO, DiaryTextElementDTO) {
  'use strict';




  /**
   * The DiaryElementsResponseListDTO model module.
   * @module model/DiaryElementsResponseListDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryElementsResponseListDTO</code>.
   * @alias module:model/DiaryElementsResponseListDTO
   * @class
   * @param diaryEntryId
   * @param pictureElements
   * @param textElements
   * @param coordinatesElements
   */
  var exports = function(diaryEntryId, pictureElements, textElements, coordinatesElements) {
    var _this = this;

    _this['diaryEntryId'] = diaryEntryId;
    _this['pictureElements'] = pictureElements;
    _this['textElements'] = textElements;
    _this['coordinatesElements'] = coordinatesElements;
  };

  /**
   * Constructs a <code>DiaryElementsResponseListDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryElementsResponseListDTO} obj Optional instance to populate.
   * @return {module:model/DiaryElementsResponseListDTO} The populated <code>DiaryElementsResponseListDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('diaryEntryId')) {
        obj['diaryEntryId'] = ApiClient.convertToType(data['diaryEntryId'], 'Integer');
      }
      if (data.hasOwnProperty('pictureElements')) {
        obj['pictureElements'] = ApiClient.convertToType(data['pictureElements'], [DiaryPictureElementDTO]);
      }
      if (data.hasOwnProperty('textElements')) {
        obj['textElements'] = ApiClient.convertToType(data['textElements'], [DiaryTextElementDTO]);
      }
      if (data.hasOwnProperty('coordinatesElements')) {
        obj['coordinatesElements'] = ApiClient.convertToType(data['coordinatesElements'], [DiaryCoordinatesElementDTO]);
      }
    }
    return obj;
  }

  /**
   * @member {Integer} diaryEntryId
   */
  exports.prototype['diaryEntryId'] = undefined;
  /**
   * @member {Array.<module:model/DiaryPictureElementDTO>} pictureElements
   */
  exports.prototype['pictureElements'] = undefined;
  /**
   * @member {Array.<module:model/DiaryTextElementDTO>} textElements
   */
  exports.prototype['textElements'] = undefined;
  /**
   * @member {Array.<module:model/DiaryCoordinatesElementDTO>} coordinatesElements
   */
  exports.prototype['coordinatesElements'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./DiaryCoordinatesElementDTO":14,"./DiaryPictureElementDTO":18,"./DiaryTextElementDTO":21}],17:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/DiaryCoordinatesElementDTO', 'model/DiaryPictureElementDTO', 'model/DiaryTextElementDTO', 'model/LocationCoordinateDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./DiaryCoordinatesElementDTO'), require('./DiaryPictureElementDTO'), require('./DiaryTextElementDTO'), require('./LocationCoordinateDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryEntryDTO = factory(root.Travis.ApiClient, root.Travis.DiaryCoordinatesElementDTO, root.Travis.DiaryPictureElementDTO, root.Travis.DiaryTextElementDTO, root.Travis.LocationCoordinateDTO);
  }
}(this, function(ApiClient, DiaryCoordinatesElementDTO, DiaryPictureElementDTO, DiaryTextElementDTO, LocationCoordinateDTO) {
  'use strict';




  /**
   * The DiaryEntryDTO model module.
   * @module model/DiaryEntryDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryEntryDTO</code>.
   * @alias module:model/DiaryEntryDTO
   * @class
   * @param id
   * @param name
   * @param createdTime
   * @param userId
   * @param commentCount
   * @param location
   */
  var exports = function(id, name, createdTime, userId, commentCount, location) {
    var _this = this;

    _this['id'] = id;
    _this['name'] = name;
    _this['createdTime'] = createdTime;
    _this['userId'] = userId;

    _this['commentCount'] = commentCount;
    _this['location'] = location;



  };

  /**
   * Constructs a <code>DiaryEntryDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryEntryDTO} obj Optional instance to populate.
   * @return {module:model/DiaryEntryDTO} The populated <code>DiaryEntryDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('createdTime')) {
        obj['createdTime'] = ApiClient.convertToType(data['createdTime'], 'String');
      }
      if (data.hasOwnProperty('userId')) {
        obj['userId'] = ApiClient.convertToType(data['userId'], 'Integer');
      }
      if (data.hasOwnProperty('liked')) {
        obj['liked'] = ApiClient.convertToType(data['liked'], 'Boolean');
      }
      if (data.hasOwnProperty('commentCount')) {
        obj['commentCount'] = ApiClient.convertToType(data['commentCount'], 'Integer');
      }
      if (data.hasOwnProperty('location')) {
        obj['location'] = LocationCoordinateDTO.constructFromObject(data['location']);
      }
      if (data.hasOwnProperty('pictureElement')) {
        obj['pictureElement'] = DiaryPictureElementDTO.constructFromObject(data['pictureElement']);
      }
      if (data.hasOwnProperty('textElement')) {
        obj['textElement'] = DiaryTextElementDTO.constructFromObject(data['textElement']);
      }
      if (data.hasOwnProperty('coordinatesElement')) {
        obj['coordinatesElement'] = DiaryCoordinatesElementDTO.constructFromObject(data['coordinatesElement']);
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {String} createdTime
   */
  exports.prototype['createdTime'] = undefined;
  /**
   * @member {Integer} userId
   */
  exports.prototype['userId'] = undefined;
  /**
   * @member {Boolean} liked
   */
  exports.prototype['liked'] = undefined;
  /**
   * @member {Integer} commentCount
   */
  exports.prototype['commentCount'] = undefined;
  /**
   * @member {module:model/LocationCoordinateDTO} location
   */
  exports.prototype['location'] = undefined;
  /**
   * @member {module:model/DiaryPictureElementDTO} pictureElement
   */
  exports.prototype['pictureElement'] = undefined;
  /**
   * @member {module:model/DiaryTextElementDTO} textElement
   */
  exports.prototype['textElement'] = undefined;
  /**
   * @member {module:model/DiaryCoordinatesElementDTO} coordinatesElement
   */
  exports.prototype['coordinatesElement'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./DiaryCoordinatesElementDTO":14,"./DiaryPictureElementDTO":18,"./DiaryTextElementDTO":21,"./LocationCoordinateDTO":24}],18:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryPictureElementDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The DiaryPictureElementDTO model module.
   * @module model/DiaryPictureElementDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryPictureElementDTO</code>.
   * @alias module:model/DiaryPictureElementDTO
   * @class
   * @param id
   * @param sort
   * @param diaryEntryId
   * @param imageUrl
   */
  var exports = function(id, sort, diaryEntryId, imageUrl) {
    var _this = this;

    _this['id'] = id;
    _this['sort'] = sort;
    _this['diaryEntryId'] = diaryEntryId;
    _this['imageUrl'] = imageUrl;
  };

  /**
   * Constructs a <code>DiaryPictureElementDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryPictureElementDTO} obj Optional instance to populate.
   * @return {module:model/DiaryPictureElementDTO} The populated <code>DiaryPictureElementDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('sort')) {
        obj['sort'] = ApiClient.convertToType(data['sort'], 'Integer');
      }
      if (data.hasOwnProperty('diaryEntryId')) {
        obj['diaryEntryId'] = ApiClient.convertToType(data['diaryEntryId'], 'Integer');
      }
      if (data.hasOwnProperty('imageUrl')) {
        obj['imageUrl'] = ApiClient.convertToType(data['imageUrl'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Integer} sort
   */
  exports.prototype['sort'] = undefined;
  /**
   * @member {Integer} diaryEntryId
   */
  exports.prototype['diaryEntryId'] = undefined;
  /**
   * @member {String} imageUrl
   */
  exports.prototype['imageUrl'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],19:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryRequestDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The DiaryRequestDTO model module.
   * @module model/DiaryRequestDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryRequestDTO</code>.
   * @alias module:model/DiaryRequestDTO
   * @class
   * @param name
   * @param locationId
   */
  var exports = function(name, locationId) {
    var _this = this;

    _this['name'] = name;
    _this['locationId'] = locationId;
  };

  /**
   * Constructs a <code>DiaryRequestDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryRequestDTO} obj Optional instance to populate.
   * @return {module:model/DiaryRequestDTO} The populated <code>DiaryRequestDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('locationId')) {
        obj['locationId'] = ApiClient.convertToType(data['locationId'], 'Integer');
      }
    }
    return obj;
  }

  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {Integer} locationId
   */
  exports.prototype['locationId'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],20:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryResponseDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The DiaryResponseDTO model module.
   * @module model/DiaryResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryResponseDTO</code>.
   * @alias module:model/DiaryResponseDTO
   * @class
   * @param id
   * @param name
   * @param createdTime
   * @param userId
   * @param locationId
   */
  var exports = function(id, name, createdTime, userId, locationId) {
    var _this = this;

    _this['id'] = id;
    _this['name'] = name;
    _this['createdTime'] = createdTime;
    _this['userId'] = userId;
    _this['locationId'] = locationId;
  };

  /**
   * Constructs a <code>DiaryResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryResponseDTO} obj Optional instance to populate.
   * @return {module:model/DiaryResponseDTO} The populated <code>DiaryResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('createdTime')) {
        obj['createdTime'] = ApiClient.convertToType(data['createdTime'], 'String');
      }
      if (data.hasOwnProperty('userId')) {
        obj['userId'] = ApiClient.convertToType(data['userId'], 'Integer');
      }
      if (data.hasOwnProperty('locationId')) {
        obj['locationId'] = ApiClient.convertToType(data['locationId'], 'Integer');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {String} createdTime
   */
  exports.prototype['createdTime'] = undefined;
  /**
   * @member {Integer} userId
   */
  exports.prototype['userId'] = undefined;
  /**
   * @member {Integer} locationId
   */
  exports.prototype['locationId'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],21:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryTextElementDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The DiaryTextElementDTO model module.
   * @module model/DiaryTextElementDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryTextElementDTO</code>.
   * @alias module:model/DiaryTextElementDTO
   * @class
   * @param id
   * @param sort
   * @param diaryEntryId
   * @param text
   */
  var exports = function(id, sort, diaryEntryId, text) {
    var _this = this;

    _this['id'] = id;
    _this['sort'] = sort;
    _this['diaryEntryId'] = diaryEntryId;
    _this['text'] = text;
  };

  /**
   * Constructs a <code>DiaryTextElementDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryTextElementDTO} obj Optional instance to populate.
   * @return {module:model/DiaryTextElementDTO} The populated <code>DiaryTextElementDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('sort')) {
        obj['sort'] = ApiClient.convertToType(data['sort'], 'Integer');
      }
      if (data.hasOwnProperty('diaryEntryId')) {
        obj['diaryEntryId'] = ApiClient.convertToType(data['diaryEntryId'], 'Integer');
      }
      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Integer} sort
   */
  exports.prototype['sort'] = undefined;
  /**
   * @member {Integer} diaryEntryId
   */
  exports.prototype['diaryEntryId'] = undefined;
  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],22:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.DiaryTextRequestDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The DiaryTextRequestDTO model module.
   * @module model/DiaryTextRequestDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>DiaryTextRequestDTO</code>.
   * @alias module:model/DiaryTextRequestDTO
   * @class
   * @param sort
   * @param text
   */
  var exports = function(sort, text) {
    var _this = this;

    _this['sort'] = sort;
    _this['text'] = text;
  };

  /**
   * Constructs a <code>DiaryTextRequestDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DiaryTextRequestDTO} obj Optional instance to populate.
   * @return {module:model/DiaryTextRequestDTO} The populated <code>DiaryTextRequestDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('sort')) {
        obj['sort'] = ApiClient.convertToType(data['sort'], 'Integer');
      }
      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} sort
   */
  exports.prototype['sort'] = undefined;
  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],23:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.ErrorResponse = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The ErrorResponse model module.
   * @module model/ErrorResponse
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>ErrorResponse</code>.
   * @alias module:model/ErrorResponse
   * @class
   * @param message
   */
  var exports = function(message) {
    var _this = this;

    _this['message'] = message;
  };

  /**
   * Constructs a <code>ErrorResponse</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ErrorResponse} obj Optional instance to populate.
   * @return {module:model/ErrorResponse} The populated <code>ErrorResponse</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('message')) {
        obj['message'] = ApiClient.convertToType(data['message'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} message
   */
  exports.prototype['message'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],24:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.LocationCoordinateDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The LocationCoordinateDTO model module.
   * @module model/LocationCoordinateDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>LocationCoordinateDTO</code>.
   * @alias module:model/LocationCoordinateDTO
   * @class
   * @param id
   * @param name
   */
  var exports = function(id, name) {
    var _this = this;

    _this['id'] = id;
    _this['name'] = name;






  };

  /**
   * Constructs a <code>LocationCoordinateDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/LocationCoordinateDTO} obj Optional instance to populate.
   * @return {module:model/LocationCoordinateDTO} The populated <code>LocationCoordinateDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('googlePlaceId')) {
        obj['googlePlaceId'] = ApiClient.convertToType(data['googlePlaceId'], 'String');
      }
      if (data.hasOwnProperty('latitude')) {
        obj['latitude'] = ApiClient.convertToType(data['latitude'], 'Number');
      }
      if (data.hasOwnProperty('longitude')) {
        obj['longitude'] = ApiClient.convertToType(data['longitude'], 'Number');
      }
      if (data.hasOwnProperty('altitude')) {
        obj['altitude'] = ApiClient.convertToType(data['altitude'], 'Number');
      }
      if (data.hasOwnProperty('visited')) {
        obj['visited'] = ApiClient.convertToType(data['visited'], 'Boolean');
      }
      if (data.hasOwnProperty('rated')) {
        obj['rated'] = ApiClient.convertToType(data['rated'], 'Boolean');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {String} googlePlaceId
   */
  exports.prototype['googlePlaceId'] = undefined;
  /**
   * @member {Number} latitude
   */
  exports.prototype['latitude'] = undefined;
  /**
   * @member {Number} longitude
   */
  exports.prototype['longitude'] = undefined;
  /**
   * @member {Number} altitude
   */
  exports.prototype['altitude'] = undefined;
  /**
   * @member {Boolean} visited
   */
  exports.prototype['visited'] = undefined;
  /**
   * @member {Boolean} rated
   */
  exports.prototype['rated'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],25:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.LocationDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The LocationDTO model module.
   * @module model/LocationDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>LocationDTO</code>.
   * @alias module:model/LocationDTO
   * @class
   * @param name
   */
  var exports = function(name) {
    var _this = this;

    _this['name'] = name;




  };

  /**
   * Constructs a <code>LocationDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/LocationDTO} obj Optional instance to populate.
   * @return {module:model/LocationDTO} The populated <code>LocationDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('googlePlaceId')) {
        obj['googlePlaceId'] = ApiClient.convertToType(data['googlePlaceId'], 'String');
      }
      if (data.hasOwnProperty('latitude')) {
        obj['latitude'] = ApiClient.convertToType(data['latitude'], 'Number');
      }
      if (data.hasOwnProperty('longitude')) {
        obj['longitude'] = ApiClient.convertToType(data['longitude'], 'Number');
      }
      if (data.hasOwnProperty('altitude')) {
        obj['altitude'] = ApiClient.convertToType(data['altitude'], 'Number');
      }
    }
    return obj;
  }

  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {String} googlePlaceId
   */
  exports.prototype['googlePlaceId'] = undefined;
  /**
   * @member {Number} latitude
   */
  exports.prototype['latitude'] = undefined;
  /**
   * @member {Number} longitude
   */
  exports.prototype['longitude'] = undefined;
  /**
   * @member {Number} altitude
   */
  exports.prototype['altitude'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],26:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.LocationResponseDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The LocationResponseDTO model module.
   * @module model/LocationResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>LocationResponseDTO</code>.
   * @alias module:model/LocationResponseDTO
   * @class
   * @param id
   * @param name
   */
  var exports = function(id, name) {
    var _this = this;

    _this['id'] = id;
    _this['name'] = name;

  };

  /**
   * Constructs a <code>LocationResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/LocationResponseDTO} obj Optional instance to populate.
   * @return {module:model/LocationResponseDTO} The populated <code>LocationResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('coordinatesId')) {
        obj['coordinatesId'] = ApiClient.convertToType(data['coordinatesId'], 'Integer');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {Integer} coordinatesId
   */
  exports.prototype['coordinatesId'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],27:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LocationResponseDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./LocationResponseDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.LocationSearchDTO = factory(root.Travis.ApiClient, root.Travis.LocationResponseDTO);
  }
}(this, function(ApiClient, LocationResponseDTO) {
  'use strict';




  /**
   * The LocationSearchDTO model module.
   * @module model/LocationSearchDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>LocationSearchDTO</code>.
   * @alias module:model/LocationSearchDTO
   * @class
   * @param searchResult
   */
  var exports = function(searchResult) {
    var _this = this;

    _this['searchResult'] = searchResult;
  };

  /**
   * Constructs a <code>LocationSearchDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/LocationSearchDTO} obj Optional instance to populate.
   * @return {module:model/LocationSearchDTO} The populated <code>LocationSearchDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('searchResult')) {
        obj['searchResult'] = ApiClient.convertToType(data['searchResult'], [LocationResponseDTO]);
      }
    }
    return obj;
  }

  /**
   * @member {Array.<module:model/LocationResponseDTO>} searchResult
   */
  exports.prototype['searchResult'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./LocationResponseDTO":26}],28:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.LoginRequestDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The LoginRequestDTO model module.
   * @module model/LoginRequestDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>LoginRequestDTO</code>.
   * @alias module:model/LoginRequestDTO
   * @class
   * @param loginType
   * @param accessToken
   * @param firstName
   * @param lastName
   */
  var exports = function(loginType, accessToken, firstName, lastName) {
    var _this = this;

    _this['loginType'] = loginType;
    _this['accessToken'] = accessToken;
    _this['firstName'] = firstName;
    _this['lastName'] = lastName;

  };

  /**
   * Constructs a <code>LoginRequestDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/LoginRequestDTO} obj Optional instance to populate.
   * @return {module:model/LoginRequestDTO} The populated <code>LoginRequestDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('loginType')) {
        obj['loginType'] = ApiClient.convertToType(data['loginType'], 'String');
      }
      if (data.hasOwnProperty('accessToken')) {
        obj['accessToken'] = ApiClient.convertToType(data['accessToken'], 'String');
      }
      if (data.hasOwnProperty('firstName')) {
        obj['firstName'] = ApiClient.convertToType(data['firstName'], 'String');
      }
      if (data.hasOwnProperty('lastName')) {
        obj['lastName'] = ApiClient.convertToType(data['lastName'], 'String');
      }
      if (data.hasOwnProperty('birthday')) {
        obj['birthday'] = ApiClient.convertToType(data['birthday'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} loginType
   */
  exports.prototype['loginType'] = undefined;
  /**
   * @member {String} accessToken
   */
  exports.prototype['accessToken'] = undefined;
  /**
   * @member {String} firstName
   */
  exports.prototype['firstName'] = undefined;
  /**
   * @member {String} lastName
   */
  exports.prototype['lastName'] = undefined;
  /**
   * @member {String} birthday
   */
  exports.prototype['birthday'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],29:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.LoginResponseDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The LoginResponseDTO model module.
   * @module model/LoginResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>LoginResponseDTO</code>.
   * @alias module:model/LoginResponseDTO
   * @class
   * @param sessionId
   * @param userId
   */
  var exports = function(sessionId, userId) {
    var _this = this;

    _this['sessionId'] = sessionId;
    _this['userId'] = userId;
  };

  /**
   * Constructs a <code>LoginResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/LoginResponseDTO} obj Optional instance to populate.
   * @return {module:model/LoginResponseDTO} The populated <code>LoginResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('sessionId')) {
        obj['sessionId'] = ApiClient.convertToType(data['sessionId'], 'String');
      }
      if (data.hasOwnProperty('userId')) {
        obj['userId'] = ApiClient.convertToType(data['userId'], 'Integer');
      }
    }
    return obj;
  }

  /**
   * sessionId, valid for 2 hours
   * @member {String} sessionId
   */
  exports.prototype['sessionId'] = undefined;
  /**
   * @member {Integer} userId
   */
  exports.prototype['userId'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],30:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LocationResponseDTO', 'model/UserDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./LocationResponseDTO'), require('./UserDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.MainSearchDTO = factory(root.Travis.ApiClient, root.Travis.LocationResponseDTO, root.Travis.UserDTO);
  }
}(this, function(ApiClient, LocationResponseDTO, UserDTO) {
  'use strict';




  /**
   * The MainSearchDTO model module.
   * @module model/MainSearchDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>MainSearchDTO</code>.
   * @alias module:model/MainSearchDTO
   * @class
   */
  var exports = function() {
    var _this = this;



  };

  /**
   * Constructs a <code>MainSearchDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/MainSearchDTO} obj Optional instance to populate.
   * @return {module:model/MainSearchDTO} The populated <code>MainSearchDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('users')) {
        obj['users'] = ApiClient.convertToType(data['users'], [UserDTO]);
      }
      if (data.hasOwnProperty('locations')) {
        obj['locations'] = ApiClient.convertToType(data['locations'], [LocationResponseDTO]);
      }
    }
    return obj;
  }

  /**
   * @member {Array.<module:model/UserDTO>} users
   */
  exports.prototype['users'] = undefined;
  /**
   * @member {Array.<module:model/LocationResponseDTO>} locations
   */
  exports.prototype['locations'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./LocationResponseDTO":26,"./UserDTO":47}],31:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.ProfileLocationResponseDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The ProfileLocationResponseDTO model module.
   * @module model/ProfileLocationResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>ProfileLocationResponseDTO</code>.
   * @alias module:model/ProfileLocationResponseDTO
   * @class
   * @param id
   * @param userId
   * @param locationId
   * @param createdTime
   */
  var exports = function(id, userId, locationId, createdTime) {
    var _this = this;

    _this['id'] = id;
    _this['userId'] = userId;
    _this['locationId'] = locationId;
    _this['createdTime'] = createdTime;
  };

  /**
   * Constructs a <code>ProfileLocationResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ProfileLocationResponseDTO} obj Optional instance to populate.
   * @return {module:model/ProfileLocationResponseDTO} The populated <code>ProfileLocationResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('userId')) {
        obj['userId'] = ApiClient.convertToType(data['userId'], 'Integer');
      }
      if (data.hasOwnProperty('locationId')) {
        obj['locationId'] = ApiClient.convertToType(data['locationId'], 'Integer');
      }
      if (data.hasOwnProperty('createdTime')) {
        obj['createdTime'] = ApiClient.convertToType(data['createdTime'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Integer} userId
   */
  exports.prototype['userId'] = undefined;
  /**
   * @member {Integer} locationId
   */
  exports.prototype['locationId'] = undefined;
  /**
   * @member {String} createdTime
   */
  exports.prototype['createdTime'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],32:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.ProfileTagDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The ProfileTagDTO model module.
   * @module model/ProfileTagDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>ProfileTagDTO</code>.
   * @alias module:model/ProfileTagDTO
   * @class
   * @param text
   */
  var exports = function(text) {
    var _this = this;

    _this['text'] = text;
  };

  /**
   * Constructs a <code>ProfileTagDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ProfileTagDTO} obj Optional instance to populate.
   * @return {module:model/ProfileTagDTO} The populated <code>ProfileTagDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],33:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.ProfileTagResponseDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The ProfileTagResponseDTO model module.
   * @module model/ProfileTagResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>ProfileTagResponseDTO</code>.
   * @alias module:model/ProfileTagResponseDTO
   * @class
   * @param id
   * @param userId
   * @param text
   * @param lastChanged
   * @param isActive
   */
  var exports = function(id, userId, text, lastChanged, isActive) {
    var _this = this;

    _this['id'] = id;
    _this['userId'] = userId;
    _this['text'] = text;
    _this['lastChanged'] = lastChanged;
    _this['isActive'] = isActive;
  };

  /**
   * Constructs a <code>ProfileTagResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ProfileTagResponseDTO} obj Optional instance to populate.
   * @return {module:model/ProfileTagResponseDTO} The populated <code>ProfileTagResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('userId')) {
        obj['userId'] = ApiClient.convertToType(data['userId'], 'Integer');
      }
      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
      if (data.hasOwnProperty('lastChanged')) {
        obj['lastChanged'] = ApiClient.convertToType(data['lastChanged'], 'String');
      }
      if (data.hasOwnProperty('isActive')) {
        obj['isActive'] = ApiClient.convertToType(data['isActive'], 'Boolean');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Integer} userId
   */
  exports.prototype['userId'] = undefined;
  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;
  /**
   * @member {String} lastChanged
   */
  exports.prototype['lastChanged'] = undefined;
  /**
   * @member {Boolean} isActive
   */
  exports.prototype['isActive'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],34:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.ProfileTextDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The ProfileTextDTO model module.
   * @module model/ProfileTextDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>ProfileTextDTO</code>.
   * @alias module:model/ProfileTextDTO
   * @class
   * @param text
   */
  var exports = function(text) {
    var _this = this;

    _this['text'] = text;
  };

  /**
   * Constructs a <code>ProfileTextDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ProfileTextDTO} obj Optional instance to populate.
   * @return {module:model/ProfileTextDTO} The populated <code>ProfileTextDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],35:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.ProfileTextResponseDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The ProfileTextResponseDTO model module.
   * @module model/ProfileTextResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>ProfileTextResponseDTO</code>.
   * @alias module:model/ProfileTextResponseDTO
   * @class
   * @param id
   * @param userId
   * @param text
   * @param createdTime
   */
  var exports = function(id, userId, text, createdTime) {
    var _this = this;

    _this['id'] = id;
    _this['userId'] = userId;
    _this['text'] = text;
    _this['createdTime'] = createdTime;
  };

  /**
   * Constructs a <code>ProfileTextResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ProfileTextResponseDTO} obj Optional instance to populate.
   * @return {module:model/ProfileTextResponseDTO} The populated <code>ProfileTextResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('userId')) {
        obj['userId'] = ApiClient.convertToType(data['userId'], 'Integer');
      }
      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
      if (data.hasOwnProperty('createdTime')) {
        obj['createdTime'] = ApiClient.convertToType(data['createdTime'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Integer} userId
   */
  exports.prototype['userId'] = undefined;
  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;
  /**
   * @member {String} createdTime
   */
  exports.prototype['createdTime'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],36:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.PushNotificationDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The PushNotificationDTO model module.
   * @module model/PushNotificationDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>PushNotificationDTO</code>.
   * @alias module:model/PushNotificationDTO
   * @class
   * @param registrationToken
   * @param newFollowEntry
   */
  var exports = function(registrationToken, newFollowEntry) {
    var _this = this;

    _this['registrationToken'] = registrationToken;
    _this['newFollowEntry'] = newFollowEntry;
  };

  /**
   * Constructs a <code>PushNotificationDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PushNotificationDTO} obj Optional instance to populate.
   * @return {module:model/PushNotificationDTO} The populated <code>PushNotificationDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('registrationToken')) {
        obj['registrationToken'] = ApiClient.convertToType(data['registrationToken'], 'String');
      }
      if (data.hasOwnProperty('newFollowEntry')) {
        obj['newFollowEntry'] = ApiClient.convertToType(data['newFollowEntry'], 'Boolean');
      }
    }
    return obj;
  }

  /**
   * @member {String} registrationToken
   */
  exports.prototype['registrationToken'] = undefined;
  /**
   * @member {Boolean} newFollowEntry
   */
  exports.prototype['newFollowEntry'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],37:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.RatingCategoryRequestDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The RatingCategoryRequestDTO model module.
   * @module model/RatingCategoryRequestDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>RatingCategoryRequestDTO</code>.
   * @alias module:model/RatingCategoryRequestDTO
   * @class
   * @param ratingCategory
   * @param rating
   */
  var exports = function(ratingCategory, rating) {
    var _this = this;

    _this['ratingCategory'] = ratingCategory;
    _this['rating'] = rating;
  };

  /**
   * Constructs a <code>RatingCategoryRequestDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RatingCategoryRequestDTO} obj Optional instance to populate.
   * @return {module:model/RatingCategoryRequestDTO} The populated <code>RatingCategoryRequestDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('ratingCategory')) {
        obj['ratingCategory'] = ApiClient.convertToType(data['ratingCategory'], 'String');
      }
      if (data.hasOwnProperty('rating')) {
        obj['rating'] = ApiClient.convertToType(data['rating'], 'Number');
      }
    }
    return obj;
  }

  /**
   * @member {String} ratingCategory
   */
  exports.prototype['ratingCategory'] = undefined;
  /**
   * @member {Number} rating
   */
  exports.prototype['rating'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],38:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.RatingCategoryResponseDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The RatingCategoryResponseDTO model module.
   * @module model/RatingCategoryResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>RatingCategoryResponseDTO</code>.
   * @alias module:model/RatingCategoryResponseDTO
   * @class
   * @param ratingCategory
   * @param average
   * @param count
   */
  var exports = function(ratingCategory, average, count) {
    var _this = this;

    _this['ratingCategory'] = ratingCategory;
    _this['average'] = average;
    _this['count'] = count;
  };

  /**
   * Constructs a <code>RatingCategoryResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RatingCategoryResponseDTO} obj Optional instance to populate.
   * @return {module:model/RatingCategoryResponseDTO} The populated <code>RatingCategoryResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('ratingCategory')) {
        obj['ratingCategory'] = ApiClient.convertToType(data['ratingCategory'], 'String');
      }
      if (data.hasOwnProperty('average')) {
        obj['average'] = ApiClient.convertToType(data['average'], 'Number');
      }
      if (data.hasOwnProperty('count')) {
        obj['count'] = ApiClient.convertToType(data['count'], 'Integer');
      }
    }
    return obj;
  }

  /**
   * @member {String} ratingCategory
   */
  exports.prototype['ratingCategory'] = undefined;
  /**
   * 0 ... 5, < 0 && > 5  => error
   * @member {Number} average
   */
  exports.prototype['average'] = undefined;
  /**
   * @member {Integer} count
   */
  exports.prototype['count'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],39:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.TripActivityRequestDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The TripActivityRequestDTO model module.
   * @module model/TripActivityRequestDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>TripActivityRequestDTO</code>.
   * @alias module:model/TripActivityRequestDTO
   * @class
   */
  var exports = function() {
    var _this = this;








  };

  /**
   * Constructs a <code>TripActivityRequestDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TripActivityRequestDTO} obj Optional instance to populate.
   * @return {module:model/TripActivityRequestDTO} The populated <code>TripActivityRequestDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
      if (data.hasOwnProperty('beginDate')) {
        obj['beginDate'] = ApiClient.convertToType(data['beginDate'], 'String');
      }
      if (data.hasOwnProperty('endDate')) {
        obj['endDate'] = ApiClient.convertToType(data['endDate'], 'String');
      }
      if (data.hasOwnProperty('locationId')) {
        obj['locationId'] = ApiClient.convertToType(data['locationId'], 'Integer');
      }
      if (data.hasOwnProperty('costs')) {
        obj['costs'] = ApiClient.convertToType(data['costs'], 'Number');
      }
      if (data.hasOwnProperty('currency')) {
        obj['currency'] = ApiClient.convertToType(data['currency'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;
  /**
   * @member {String} beginDate
   */
  exports.prototype['beginDate'] = undefined;
  /**
   * @member {String} endDate
   */
  exports.prototype['endDate'] = undefined;
  /**
   * @member {Integer} locationId
   */
  exports.prototype['locationId'] = undefined;
  /**
   * @member {Number} costs
   */
  exports.prototype['costs'] = undefined;
  /**
   * @member {String} currency
   */
  exports.prototype['currency'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],40:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LocationCoordinateDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./LocationCoordinateDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.TripActivityResponseDTO = factory(root.Travis.ApiClient, root.Travis.LocationCoordinateDTO);
  }
}(this, function(ApiClient, LocationCoordinateDTO) {
  'use strict';




  /**
   * The TripActivityResponseDTO model module.
   * @module model/TripActivityResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>TripActivityResponseDTO</code>.
   * @alias module:model/TripActivityResponseDTO
   * @class
   * @param id
   * @param name
   * @param costs
   */
  var exports = function(id, name, costs) {
    var _this = this;

    _this['id'] = id;
    _this['name'] = name;




    _this['costs'] = costs;

  };

  /**
   * Constructs a <code>TripActivityResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TripActivityResponseDTO} obj Optional instance to populate.
   * @return {module:model/TripActivityResponseDTO} The populated <code>TripActivityResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('text')) {
        obj['text'] = ApiClient.convertToType(data['text'], 'String');
      }
      if (data.hasOwnProperty('beginDate')) {
        obj['beginDate'] = ApiClient.convertToType(data['beginDate'], 'String');
      }
      if (data.hasOwnProperty('endDate')) {
        obj['endDate'] = ApiClient.convertToType(data['endDate'], 'String');
      }
      if (data.hasOwnProperty('location')) {
        obj['location'] = LocationCoordinateDTO.constructFromObject(data['location']);
      }
      if (data.hasOwnProperty('costs')) {
        obj['costs'] = ApiClient.convertToType(data['costs'], 'Number');
      }
      if (data.hasOwnProperty('currency')) {
        obj['currency'] = ApiClient.convertToType(data['currency'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {String} text
   */
  exports.prototype['text'] = undefined;
  /**
   * @member {String} beginDate
   */
  exports.prototype['beginDate'] = undefined;
  /**
   * @member {String} endDate
   */
  exports.prototype['endDate'] = undefined;
  /**
   * @member {module:model/LocationCoordinateDTO} location
   */
  exports.prototype['location'] = undefined;
  /**
   * @member {Number} costs
   */
  exports.prototype['costs'] = undefined;
  /**
   * @member {String} currency
   */
  exports.prototype['currency'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./LocationCoordinateDTO":24}],41:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LocationCoordinateDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./LocationCoordinateDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.TripOverviewResponseDTO = factory(root.Travis.ApiClient, root.Travis.LocationCoordinateDTO);
  }
}(this, function(ApiClient, LocationCoordinateDTO) {
  'use strict';




  /**
   * The TripOverviewResponseDTO model module.
   * @module model/TripOverviewResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>TripOverviewResponseDTO</code>.
   * @alias module:model/TripOverviewResponseDTO
   * @class
   * @param id
   * @param title
   * @param budget
   * @param admin
   * @param status
   */
  var exports = function(id, title, budget, admin, status) {
    var _this = this;

    _this['id'] = id;
    _this['title'] = title;



    _this['budget'] = budget;
    _this['admin'] = admin;
    _this['status'] = status;
  };

  /**
   * Constructs a <code>TripOverviewResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TripOverviewResponseDTO} obj Optional instance to populate.
   * @return {module:model/TripOverviewResponseDTO} The populated <code>TripOverviewResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('title')) {
        obj['title'] = ApiClient.convertToType(data['title'], 'String');
      }
      if (data.hasOwnProperty('location')) {
        obj['location'] = LocationCoordinateDTO.constructFromObject(data['location']);
      }
      if (data.hasOwnProperty('beginDate')) {
        obj['beginDate'] = ApiClient.convertToType(data['beginDate'], 'String');
      }
      if (data.hasOwnProperty('endDate')) {
        obj['endDate'] = ApiClient.convertToType(data['endDate'], 'String');
      }
      if (data.hasOwnProperty('budget')) {
        obj['budget'] = ApiClient.convertToType(data['budget'], 'Number');
      }
      if (data.hasOwnProperty('admin')) {
        obj['admin'] = ApiClient.convertToType(data['admin'], 'Boolean');
      }
      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} title
   */
  exports.prototype['title'] = undefined;
  /**
   * @member {module:model/LocationCoordinateDTO} location
   */
  exports.prototype['location'] = undefined;
  /**
   * @member {String} beginDate
   */
  exports.prototype['beginDate'] = undefined;
  /**
   * @member {String} endDate
   */
  exports.prototype['endDate'] = undefined;
  /**
   * @member {Number} budget
   */
  exports.prototype['budget'] = undefined;
  /**
   * @member {Boolean} admin
   */
  exports.prototype['admin'] = undefined;
  /**
   * @member {String} status
   */
  exports.prototype['status'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./LocationCoordinateDTO":24}],42:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.TripPlannerRequestDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The TripPlannerRequestDTO model module.
   * @module model/TripPlannerRequestDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>TripPlannerRequestDTO</code>.
   * @alias module:model/TripPlannerRequestDTO
   * @class
   * @param ratingCategory
   * @param like
   */
  var exports = function(ratingCategory, like) {
    var _this = this;

    _this['ratingCategory'] = ratingCategory;
    _this['like'] = like;
  };

  /**
   * Constructs a <code>TripPlannerRequestDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TripPlannerRequestDTO} obj Optional instance to populate.
   * @return {module:model/TripPlannerRequestDTO} The populated <code>TripPlannerRequestDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('ratingCategory')) {
        obj['ratingCategory'] = ApiClient.convertToType(data['ratingCategory'], 'String');
      }
      if (data.hasOwnProperty('like')) {
        obj['like'] = ApiClient.convertToType(data['like'], 'Boolean');
      }
    }
    return obj;
  }

  /**
   * @member {String} ratingCategory
   */
  exports.prototype['ratingCategory'] = undefined;
  /**
   * @member {Boolean} like
   */
  exports.prototype['like'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],43:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LocationCoordinateDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./LocationCoordinateDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.TripPlannerResponseDTO = factory(root.Travis.ApiClient, root.Travis.LocationCoordinateDTO);
  }
}(this, function(ApiClient, LocationCoordinateDTO) {
  'use strict';




  /**
   * The TripPlannerResponseDTO model module.
   * @module model/TripPlannerResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>TripPlannerResponseDTO</code>.
   * @alias module:model/TripPlannerResponseDTO
   * @class
   * @param location
   * @param match
   */
  var exports = function(location, match) {
    var _this = this;

    _this['location'] = location;
    _this['match'] = match;
  };

  /**
   * Constructs a <code>TripPlannerResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TripPlannerResponseDTO} obj Optional instance to populate.
   * @return {module:model/TripPlannerResponseDTO} The populated <code>TripPlannerResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('location')) {
        obj['location'] = LocationCoordinateDTO.constructFromObject(data['location']);
      }
      if (data.hasOwnProperty('match')) {
        obj['match'] = ApiClient.convertToType(data['match'], 'Number');
      }
    }
    return obj;
  }

  /**
   * @member {module:model/LocationCoordinateDTO} location
   */
  exports.prototype['location'] = undefined;
  /**
   * @member {Number} match
   */
  exports.prototype['match'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./LocationCoordinateDTO":24}],44:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.TripRequestDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The TripRequestDTO model module.
   * @module model/TripRequestDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>TripRequestDTO</code>.
   * @alias module:model/TripRequestDTO
   * @class
   */
  var exports = function() {
    var _this = this;











  };

  /**
   * Constructs a <code>TripRequestDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TripRequestDTO} obj Optional instance to populate.
   * @return {module:model/TripRequestDTO} The populated <code>TripRequestDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('title')) {
        obj['title'] = ApiClient.convertToType(data['title'], 'String');
      }
      if (data.hasOwnProperty('locationId')) {
        obj['locationId'] = ApiClient.convertToType(data['locationId'], 'Integer');
      }
      if (data.hasOwnProperty('beginDate')) {
        obj['beginDate'] = ApiClient.convertToType(data['beginDate'], 'String');
      }
      if (data.hasOwnProperty('endDate')) {
        obj['endDate'] = ApiClient.convertToType(data['endDate'], 'String');
      }
      if (data.hasOwnProperty('budget')) {
        obj['budget'] = ApiClient.convertToType(data['budget'], 'Number');
      }
      if (data.hasOwnProperty('currency')) {
        obj['currency'] = ApiClient.convertToType(data['currency'], 'String');
      }
      if (data.hasOwnProperty('timezone')) {
        obj['timezone'] = ApiClient.convertToType(data['timezone'], 'String');
      }
      if (data.hasOwnProperty('packingList')) {
        obj['packingList'] = ApiClient.convertToType(data['packingList'], 'String');
      }
      if (data.hasOwnProperty('tripInfo')) {
        obj['tripInfo'] = ApiClient.convertToType(data['tripInfo'], 'String');
      }
      if (data.hasOwnProperty('description')) {
        obj['description'] = ApiClient.convertToType(data['description'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} title
   */
  exports.prototype['title'] = undefined;
  /**
   * @member {Integer} locationId
   */
  exports.prototype['locationId'] = undefined;
  /**
   * @member {String} beginDate
   */
  exports.prototype['beginDate'] = undefined;
  /**
   * @member {String} endDate
   */
  exports.prototype['endDate'] = undefined;
  /**
   * @member {Number} budget
   */
  exports.prototype['budget'] = undefined;
  /**
   * @member {String} currency
   */
  exports.prototype['currency'] = undefined;
  /**
   * @member {String} timezone
   */
  exports.prototype['timezone'] = undefined;
  /**
   * @member {String} packingList
   */
  exports.prototype['packingList'] = undefined;
  /**
   * @member {String} tripInfo
   */
  exports.prototype['tripInfo'] = undefined;
  /**
   * @member {String} description
   */
  exports.prototype['description'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],45:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LocationCoordinateDTO', 'model/TripActivityResponseDTO', 'model/TripUserResponseDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./LocationCoordinateDTO'), require('./TripActivityResponseDTO'), require('./TripUserResponseDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.TripResponseDTO = factory(root.Travis.ApiClient, root.Travis.LocationCoordinateDTO, root.Travis.TripActivityResponseDTO, root.Travis.TripUserResponseDTO);
  }
}(this, function(ApiClient, LocationCoordinateDTO, TripActivityResponseDTO, TripUserResponseDTO) {
  'use strict';




  /**
   * The TripResponseDTO model module.
   * @module model/TripResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>TripResponseDTO</code>.
   * @alias module:model/TripResponseDTO
   * @class
   * @param id
   * @param title
   * @param users
   * @param budget
   * @param activities
   */
  var exports = function(id, title, users, budget, activities) {
    var _this = this;

    _this['id'] = id;
    _this['title'] = title;

    _this['users'] = users;


    _this['budget'] = budget;





    _this['activities'] = activities;
  };

  /**
   * Constructs a <code>TripResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TripResponseDTO} obj Optional instance to populate.
   * @return {module:model/TripResponseDTO} The populated <code>TripResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('title')) {
        obj['title'] = ApiClient.convertToType(data['title'], 'String');
      }
      if (data.hasOwnProperty('location')) {
        obj['location'] = LocationCoordinateDTO.constructFromObject(data['location']);
      }
      if (data.hasOwnProperty('users')) {
        obj['users'] = ApiClient.convertToType(data['users'], [TripUserResponseDTO]);
      }
      if (data.hasOwnProperty('beginDate')) {
        obj['beginDate'] = ApiClient.convertToType(data['beginDate'], 'String');
      }
      if (data.hasOwnProperty('endDate')) {
        obj['endDate'] = ApiClient.convertToType(data['endDate'], 'String');
      }
      if (data.hasOwnProperty('budget')) {
        obj['budget'] = ApiClient.convertToType(data['budget'], 'Number');
      }
      if (data.hasOwnProperty('currency')) {
        obj['currency'] = ApiClient.convertToType(data['currency'], 'String');
      }
      if (data.hasOwnProperty('timezone')) {
        obj['timezone'] = ApiClient.convertToType(data['timezone'], 'String');
      }
      if (data.hasOwnProperty('packingList')) {
        obj['packingList'] = ApiClient.convertToType(data['packingList'], 'String');
      }
      if (data.hasOwnProperty('tripInfo')) {
        obj['tripInfo'] = ApiClient.convertToType(data['tripInfo'], 'String');
      }
      if (data.hasOwnProperty('description')) {
        obj['description'] = ApiClient.convertToType(data['description'], 'String');
      }
      if (data.hasOwnProperty('activities')) {
        obj['activities'] = ApiClient.convertToType(data['activities'], [TripActivityResponseDTO]);
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} title
   */
  exports.prototype['title'] = undefined;
  /**
   * @member {module:model/LocationCoordinateDTO} location
   */
  exports.prototype['location'] = undefined;
  /**
   * @member {Array.<module:model/TripUserResponseDTO>} users
   */
  exports.prototype['users'] = undefined;
  /**
   * @member {String} beginDate
   */
  exports.prototype['beginDate'] = undefined;
  /**
   * @member {String} endDate
   */
  exports.prototype['endDate'] = undefined;
  /**
   * @member {Number} budget
   */
  exports.prototype['budget'] = undefined;
  /**
   * @member {String} currency
   */
  exports.prototype['currency'] = undefined;
  /**
   * @member {String} timezone
   */
  exports.prototype['timezone'] = undefined;
  /**
   * @member {String} packingList
   */
  exports.prototype['packingList'] = undefined;
  /**
   * @member {String} tripInfo
   */
  exports.prototype['tripInfo'] = undefined;
  /**
   * @member {String} description
   */
  exports.prototype['description'] = undefined;
  /**
   * @member {Array.<module:model/TripActivityResponseDTO>} activities
   */
  exports.prototype['activities'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./LocationCoordinateDTO":24,"./TripActivityResponseDTO":40,"./TripUserResponseDTO":46}],46:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/UserDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./UserDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.TripUserResponseDTO = factory(root.Travis.ApiClient, root.Travis.UserDTO);
  }
}(this, function(ApiClient, UserDTO) {
  'use strict';




  /**
   * The TripUserResponseDTO model module.
   * @module model/TripUserResponseDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>TripUserResponseDTO</code>.
   * @alias module:model/TripUserResponseDTO
   * @class
   */
  var exports = function() {
    var _this = this;




  };

  /**
   * Constructs a <code>TripUserResponseDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TripUserResponseDTO} obj Optional instance to populate.
   * @return {module:model/TripUserResponseDTO} The populated <code>TripUserResponseDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('user')) {
        obj['user'] = UserDTO.constructFromObject(data['user']);
      }
      if (data.hasOwnProperty('admin')) {
        obj['admin'] = ApiClient.convertToType(data['admin'], 'Boolean');
      }
      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {module:model/UserDTO} user
   */
  exports.prototype['user'] = undefined;
  /**
   * @member {Boolean} admin
   */
  exports.prototype['admin'] = undefined;
  /**
   * @member {String} status
   */
  exports.prototype['status'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./UserDTO":47}],47:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.UserDTO = factory(root.Travis.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The UserDTO model module.
   * @module model/UserDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>UserDTO</code>.
   * @alias module:model/UserDTO
   * @class
   * @param id
   * @param firstName
   * @param lastName
   * @param loginImageUrl
   */
  var exports = function(id, firstName, lastName, loginImageUrl) {
    var _this = this;

    _this['id'] = id;
    _this['firstName'] = firstName;
    _this['lastName'] = lastName;

    _this['loginImageUrl'] = loginImageUrl;
  };

  /**
   * Constructs a <code>UserDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UserDTO} obj Optional instance to populate.
   * @return {module:model/UserDTO} The populated <code>UserDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('firstName')) {
        obj['firstName'] = ApiClient.convertToType(data['firstName'], 'String');
      }
      if (data.hasOwnProperty('lastName')) {
        obj['lastName'] = ApiClient.convertToType(data['lastName'], 'String');
      }
      if (data.hasOwnProperty('birthday')) {
        obj['birthday'] = ApiClient.convertToType(data['birthday'], 'String');
      }
      if (data.hasOwnProperty('loginImageUrl')) {
        obj['loginImageUrl'] = ApiClient.convertToType(data['loginImageUrl'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} firstName
   */
  exports.prototype['firstName'] = undefined;
  /**
   * @member {String} lastName
   */
  exports.prototype['lastName'] = undefined;
  /**
   * @member {String} birthday
   */
  exports.prototype['birthday'] = undefined;
  /**
   * @member {String} loginImageUrl
   */
  exports.prototype['loginImageUrl'] = undefined;




  return exports;
}));



},{"../ApiClient":1}],48:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/DiaryEntryDTO', 'model/UserDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./DiaryEntryDTO'), require('./UserDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.UserMainPageEntryDTO = factory(root.Travis.ApiClient, root.Travis.DiaryEntryDTO, root.Travis.UserDTO);
  }
}(this, function(ApiClient, DiaryEntryDTO, UserDTO) {
  'use strict';




  /**
   * The UserMainPageEntryDTO model module.
   * @module model/UserMainPageEntryDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>UserMainPageEntryDTO</code>.
   * @alias module:model/UserMainPageEntryDTO
   * @class
   * @param user
   * @param diaryEntry
   */
  var exports = function(user, diaryEntry) {
    var _this = this;

    _this['user'] = user;
    _this['diaryEntry'] = diaryEntry;
  };

  /**
   * Constructs a <code>UserMainPageEntryDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UserMainPageEntryDTO} obj Optional instance to populate.
   * @return {module:model/UserMainPageEntryDTO} The populated <code>UserMainPageEntryDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('user')) {
        obj['user'] = UserDTO.constructFromObject(data['user']);
      }
      if (data.hasOwnProperty('diaryEntry')) {
        obj['diaryEntry'] = DiaryEntryDTO.constructFromObject(data['diaryEntry']);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/UserDTO} user
   */
  exports.prototype['user'] = undefined;
  /**
   * @member {module:model/DiaryEntryDTO} diaryEntry
   */
  exports.prototype['diaryEntry'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./DiaryEntryDTO":17,"./UserDTO":47}],49:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LocationResponseDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./LocationResponseDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.UserProfileDTO = factory(root.Travis.ApiClient, root.Travis.LocationResponseDTO);
  }
}(this, function(ApiClient, LocationResponseDTO) {
  'use strict';




  /**
   * The UserProfileDTO model module.
   * @module model/UserProfileDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>UserProfileDTO</code>.
   * @alias module:model/UserProfileDTO
   * @class
   * @param id
   * @param firstName
   * @param lastName
   * @param followers
   * @param following
   * @param follow
   */
  var exports = function(id, firstName, lastName, followers, following, follow) {
    var _this = this;

    _this['id'] = id;
    _this['firstName'] = firstName;
    _this['lastName'] = lastName;



    _this['followers'] = followers;
    _this['following'] = following;
    _this['follow'] = follow;






  };

  /**
   * Constructs a <code>UserProfileDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UserProfileDTO} obj Optional instance to populate.
   * @return {module:model/UserProfileDTO} The populated <code>UserProfileDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Integer');
      }
      if (data.hasOwnProperty('firstName')) {
        obj['firstName'] = ApiClient.convertToType(data['firstName'], 'String');
      }
      if (data.hasOwnProperty('lastName')) {
        obj['lastName'] = ApiClient.convertToType(data['lastName'], 'String');
      }
      if (data.hasOwnProperty('birthday')) {
        obj['birthday'] = ApiClient.convertToType(data['birthday'], 'String');
      }
      if (data.hasOwnProperty('loginProfileUrl')) {
        obj['loginProfileUrl'] = ApiClient.convertToType(data['loginProfileUrl'], 'String');
      }
      if (data.hasOwnProperty('loginImageUrl')) {
        obj['loginImageUrl'] = ApiClient.convertToType(data['loginImageUrl'], 'String');
      }
      if (data.hasOwnProperty('followers')) {
        obj['followers'] = ApiClient.convertToType(data['followers'], 'Integer');
      }
      if (data.hasOwnProperty('following')) {
        obj['following'] = ApiClient.convertToType(data['following'], 'Integer');
      }
      if (data.hasOwnProperty('follow')) {
        obj['follow'] = ApiClient.convertToType(data['follow'], 'Boolean');
      }
      if (data.hasOwnProperty('shortText')) {
        obj['shortText'] = ApiClient.convertToType(data['shortText'], 'String');
      }
      if (data.hasOwnProperty('longText')) {
        obj['longText'] = ApiClient.convertToType(data['longText'], 'String');
      }
      if (data.hasOwnProperty('profileTags')) {
        obj['profileTags'] = ApiClient.convertToType(data['profileTags'], ['String']);
      }
      if (data.hasOwnProperty('homeLocation')) {
        obj['homeLocation'] = LocationResponseDTO.constructFromObject(data['homeLocation']);
      }
      if (data.hasOwnProperty('currentLocation')) {
        obj['currentLocation'] = LocationResponseDTO.constructFromObject(data['currentLocation']);
      }
      if (data.hasOwnProperty('nextLocation')) {
        obj['nextLocation'] = LocationResponseDTO.constructFromObject(data['nextLocation']);
      }
    }
    return obj;
  }

  /**
   * @member {Integer} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} firstName
   */
  exports.prototype['firstName'] = undefined;
  /**
   * @member {String} lastName
   */
  exports.prototype['lastName'] = undefined;
  /**
   * @member {String} birthday
   */
  exports.prototype['birthday'] = undefined;
  /**
   * @member {String} loginProfileUrl
   */
  exports.prototype['loginProfileUrl'] = undefined;
  /**
   * @member {String} loginImageUrl
   */
  exports.prototype['loginImageUrl'] = undefined;
  /**
   * @member {Integer} followers
   */
  exports.prototype['followers'] = undefined;
  /**
   * @member {Integer} following
   */
  exports.prototype['following'] = undefined;
  /**
   * @member {Boolean} follow
   */
  exports.prototype['follow'] = undefined;
  /**
   * @member {String} shortText
   */
  exports.prototype['shortText'] = undefined;
  /**
   * @member {String} longText
   */
  exports.prototype['longText'] = undefined;
  /**
   * @member {Array.<String>} profileTags
   */
  exports.prototype['profileTags'] = undefined;
  /**
   * @member {module:model/LocationResponseDTO} homeLocation
   */
  exports.prototype['homeLocation'] = undefined;
  /**
   * @member {module:model/LocationResponseDTO} currentLocation
   */
  exports.prototype['currentLocation'] = undefined;
  /**
   * @member {module:model/LocationResponseDTO} nextLocation
   */
  exports.prototype['nextLocation'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./LocationResponseDTO":26}],50:[function(require,module,exports){
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/UserDTO'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./UserDTO'));
  } else {
    // Browser globals (root is window)
    if (!root.Travis) {
      root.Travis = {};
    }
    root.Travis.UserSearchDTO = factory(root.Travis.ApiClient, root.Travis.UserDTO);
  }
}(this, function(ApiClient, UserDTO) {
  'use strict';




  /**
   * The UserSearchDTO model module.
   * @module model/UserSearchDTO
   * @version 0.0.1
   */

  /**
   * Constructs a new <code>UserSearchDTO</code>.
   * @alias module:model/UserSearchDTO
   * @class
   * @param searchResult
   */
  var exports = function(searchResult) {
    var _this = this;

    _this['searchResult'] = searchResult;
  };

  /**
   * Constructs a <code>UserSearchDTO</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UserSearchDTO} obj Optional instance to populate.
   * @return {module:model/UserSearchDTO} The populated <code>UserSearchDTO</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('searchResult')) {
        obj['searchResult'] = ApiClient.convertToType(data['searchResult'], [UserDTO]);
      }
    }
    return obj;
  }

  /**
   * @member {Array.<module:model/UserDTO>} searchResult
   */
  exports.prototype['searchResult'] = undefined;




  return exports;
}));



},{"../ApiClient":1,"./UserDTO":47}],51:[function(require,module,exports){
/**
 * Module dependencies.
 */

var Emitter = require('emitter');
var reduce = require('reduce');
var requestBase = require('./request-base');
var isObject = require('./is-object');

/**
 * Root reference for iframes.
 */

var root;
if (typeof window !== 'undefined') { // Browser window
  root = window;
} else if (typeof self !== 'undefined') { // Web Worker
  root = self;
} else { // Other environments
  root = this;
}

/**
 * Noop.
 */

function noop(){};

/**
 * Check if `obj` is a host object,
 * we don't want to serialize these :)
 *
 * TODO: future proof, move to compoent land
 *
 * @param {Object} obj
 * @return {Boolean}
 * @api private
 */

function isHost(obj) {
  var str = {}.toString.call(obj);

  switch (str) {
    case '[object File]':
    case '[object Blob]':
    case '[object FormData]':
      return true;
    default:
      return false;
  }
}

/**
 * Expose `request`.
 */

var request = module.exports = require('./request').bind(null, Request);

/**
 * Determine XHR.
 */

request.getXHR = function () {
  if (root.XMLHttpRequest
      && (!root.location || 'file:' != root.location.protocol
          || !root.ActiveXObject)) {
    return new XMLHttpRequest;
  } else {
    try { return new ActiveXObject('Microsoft.XMLHTTP'); } catch(e) {}
    try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch(e) {}
    try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch(e) {}
    try { return new ActiveXObject('Msxml2.XMLHTTP'); } catch(e) {}
  }
  return false;
};

/**
 * Removes leading and trailing whitespace, added to support IE.
 *
 * @param {String} s
 * @return {String}
 * @api private
 */

var trim = ''.trim
  ? function(s) { return s.trim(); }
  : function(s) { return s.replace(/(^\s*|\s*$)/g, ''); };

/**
 * Serialize the given `obj`.
 *
 * @param {Object} obj
 * @return {String}
 * @api private
 */

function serialize(obj) {
  if (!isObject(obj)) return obj;
  var pairs = [];
  for (var key in obj) {
    if (null != obj[key]) {
      pushEncodedKeyValuePair(pairs, key, obj[key]);
        }
      }
  return pairs.join('&');
}

/**
 * Helps 'serialize' with serializing arrays.
 * Mutates the pairs array.
 *
 * @param {Array} pairs
 * @param {String} key
 * @param {Mixed} val
 */

function pushEncodedKeyValuePair(pairs, key, val) {
  if (Array.isArray(val)) {
    return val.forEach(function(v) {
      pushEncodedKeyValuePair(pairs, key, v);
    });
  }
  pairs.push(encodeURIComponent(key)
    + '=' + encodeURIComponent(val));
}

/**
 * Expose serialization method.
 */

 request.serializeObject = serialize;

 /**
  * Parse the given x-www-form-urlencoded `str`.
  *
  * @param {String} str
  * @return {Object}
  * @api private
  */

function parseString(str) {
  var obj = {};
  var pairs = str.split('&');
  var parts;
  var pair;

  for (var i = 0, len = pairs.length; i < len; ++i) {
    pair = pairs[i];
    parts = pair.split('=');
    obj[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
  }

  return obj;
}

/**
 * Expose parser.
 */

request.parseString = parseString;

/**
 * Default MIME type map.
 *
 *     superagent.types.xml = 'application/xml';
 *
 */

request.types = {
  html: 'text/html',
  json: 'application/json',
  xml: 'application/xml',
  urlencoded: 'application/x-www-form-urlencoded',
  'form': 'application/x-www-form-urlencoded',
  'form-data': 'application/x-www-form-urlencoded'
};

/**
 * Default serialization map.
 *
 *     superagent.serialize['application/xml'] = function(obj){
 *       return 'generated xml here';
 *     };
 *
 */

 request.serialize = {
   'application/x-www-form-urlencoded': serialize,
   'application/json': JSON.stringify
 };

 /**
  * Default parsers.
  *
  *     superagent.parse['application/xml'] = function(str){
  *       return { object parsed from str };
  *     };
  *
  */

request.parse = {
  'application/x-www-form-urlencoded': parseString,
  'application/json': JSON.parse
};

/**
 * Parse the given header `str` into
 * an object containing the mapped fields.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */

function parseHeader(str) {
  var lines = str.split(/\r?\n/);
  var fields = {};
  var index;
  var line;
  var field;
  var val;

  lines.pop(); // trailing CRLF

  for (var i = 0, len = lines.length; i < len; ++i) {
    line = lines[i];
    index = line.indexOf(':');
    field = line.slice(0, index).toLowerCase();
    val = trim(line.slice(index + 1));
    fields[field] = val;
  }

  return fields;
}

/**
 * Check if `mime` is json or has +json structured syntax suffix.
 *
 * @param {String} mime
 * @return {Boolean}
 * @api private
 */

function isJSON(mime) {
  return /[\/+]json\b/.test(mime);
}

/**
 * Return the mime type for the given `str`.
 *
 * @param {String} str
 * @return {String}
 * @api private
 */

function type(str){
  return str.split(/ *; */).shift();
};

/**
 * Return header field parameters.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */

function params(str){
  return reduce(str.split(/ *; */), function(obj, str){
    var parts = str.split(/ *= */)
      , key = parts.shift()
      , val = parts.shift();

    if (key && val) obj[key] = val;
    return obj;
  }, {});
};

/**
 * Initialize a new `Response` with the given `xhr`.
 *
 *  - set flags (.ok, .error, etc)
 *  - parse header
 *
 * Examples:
 *
 *  Aliasing `superagent` as `request` is nice:
 *
 *      request = superagent;
 *
 *  We can use the promise-like API, or pass callbacks:
 *
 *      request.get('/').end(function(res){});
 *      request.get('/', function(res){});
 *
 *  Sending data can be chained:
 *
 *      request
 *        .post('/user')
 *        .send({ name: 'tj' })
 *        .end(function(res){});
 *
 *  Or passed to `.send()`:
 *
 *      request
 *        .post('/user')
 *        .send({ name: 'tj' }, function(res){});
 *
 *  Or passed to `.post()`:
 *
 *      request
 *        .post('/user', { name: 'tj' })
 *        .end(function(res){});
 *
 * Or further reduced to a single call for simple cases:
 *
 *      request
 *        .post('/user', { name: 'tj' }, function(res){});
 *
 * @param {XMLHTTPRequest} xhr
 * @param {Object} options
 * @api private
 */

function Response(req, options) {
  options = options || {};
  this.req = req;
  this.xhr = this.req.xhr;
  // responseText is accessible only if responseType is '' or 'text' and on older browsers
  this.text = ((this.req.method !='HEAD' && (this.xhr.responseType === '' || this.xhr.responseType === 'text')) || typeof this.xhr.responseType === 'undefined')
     ? this.xhr.responseText
     : null;
  this.statusText = this.req.xhr.statusText;
  this.setStatusProperties(this.xhr.status);
  this.header = this.headers = parseHeader(this.xhr.getAllResponseHeaders());
  // getAllResponseHeaders sometimes falsely returns "" for CORS requests, but
  // getResponseHeader still works. so we get content-type even if getting
  // other headers fails.
  this.header['content-type'] = this.xhr.getResponseHeader('content-type');
  this.setHeaderProperties(this.header);
  this.body = this.req.method != 'HEAD'
    ? this.parseBody(this.text ? this.text : this.xhr.response)
    : null;
}

/**
 * Get case-insensitive `field` value.
 *
 * @param {String} field
 * @return {String}
 * @api public
 */

Response.prototype.get = function(field){
  return this.header[field.toLowerCase()];
};

/**
 * Set header related properties:
 *
 *   - `.type` the content type without params
 *
 * A response of "Content-Type: text/plain; charset=utf-8"
 * will provide you with a `.type` of "text/plain".
 *
 * @param {Object} header
 * @api private
 */

Response.prototype.setHeaderProperties = function(header){
  // content-type
  var ct = this.header['content-type'] || '';
  this.type = type(ct);

  // params
  var obj = params(ct);
  for (var key in obj) this[key] = obj[key];
};

/**
 * Parse the given body `str`.
 *
 * Used for auto-parsing of bodies. Parsers
 * are defined on the `superagent.parse` object.
 *
 * @param {String} str
 * @return {Mixed}
 * @api private
 */

Response.prototype.parseBody = function(str){
  var parse = request.parse[this.type];
  if (!parse && isJSON(this.type)) {
    parse = request.parse['application/json'];
  }
  return parse && str && (str.length || str instanceof Object)
    ? parse(str)
    : null;
};

/**
 * Set flags such as `.ok` based on `status`.
 *
 * For example a 2xx response will give you a `.ok` of __true__
 * whereas 5xx will be __false__ and `.error` will be __true__. The
 * `.clientError` and `.serverError` are also available to be more
 * specific, and `.statusType` is the class of error ranging from 1..5
 * sometimes useful for mapping respond colors etc.
 *
 * "sugar" properties are also defined for common cases. Currently providing:
 *
 *   - .noContent
 *   - .badRequest
 *   - .unauthorized
 *   - .notAcceptable
 *   - .notFound
 *
 * @param {Number} status
 * @api private
 */

Response.prototype.setStatusProperties = function(status){
  // handle IE9 bug: http://stackoverflow.com/questions/10046972/msie-returns-status-code-of-1223-for-ajax-request
  if (status === 1223) {
    status = 204;
  }

  var type = status / 100 | 0;

  // status / class
  this.status = this.statusCode = status;
  this.statusType = type;

  // basics
  this.info = 1 == type;
  this.ok = 2 == type;
  this.clientError = 4 == type;
  this.serverError = 5 == type;
  this.error = (4 == type || 5 == type)
    ? this.toError()
    : false;

  // sugar
  this.accepted = 202 == status;
  this.noContent = 204 == status;
  this.badRequest = 400 == status;
  this.unauthorized = 401 == status;
  this.notAcceptable = 406 == status;
  this.notFound = 404 == status;
  this.forbidden = 403 == status;
};

/**
 * Return an `Error` representative of this response.
 *
 * @return {Error}
 * @api public
 */

Response.prototype.toError = function(){
  var req = this.req;
  var method = req.method;
  var url = req.url;

  var msg = 'cannot ' + method + ' ' + url + ' (' + this.status + ')';
  var err = new Error(msg);
  err.status = this.status;
  err.method = method;
  err.url = url;

  return err;
};

/**
 * Expose `Response`.
 */

request.Response = Response;

/**
 * Initialize a new `Request` with the given `method` and `url`.
 *
 * @param {String} method
 * @param {String} url
 * @api public
 */

function Request(method, url) {
  var self = this;
  this._query = this._query || [];
  this.method = method;
  this.url = url;
  this.header = {}; // preserves header name case
  this._header = {}; // coerces header names to lowercase
  this.on('end', function(){
    var err = null;
    var res = null;

    try {
      res = new Response(self);
    } catch(e) {
      err = new Error('Parser is unable to parse the response');
      err.parse = true;
      err.original = e;
      // issue #675: return the raw response if the response parsing fails
      err.rawResponse = self.xhr && self.xhr.responseText ? self.xhr.responseText : null;
      // issue #876: return the http status code if the response parsing fails
      err.statusCode = self.xhr && self.xhr.status ? self.xhr.status : null;
      return self.callback(err);
    }

    self.emit('response', res);

    if (err) {
      return self.callback(err, res);
    }

    if (res.status >= 200 && res.status < 300) {
      return self.callback(err, res);
    }

    var new_err = new Error(res.statusText || 'Unsuccessful HTTP response');
    new_err.original = err;
    new_err.response = res;
    new_err.status = res.status;

    self.callback(new_err, res);
  });
}

/**
 * Mixin `Emitter` and `requestBase`.
 */

Emitter(Request.prototype);
for (var key in requestBase) {
  Request.prototype[key] = requestBase[key];
}

/**
 * Abort the request, and clear potential timeout.
 *
 * @return {Request}
 * @api public
 */

Request.prototype.abort = function(){
  if (this.aborted) return;
  this.aborted = true;
  this.xhr.abort();
  this.clearTimeout();
  this.emit('abort');
  return this;
};

/**
 * Set Content-Type to `type`, mapping values from `request.types`.
 *
 * Examples:
 *
 *      superagent.types.xml = 'application/xml';
 *
 *      request.post('/')
 *        .type('xml')
 *        .send(xmlstring)
 *        .end(callback);
 *
 *      request.post('/')
 *        .type('application/xml')
 *        .send(xmlstring)
 *        .end(callback);
 *
 * @param {String} type
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.type = function(type){
  this.set('Content-Type', request.types[type] || type);
  return this;
};

/**
 * Set responseType to `val`. Presently valid responseTypes are 'blob' and 
 * 'arraybuffer'.
 *
 * Examples:
 *
 *      req.get('/')
 *        .responseType('blob')
 *        .end(callback);
 *
 * @param {String} val
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.responseType = function(val){
  this._responseType = val;
  return this;
};

/**
 * Set Accept to `type`, mapping values from `request.types`.
 *
 * Examples:
 *
 *      superagent.types.json = 'application/json';
 *
 *      request.get('/agent')
 *        .accept('json')
 *        .end(callback);
 *
 *      request.get('/agent')
 *        .accept('application/json')
 *        .end(callback);
 *
 * @param {String} accept
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.accept = function(type){
  this.set('Accept', request.types[type] || type);
  return this;
};

/**
 * Set Authorization field value with `user` and `pass`.
 *
 * @param {String} user
 * @param {String} pass
 * @param {Object} options with 'type' property 'auto' or 'basic' (default 'basic')
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.auth = function(user, pass, options){
  if (!options) {
    options = {
      type: 'basic'
    }
  }

  switch (options.type) {
    case 'basic':
      var str = btoa(user + ':' + pass);
      this.set('Authorization', 'Basic ' + str);
    break;

    case 'auto':
      this.username = user;
      this.password = pass;
    break;
  }
  return this;
};

/**
* Add query-string `val`.
*
* Examples:
*
*   request.get('/shoes')
*     .query('size=10')
*     .query({ color: 'blue' })
*
* @param {Object|String} val
* @return {Request} for chaining
* @api public
*/

Request.prototype.query = function(val){
  if ('string' != typeof val) val = serialize(val);
  if (val) this._query.push(val);
  return this;
};

/**
 * Queue the given `file` as an attachment to the specified `field`,
 * with optional `filename`.
 *
 * ``` js
 * request.post('/upload')
 *   .attach(new Blob(['<a id="a"><b id="b">hey!</b></a>'], { type: "text/html"}))
 *   .end(callback);
 * ```
 *
 * @param {String} field
 * @param {Blob|File} file
 * @param {String} filename
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.attach = function(field, file, filename){
  this._getFormData().append(field, file, filename || file.name);
  return this;
};

Request.prototype._getFormData = function(){
  if (!this._formData) {
    this._formData = new root.FormData();
  }
  return this._formData;
};

/**
 * Send `data` as the request body, defaulting the `.type()` to "json" when
 * an object is given.
 *
 * Examples:
 *
 *       // manual json
 *       request.post('/user')
 *         .type('json')
 *         .send('{"name":"tj"}')
 *         .end(callback)
 *
 *       // auto json
 *       request.post('/user')
 *         .send({ name: 'tj' })
 *         .end(callback)
 *
 *       // manual x-www-form-urlencoded
 *       request.post('/user')
 *         .type('form')
 *         .send('name=tj')
 *         .end(callback)
 *
 *       // auto x-www-form-urlencoded
 *       request.post('/user')
 *         .type('form')
 *         .send({ name: 'tj' })
 *         .end(callback)
 *
 *       // defaults to x-www-form-urlencoded
  *      request.post('/user')
  *        .send('name=tobi')
  *        .send('species=ferret')
  *        .end(callback)
 *
 * @param {String|Object} data
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.send = function(data){
  var obj = isObject(data);
  var type = this._header['content-type'];

  // merge
  if (obj && isObject(this._data)) {
    for (var key in data) {
      this._data[key] = data[key];
    }
  } else if ('string' == typeof data) {
    if (!type) this.type('form');
    type = this._header['content-type'];
    if ('application/x-www-form-urlencoded' == type) {
      this._data = this._data
        ? this._data + '&' + data
        : data;
    } else {
      this._data = (this._data || '') + data;
    }
  } else {
    this._data = data;
  }

  if (!obj || isHost(data)) return this;
  if (!type) this.type('json');
  return this;
};

/**
 * @deprecated
 */
Response.prototype.parse = function serialize(fn){
  if (root.console) {
    console.warn("Client-side parse() method has been renamed to serialize(). This method is not compatible with superagent v2.0");
  }
  this.serialize(fn);
  return this;
};

Response.prototype.serialize = function serialize(fn){
  this._parser = fn;
  return this;
};

/**
 * Invoke the callback with `err` and `res`
 * and handle arity check.
 *
 * @param {Error} err
 * @param {Response} res
 * @api private
 */

Request.prototype.callback = function(err, res){
  var fn = this._callback;
  this.clearTimeout();
  fn(err, res);
};

/**
 * Invoke callback with x-domain error.
 *
 * @api private
 */

Request.prototype.crossDomainError = function(){
  var err = new Error('Request has been terminated\nPossible causes: the network is offline, Origin is not allowed by Access-Control-Allow-Origin, the page is being unloaded, etc.');
  err.crossDomain = true;

  err.status = this.status;
  err.method = this.method;
  err.url = this.url;

  this.callback(err);
};

/**
 * Invoke callback with timeout error.
 *
 * @api private
 */

Request.prototype.timeoutError = function(){
  var timeout = this._timeout;
  var err = new Error('timeout of ' + timeout + 'ms exceeded');
  err.timeout = timeout;
  this.callback(err);
};

/**
 * Enable transmission of cookies with x-domain requests.
 *
 * Note that for this to work the origin must not be
 * using "Access-Control-Allow-Origin" with a wildcard,
 * and also must set "Access-Control-Allow-Credentials"
 * to "true".
 *
 * @api public
 */

Request.prototype.withCredentials = function(){
  this._withCredentials = true;
  return this;
};

/**
 * Initiate request, invoking callback `fn(res)`
 * with an instanceof `Response`.
 *
 * @param {Function} fn
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.end = function(fn){
  var self = this;
  var xhr = this.xhr = request.getXHR();
  var query = this._query.join('&');
  var timeout = this._timeout;
  var data = this._formData || this._data;

  // store callback
  this._callback = fn || noop;

  // state change
  xhr.onreadystatechange = function(){
    if (4 != xhr.readyState) return;

    // In IE9, reads to any property (e.g. status) off of an aborted XHR will
    // result in the error "Could not complete the operation due to error c00c023f"
    var status;
    try { status = xhr.status } catch(e) { status = 0; }

    if (0 == status) {
      if (self.timedout) return self.timeoutError();
      if (self.aborted) return;
      return self.crossDomainError();
    }
    self.emit('end');
  };

  // progress
  var handleProgress = function(e){
    if (e.total > 0) {
      e.percent = e.loaded / e.total * 100;
    }
    e.direction = 'download';
    self.emit('progress', e);
  };
  if (this.hasListeners('progress')) {
    xhr.onprogress = handleProgress;
  }
  try {
    if (xhr.upload && this.hasListeners('progress')) {
      xhr.upload.onprogress = handleProgress;
    }
  } catch(e) {
    // Accessing xhr.upload fails in IE from a web worker, so just pretend it doesn't exist.
    // Reported here:
    // https://connect.microsoft.com/IE/feedback/details/837245/xmlhttprequest-upload-throws-invalid-argument-when-used-from-web-worker-context
  }

  // timeout
  if (timeout && !this._timer) {
    this._timer = setTimeout(function(){
      self.timedout = true;
      self.abort();
    }, timeout);
  }

  // querystring
  if (query) {
    query = request.serializeObject(query);
    this.url += ~this.url.indexOf('?')
      ? '&' + query
      : '?' + query;
  }

  // initiate request
  if (this.username && this.password) {
    xhr.open(this.method, this.url, true, this.username, this.password);
  } else {
    xhr.open(this.method, this.url, true);
  }

  // CORS
  if (this._withCredentials) xhr.withCredentials = true;

  // body
  if ('GET' != this.method && 'HEAD' != this.method && 'string' != typeof data && !isHost(data)) {
    // serialize stuff
    var contentType = this._header['content-type'];
    var serialize = this._parser || request.serialize[contentType ? contentType.split(';')[0] : ''];
    if (!serialize && isJSON(contentType)) serialize = request.serialize['application/json'];
    if (serialize) data = serialize(data);
  }

  // set header fields
  for (var field in this.header) {
    if (null == this.header[field]) continue;
    xhr.setRequestHeader(field, this.header[field]);
  }

  if (this._responseType) {
    xhr.responseType = this._responseType;
  }

  // send stuff
  this.emit('request', this);

  // IE11 xhr.send(undefined) sends 'undefined' string as POST payload (instead of nothing)
  // We need null here if data is undefined
  xhr.send(typeof data !== 'undefined' ? data : null);
  return this;
};


/**
 * Expose `Request`.
 */

request.Request = Request;

/**
 * GET `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} data or fn
 * @param {Function} fn
 * @return {Request}
 * @api public
 */

request.get = function(url, data, fn){
  var req = request('GET', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.query(data);
  if (fn) req.end(fn);
  return req;
};

/**
 * HEAD `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} data or fn
 * @param {Function} fn
 * @return {Request}
 * @api public
 */

request.head = function(url, data, fn){
  var req = request('HEAD', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

/**
 * DELETE `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Function} fn
 * @return {Request}
 * @api public
 */

function del(url, fn){
  var req = request('DELETE', url);
  if (fn) req.end(fn);
  return req;
};

request['del'] = del;
request['delete'] = del;

/**
 * PATCH `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} data
 * @param {Function} fn
 * @return {Request}
 * @api public
 */

request.patch = function(url, data, fn){
  var req = request('PATCH', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

/**
 * POST `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} data
 * @param {Function} fn
 * @return {Request}
 * @api public
 */

request.post = function(url, data, fn){
  var req = request('POST', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

/**
 * PUT `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} data or fn
 * @param {Function} fn
 * @return {Request}
 * @api public
 */

request.put = function(url, data, fn){
  var req = request('PUT', url);
  if ('function' == typeof data) fn = data, data = null;
  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

},{"./is-object":52,"./request":54,"./request-base":53,"emitter":55,"reduce":56}],52:[function(require,module,exports){
/**
 * Check if `obj` is an object.
 *
 * @param {Object} obj
 * @return {Boolean}
 * @api private
 */

function isObject(obj) {
  return null != obj && 'object' == typeof obj;
}

module.exports = isObject;

},{}],53:[function(require,module,exports){
/**
 * Module of mixed-in functions shared between node and client code
 */
var isObject = require('./is-object');

/**
 * Clear previous timeout.
 *
 * @return {Request} for chaining
 * @api public
 */

exports.clearTimeout = function _clearTimeout(){
  this._timeout = 0;
  clearTimeout(this._timer);
  return this;
};

/**
 * Force given parser
 *
 * Sets the body parser no matter type.
 *
 * @param {Function}
 * @api public
 */

exports.parse = function parse(fn){
  this._parser = fn;
  return this;
};

/**
 * Set timeout to `ms`.
 *
 * @param {Number} ms
 * @return {Request} for chaining
 * @api public
 */

exports.timeout = function timeout(ms){
  this._timeout = ms;
  return this;
};

/**
 * Faux promise support
 *
 * @param {Function} fulfill
 * @param {Function} reject
 * @return {Request}
 */

exports.then = function then(fulfill, reject) {
  return this.end(function(err, res) {
    err ? reject(err) : fulfill(res);
  });
}

/**
 * Allow for extension
 */

exports.use = function use(fn) {
  fn(this);
  return this;
}


/**
 * Get request header `field`.
 * Case-insensitive.
 *
 * @param {String} field
 * @return {String}
 * @api public
 */

exports.get = function(field){
  return this._header[field.toLowerCase()];
};

/**
 * Get case-insensitive header `field` value.
 * This is a deprecated internal API. Use `.get(field)` instead.
 *
 * (getHeader is no longer used internally by the superagent code base)
 *
 * @param {String} field
 * @return {String}
 * @api private
 * @deprecated
 */

exports.getHeader = exports.get;

/**
 * Set header `field` to `val`, or multiple fields with one object.
 * Case-insensitive.
 *
 * Examples:
 *
 *      req.get('/')
 *        .set('Accept', 'application/json')
 *        .set('X-API-Key', 'foobar')
 *        .end(callback);
 *
 *      req.get('/')
 *        .set({ Accept: 'application/json', 'X-API-Key': 'foobar' })
 *        .end(callback);
 *
 * @param {String|Object} field
 * @param {String} val
 * @return {Request} for chaining
 * @api public
 */

exports.set = function(field, val){
  if (isObject(field)) {
    for (var key in field) {
      this.set(key, field[key]);
    }
    return this;
  }
  this._header[field.toLowerCase()] = val;
  this.header[field] = val;
  return this;
};

/**
 * Remove header `field`.
 * Case-insensitive.
 *
 * Example:
 *
 *      req.get('/')
 *        .unset('User-Agent')
 *        .end(callback);
 *
 * @param {String} field
 */
exports.unset = function(field){
  delete this._header[field.toLowerCase()];
  delete this.header[field];
  return this;
};

/**
 * Write the field `name` and `val` for "multipart/form-data"
 * request bodies.
 *
 * ``` js
 * request.post('/upload')
 *   .field('foo', 'bar')
 *   .end(callback);
 * ```
 *
 * @param {String} name
 * @param {String|Blob|File|Buffer|fs.ReadStream} val
 * @return {Request} for chaining
 * @api public
 */
exports.field = function(name, val) {
  this._getFormData().append(name, val);
  return this;
};

},{"./is-object":52}],54:[function(require,module,exports){
// The node and browser modules expose versions of this with the
// appropriate constructor function bound as first argument
/**
 * Issue a request:
 *
 * Examples:
 *
 *    request('GET', '/users').end(callback)
 *    request('/users').end(callback)
 *    request('/users', callback)
 *
 * @param {String} method
 * @param {String|Function} url or callback
 * @return {Request}
 * @api public
 */

function request(RequestConstructor, method, url) {
  // callback
  if ('function' == typeof url) {
    return new RequestConstructor('GET', method).end(url);
  }

  // url first
  if (2 == arguments.length) {
    return new RequestConstructor('GET', method);
  }

  return new RequestConstructor(method, url);
}

module.exports = request;

},{}],55:[function(require,module,exports){

/**
 * Expose `Emitter`.
 */

module.exports = Emitter;

/**
 * Initialize a new `Emitter`.
 *
 * @api public
 */

function Emitter(obj) {
  if (obj) return mixin(obj);
};

/**
 * Mixin the emitter properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */

function mixin(obj) {
  for (var key in Emitter.prototype) {
    obj[key] = Emitter.prototype[key];
  }
  return obj;
}

/**
 * Listen on the given `event` with `fn`.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.on =
Emitter.prototype.addEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};
  (this._callbacks['$' + event] = this._callbacks['$' + event] || [])
    .push(fn);
  return this;
};

/**
 * Adds an `event` listener that will be invoked a single
 * time then automatically removed.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.once = function(event, fn){
  function on() {
    this.off(event, on);
    fn.apply(this, arguments);
  }

  on.fn = fn;
  this.on(event, on);
  return this;
};

/**
 * Remove the given callback for `event` or all
 * registered callbacks.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.off =
Emitter.prototype.removeListener =
Emitter.prototype.removeAllListeners =
Emitter.prototype.removeEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};

  // all
  if (0 == arguments.length) {
    this._callbacks = {};
    return this;
  }

  // specific event
  var callbacks = this._callbacks['$' + event];
  if (!callbacks) return this;

  // remove all handlers
  if (1 == arguments.length) {
    delete this._callbacks['$' + event];
    return this;
  }

  // remove specific handler
  var cb;
  for (var i = 0; i < callbacks.length; i++) {
    cb = callbacks[i];
    if (cb === fn || cb.fn === fn) {
      callbacks.splice(i, 1);
      break;
    }
  }
  return this;
};

/**
 * Emit `event` with the given args.
 *
 * @param {String} event
 * @param {Mixed} ...
 * @return {Emitter}
 */

Emitter.prototype.emit = function(event){
  this._callbacks = this._callbacks || {};
  var args = [].slice.call(arguments, 1)
    , callbacks = this._callbacks['$' + event];

  if (callbacks) {
    callbacks = callbacks.slice(0);
    for (var i = 0, len = callbacks.length; i < len; ++i) {
      callbacks[i].apply(this, args);
    }
  }

  return this;
};

/**
 * Return array of callbacks for `event`.
 *
 * @param {String} event
 * @return {Array}
 * @api public
 */

Emitter.prototype.listeners = function(event){
  this._callbacks = this._callbacks || {};
  return this._callbacks['$' + event] || [];
};

/**
 * Check if this emitter has `event` handlers.
 *
 * @param {String} event
 * @return {Boolean}
 * @api public
 */

Emitter.prototype.hasListeners = function(event){
  return !! this.listeners(event).length;
};

},{}],56:[function(require,module,exports){

/**
 * Reduce `arr` with `fn`.
 *
 * @param {Array} arr
 * @param {Function} fn
 * @param {Mixed} initial
 *
 * TODO: combatible error handling?
 */

module.exports = function(arr, fn, initial){  
  var idx = 0;
  var len = arr.length;
  var curr = arguments.length == 3
    ? initial
    : arr[idx++];

  while (idx < len) {
    curr = fn.call(null, curr, arr[idx], ++idx, arr);
  }
  
  return curr;
};
},{}],57:[function(require,module,exports){

},{}],58:[function(require,module,exports){
(function (global){
/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
 * @license  MIT
 */
/* eslint-disable no-proto */

'use strict'

var base64 = require('base64-js')
var ieee754 = require('ieee754')
var isArray = require('isarray')

exports.Buffer = Buffer
exports.SlowBuffer = SlowBuffer
exports.INSPECT_MAX_BYTES = 50
Buffer.poolSize = 8192 // not used by this implementation

var rootParent = {}

/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Use Object implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * Due to various browser bugs, sometimes the Object implementation will be used even
 * when the browser supports typed arrays.
 *
 * Note:
 *
 *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
 *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
 *
 *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
 *
 *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
 *     incorrect length in some situations.

 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
 * get the Object implementation, which is slower but behaves correctly.
 */
Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined
  ? global.TYPED_ARRAY_SUPPORT
  : typedArraySupport()

function typedArraySupport () {
  try {
    var arr = new Uint8Array(1)
    arr.foo = function () { return 42 }
    return arr.foo() === 42 && // typed array instances can be augmented
        typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
        arr.subarray(1, 1).byteLength === 0 // ie10 has broken `subarray`
  } catch (e) {
    return false
  }
}

function kMaxLength () {
  return Buffer.TYPED_ARRAY_SUPPORT
    ? 0x7fffffff
    : 0x3fffffff
}

/**
 * The Buffer constructor returns instances of `Uint8Array` that have their
 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
 * returns a single octet.
 *
 * The `Uint8Array` prototype remains unmodified.
 */
function Buffer (arg) {
  if (!(this instanceof Buffer)) {
    // Avoid going through an ArgumentsAdaptorTrampoline in the common case.
    if (arguments.length > 1) return new Buffer(arg, arguments[1])
    return new Buffer(arg)
  }

  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    this.length = 0
    this.parent = undefined
  }

  // Common case.
  if (typeof arg === 'number') {
    return fromNumber(this, arg)
  }

  // Slightly less common case.
  if (typeof arg === 'string') {
    return fromString(this, arg, arguments.length > 1 ? arguments[1] : 'utf8')
  }

  // Unusual.
  return fromObject(this, arg)
}

// TODO: Legacy, not needed anymore. Remove in next major version.
Buffer._augment = function (arr) {
  arr.__proto__ = Buffer.prototype
  return arr
}

function fromNumber (that, length) {
  that = allocate(that, length < 0 ? 0 : checked(length) | 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    for (var i = 0; i < length; i++) {
      that[i] = 0
    }
  }
  return that
}

function fromString (that, string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') encoding = 'utf8'

  // Assumption: byteLength() return value is always < kMaxLength.
  var length = byteLength(string, encoding) | 0
  that = allocate(that, length)

  that.write(string, encoding)
  return that
}

function fromObject (that, object) {
  if (Buffer.isBuffer(object)) return fromBuffer(that, object)

  if (isArray(object)) return fromArray(that, object)

  if (object == null) {
    throw new TypeError('must start with number, buffer, array or string')
  }

  if (typeof ArrayBuffer !== 'undefined') {
    if (object.buffer instanceof ArrayBuffer) {
      return fromTypedArray(that, object)
    }
    if (object instanceof ArrayBuffer) {
      return fromArrayBuffer(that, object)
    }
  }

  if (object.length) return fromArrayLike(that, object)

  return fromJsonObject(that, object)
}

function fromBuffer (that, buffer) {
  var length = checked(buffer.length) | 0
  that = allocate(that, length)
  buffer.copy(that, 0, 0, length)
  return that
}

function fromArray (that, array) {
  var length = checked(array.length) | 0
  that = allocate(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

// Duplicate of fromArray() to keep fromArray() monomorphic.
function fromTypedArray (that, array) {
  var length = checked(array.length) | 0
  that = allocate(that, length)
  // Truncating the elements is probably not what people expect from typed
  // arrays with BYTES_PER_ELEMENT > 1 but it's compatible with the behavior
  // of the old Buffer constructor.
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

function fromArrayBuffer (that, array) {
  array.byteLength // this throws if `array` is not a valid ArrayBuffer

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = new Uint8Array(array)
    that.__proto__ = Buffer.prototype
  } else {
    // Fallback: Return an object instance of the Buffer class
    that = fromTypedArray(that, new Uint8Array(array))
  }
  return that
}

function fromArrayLike (that, array) {
  var length = checked(array.length) | 0
  that = allocate(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

// Deserialize { type: 'Buffer', data: [1,2,3,...] } into a Buffer object.
// Returns a zero-length buffer for inputs that don't conform to the spec.
function fromJsonObject (that, object) {
  var array
  var length = 0

  if (object.type === 'Buffer' && isArray(object.data)) {
    array = object.data
    length = checked(array.length) | 0
  }
  that = allocate(that, length)

  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

if (Buffer.TYPED_ARRAY_SUPPORT) {
  Buffer.prototype.__proto__ = Uint8Array.prototype
  Buffer.__proto__ = Uint8Array
  if (typeof Symbol !== 'undefined' && Symbol.species &&
      Buffer[Symbol.species] === Buffer) {
    // Fix subarray() in ES2016. See: https://github.com/feross/buffer/pull/97
    Object.defineProperty(Buffer, Symbol.species, {
      value: null,
      configurable: true
    })
  }
} else {
  // pre-set for values that may exist in the future
  Buffer.prototype.length = undefined
  Buffer.prototype.parent = undefined
}

function allocate (that, length) {
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = new Uint8Array(length)
    that.__proto__ = Buffer.prototype
  } else {
    // Fallback: Return an object instance of the Buffer class
    that.length = length
  }

  var fromPool = length !== 0 && length <= Buffer.poolSize >>> 1
  if (fromPool) that.parent = rootParent

  return that
}

function checked (length) {
  // Note: cannot use `length < kMaxLength` here because that fails when
  // length is NaN (which is otherwise coerced to zero.)
  if (length >= kMaxLength()) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                         'size: 0x' + kMaxLength().toString(16) + ' bytes')
  }
  return length | 0
}

function SlowBuffer (subject, encoding) {
  if (!(this instanceof SlowBuffer)) return new SlowBuffer(subject, encoding)

  var buf = new Buffer(subject, encoding)
  delete buf.parent
  return buf
}

Buffer.isBuffer = function isBuffer (b) {
  return !!(b != null && b._isBuffer)
}

Buffer.compare = function compare (a, b) {
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError('Arguments must be Buffers')
  }

  if (a === b) return 0

  var x = a.length
  var y = b.length

  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
    if (a[i] !== b[i]) {
      x = a[i]
      y = b[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

Buffer.isEncoding = function isEncoding (encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'binary':
    case 'base64':
    case 'raw':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true
    default:
      return false
  }
}

Buffer.concat = function concat (list, length) {
  if (!isArray(list)) throw new TypeError('list argument must be an Array of Buffers.')

  if (list.length === 0) {
    return new Buffer(0)
  }

  var i
  if (length === undefined) {
    length = 0
    for (i = 0; i < list.length; i++) {
      length += list[i].length
    }
  }

  var buf = new Buffer(length)
  var pos = 0
  for (i = 0; i < list.length; i++) {
    var item = list[i]
    item.copy(buf, pos)
    pos += item.length
  }
  return buf
}

function byteLength (string, encoding) {
  if (typeof string !== 'string') string = '' + string

  var len = string.length
  if (len === 0) return 0

  // Use a for loop to avoid recursion
  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'binary':
      // Deprecated
      case 'raw':
      case 'raws':
        return len
      case 'utf8':
      case 'utf-8':
        return utf8ToBytes(string).length
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2
      case 'hex':
        return len >>> 1
      case 'base64':
        return base64ToBytes(string).length
      default:
        if (loweredCase) return utf8ToBytes(string).length // assume utf8
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}
Buffer.byteLength = byteLength

function slowToString (encoding, start, end) {
  var loweredCase = false

  start = start | 0
  end = end === undefined || end === Infinity ? this.length : end | 0

  if (!encoding) encoding = 'utf8'
  if (start < 0) start = 0
  if (end > this.length) end = this.length
  if (end <= start) return ''

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end)

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end)

      case 'ascii':
        return asciiSlice(this, start, end)

      case 'binary':
        return binarySlice(this, start, end)

      case 'base64':
        return base64Slice(this, start, end)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = (encoding + '').toLowerCase()
        loweredCase = true
    }
  }
}

// The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
// Buffer instances.
Buffer.prototype._isBuffer = true

Buffer.prototype.toString = function toString () {
  var length = this.length | 0
  if (length === 0) return ''
  if (arguments.length === 0) return utf8Slice(this, 0, length)
  return slowToString.apply(this, arguments)
}

Buffer.prototype.equals = function equals (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return true
  return Buffer.compare(this, b) === 0
}

Buffer.prototype.inspect = function inspect () {
  var str = ''
  var max = exports.INSPECT_MAX_BYTES
  if (this.length > 0) {
    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ')
    if (this.length > max) str += ' ... '
  }
  return '<Buffer ' + str + '>'
}

Buffer.prototype.compare = function compare (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  return Buffer.compare(this, b)
}

Buffer.prototype.indexOf = function indexOf (val, byteOffset) {
  if (byteOffset > 0x7fffffff) byteOffset = 0x7fffffff
  else if (byteOffset < -0x80000000) byteOffset = -0x80000000
  byteOffset >>= 0

  if (this.length === 0) return -1
  if (byteOffset >= this.length) return -1

  // Negative offsets start from the end of the buffer
  if (byteOffset < 0) byteOffset = Math.max(this.length + byteOffset, 0)

  if (typeof val === 'string') {
    if (val.length === 0) return -1 // special case: looking for empty string always fails
    return String.prototype.indexOf.call(this, val, byteOffset)
  }
  if (Buffer.isBuffer(val)) {
    return arrayIndexOf(this, val, byteOffset)
  }
  if (typeof val === 'number') {
    if (Buffer.TYPED_ARRAY_SUPPORT && Uint8Array.prototype.indexOf === 'function') {
      return Uint8Array.prototype.indexOf.call(this, val, byteOffset)
    }
    return arrayIndexOf(this, [ val ], byteOffset)
  }

  function arrayIndexOf (arr, val, byteOffset) {
    var foundIndex = -1
    for (var i = 0; byteOffset + i < arr.length; i++) {
      if (arr[byteOffset + i] === val[foundIndex === -1 ? 0 : i - foundIndex]) {
        if (foundIndex === -1) foundIndex = i
        if (i - foundIndex + 1 === val.length) return byteOffset + foundIndex
      } else {
        foundIndex = -1
      }
    }
    return -1
  }

  throw new TypeError('val must be string, number or Buffer')
}

function hexWrite (buf, string, offset, length) {
  offset = Number(offset) || 0
  var remaining = buf.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }

  // must be an even number of digits
  var strLen = string.length
  if (strLen % 2 !== 0) throw new Error('Invalid hex string')

  if (length > strLen / 2) {
    length = strLen / 2
  }
  for (var i = 0; i < length; i++) {
    var parsed = parseInt(string.substr(i * 2, 2), 16)
    if (isNaN(parsed)) throw new Error('Invalid hex string')
    buf[offset + i] = parsed
  }
  return i
}

function utf8Write (buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
}

function asciiWrite (buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length)
}

function binaryWrite (buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length)
}

function base64Write (buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length)
}

function ucs2Write (buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
}

Buffer.prototype.write = function write (string, offset, length, encoding) {
  // Buffer#write(string)
  if (offset === undefined) {
    encoding = 'utf8'
    length = this.length
    offset = 0
  // Buffer#write(string, encoding)
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset
    length = this.length
    offset = 0
  // Buffer#write(string, offset[, length][, encoding])
  } else if (isFinite(offset)) {
    offset = offset | 0
    if (isFinite(length)) {
      length = length | 0
      if (encoding === undefined) encoding = 'utf8'
    } else {
      encoding = length
      length = undefined
    }
  // legacy write(string, encoding, offset, length) - remove in v0.13
  } else {
    var swap = encoding
    encoding = offset
    offset = length | 0
    length = swap
  }

  var remaining = this.length - offset
  if (length === undefined || length > remaining) length = remaining

  if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
    throw new RangeError('attempt to write outside buffer bounds')
  }

  if (!encoding) encoding = 'utf8'

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length)

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length)

      case 'ascii':
        return asciiWrite(this, string, offset, length)

      case 'binary':
        return binaryWrite(this, string, offset, length)

      case 'base64':
        // Warning: maxLength not taken into account in base64Write
        return base64Write(this, string, offset, length)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.toJSON = function toJSON () {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  }
}

function base64Slice (buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf)
  } else {
    return base64.fromByteArray(buf.slice(start, end))
  }
}

function utf8Slice (buf, start, end) {
  end = Math.min(buf.length, end)
  var res = []

  var i = start
  while (i < end) {
    var firstByte = buf[i]
    var codePoint = null
    var bytesPerSequence = (firstByte > 0xEF) ? 4
      : (firstByte > 0xDF) ? 3
      : (firstByte > 0xBF) ? 2
      : 1

    if (i + bytesPerSequence <= end) {
      var secondByte, thirdByte, fourthByte, tempCodePoint

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte
          }
          break
        case 2:
          secondByte = buf[i + 1]
          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F)
            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint
            }
          }
          break
        case 3:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F)
            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint
            }
          }
          break
        case 4:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          fourthByte = buf[i + 3]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F)
            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint
            }
          }
      }
    }

    if (codePoint === null) {
      // we did not generate a valid codePoint so insert a
      // replacement char (U+FFFD) and advance only 1 byte
      codePoint = 0xFFFD
      bytesPerSequence = 1
    } else if (codePoint > 0xFFFF) {
      // encode to utf16 (surrogate pair dance)
      codePoint -= 0x10000
      res.push(codePoint >>> 10 & 0x3FF | 0xD800)
      codePoint = 0xDC00 | codePoint & 0x3FF
    }

    res.push(codePoint)
    i += bytesPerSequence
  }

  return decodeCodePointsArray(res)
}

// Based on http://stackoverflow.com/a/22747272/680742, the browser with
// the lowest limit is Chrome, with 0x10000 args.
// We go 1 magnitude less, for safety
var MAX_ARGUMENTS_LENGTH = 0x1000

function decodeCodePointsArray (codePoints) {
  var len = codePoints.length
  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints) // avoid extra slice()
  }

  // Decode in chunks to avoid "call stack size exceeded".
  var res = ''
  var i = 0
  while (i < len) {
    res += String.fromCharCode.apply(
      String,
      codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
    )
  }
  return res
}

function asciiSlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; i++) {
    ret += String.fromCharCode(buf[i] & 0x7F)
  }
  return ret
}

function binarySlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; i++) {
    ret += String.fromCharCode(buf[i])
  }
  return ret
}

function hexSlice (buf, start, end) {
  var len = buf.length

  if (!start || start < 0) start = 0
  if (!end || end < 0 || end > len) end = len

  var out = ''
  for (var i = start; i < end; i++) {
    out += toHex(buf[i])
  }
  return out
}

function utf16leSlice (buf, start, end) {
  var bytes = buf.slice(start, end)
  var res = ''
  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256)
  }
  return res
}

Buffer.prototype.slice = function slice (start, end) {
  var len = this.length
  start = ~~start
  end = end === undefined ? len : ~~end

  if (start < 0) {
    start += len
    if (start < 0) start = 0
  } else if (start > len) {
    start = len
  }

  if (end < 0) {
    end += len
    if (end < 0) end = 0
  } else if (end > len) {
    end = len
  }

  if (end < start) end = start

  var newBuf
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    newBuf = this.subarray(start, end)
    newBuf.__proto__ = Buffer.prototype
  } else {
    var sliceLen = end - start
    newBuf = new Buffer(sliceLen, undefined)
    for (var i = 0; i < sliceLen; i++) {
      newBuf[i] = this[i + start]
    }
  }

  if (newBuf.length) newBuf.parent = this.parent || this

  return newBuf
}

/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */
function checkOffset (offset, ext, length) {
  if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
}

Buffer.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }

  return val
}

Buffer.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    checkOffset(offset, byteLength, this.length)
  }

  var val = this[offset + --byteLength]
  var mul = 1
  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul
  }

  return val
}

Buffer.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  return this[offset]
}

Buffer.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return this[offset] | (this[offset + 1] << 8)
}

Buffer.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return (this[offset] << 8) | this[offset + 1]
}

Buffer.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return ((this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16)) +
      (this[offset + 3] * 0x1000000)
}

Buffer.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] * 0x1000000) +
    ((this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    this[offset + 3])
}

Buffer.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var i = byteLength
  var mul = 1
  var val = this[offset + --i]
  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readInt8 = function readInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  if (!(this[offset] & 0x80)) return (this[offset])
  return ((0xff - this[offset] + 1) * -1)
}

Buffer.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset] | (this[offset + 1] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset + 1] | (this[offset] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset]) |
    (this[offset + 1] << 8) |
    (this[offset + 2] << 16) |
    (this[offset + 3] << 24)
}

Buffer.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] << 24) |
    (this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    (this[offset + 3])
}

Buffer.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, true, 23, 4)
}

Buffer.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, false, 23, 4)
}

Buffer.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, true, 52, 8)
}

Buffer.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, false, 52, 8)
}

function checkInt (buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('buffer must be a Buffer instance')
  if (value > max || value < min) throw new RangeError('value is out of bounds')
  if (offset + ext > buf.length) throw new RangeError('index out of range')
}

Buffer.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkInt(this, value, offset, byteLength, Math.pow(2, 8 * byteLength), 0)

  var mul = 1
  var i = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkInt(this, value, offset, byteLength, Math.pow(2, 8 * byteLength), 0)

  var i = byteLength - 1
  var mul = 1
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  this[offset] = (value & 0xff)
  return offset + 1
}

function objectWriteUInt16 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; i++) {
    buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
      (littleEndian ? i : 1 - i) * 8
  }
}

Buffer.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

function objectWriteUInt32 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffffffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; i++) {
    buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff
  }
}

Buffer.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset + 3] = (value >>> 24)
    this[offset + 2] = (value >>> 16)
    this[offset + 1] = (value >>> 8)
    this[offset] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

Buffer.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = 0
  var mul = 1
  var sub = value < 0 ? 1 : 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = byteLength - 1
  var mul = 1
  var sub = value < 0 ? 1 : 0
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  if (value < 0) value = 0xff + value + 1
  this[offset] = (value & 0xff)
  return offset + 1
}

Buffer.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

Buffer.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
    this[offset + 2] = (value >>> 16)
    this[offset + 3] = (value >>> 24)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (value < 0) value = 0xffffffff + value + 1
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

function checkIEEE754 (buf, value, offset, ext, max, min) {
  if (offset + ext > buf.length) throw new RangeError('index out of range')
  if (offset < 0) throw new RangeError('index out of range')
}

function writeFloat (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
  }
  ieee754.write(buf, value, offset, littleEndian, 23, 4)
  return offset + 4
}

Buffer.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert)
}

Buffer.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert)
}

function writeDouble (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
  }
  ieee754.write(buf, value, offset, littleEndian, 52, 8)
  return offset + 8
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert)
}

Buffer.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert)
}

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy = function copy (target, targetStart, start, end) {
  if (!start) start = 0
  if (!end && end !== 0) end = this.length
  if (targetStart >= target.length) targetStart = target.length
  if (!targetStart) targetStart = 0
  if (end > 0 && end < start) end = start

  // Copy 0 bytes; we're done
  if (end === start) return 0
  if (target.length === 0 || this.length === 0) return 0

  // Fatal error conditions
  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds')
  }
  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds')
  if (end < 0) throw new RangeError('sourceEnd out of bounds')

  // Are we oob?
  if (end > this.length) end = this.length
  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start
  }

  var len = end - start
  var i

  if (this === target && start < targetStart && targetStart < end) {
    // descending copy from end
    for (i = len - 1; i >= 0; i--) {
      target[i + targetStart] = this[i + start]
    }
  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
    // ascending copy from start
    for (i = 0; i < len; i++) {
      target[i + targetStart] = this[i + start]
    }
  } else {
    Uint8Array.prototype.set.call(
      target,
      this.subarray(start, start + len),
      targetStart
    )
  }

  return len
}

// fill(value, start=0, end=buffer.length)
Buffer.prototype.fill = function fill (value, start, end) {
  if (!value) value = 0
  if (!start) start = 0
  if (!end) end = this.length

  if (end < start) throw new RangeError('end < start')

  // Fill 0 bytes; we're done
  if (end === start) return
  if (this.length === 0) return

  if (start < 0 || start >= this.length) throw new RangeError('start out of bounds')
  if (end < 0 || end > this.length) throw new RangeError('end out of bounds')

  var i
  if (typeof value === 'number') {
    for (i = start; i < end; i++) {
      this[i] = value
    }
  } else {
    var bytes = utf8ToBytes(value.toString())
    var len = bytes.length
    for (i = start; i < end; i++) {
      this[i] = bytes[i % len]
    }
  }

  return this
}

// HELPER FUNCTIONS
// ================

var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g

function base64clean (str) {
  // Node strips out invalid characters like \n and \t from the string, base64-js does not
  str = stringtrim(str).replace(INVALID_BASE64_RE, '')
  // Node converts strings with length < 2 to ''
  if (str.length < 2) return ''
  // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
  while (str.length % 4 !== 0) {
    str = str + '='
  }
  return str
}

function stringtrim (str) {
  if (str.trim) return str.trim()
  return str.replace(/^\s+|\s+$/g, '')
}

function toHex (n) {
  if (n < 16) return '0' + n.toString(16)
  return n.toString(16)
}

function utf8ToBytes (string, units) {
  units = units || Infinity
  var codePoint
  var length = string.length
  var leadSurrogate = null
  var bytes = []

  for (var i = 0; i < length; i++) {
    codePoint = string.charCodeAt(i)

    // is surrogate component
    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      // last char was a lead
      if (!leadSurrogate) {
        // no lead yet
        if (codePoint > 0xDBFF) {
          // unexpected trail
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        } else if (i + 1 === length) {
          // unpaired lead
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        }

        // valid lead
        leadSurrogate = codePoint

        continue
      }

      // 2 leads in a row
      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
        leadSurrogate = codePoint
        continue
      }

      // valid surrogate pair
      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000
    } else if (leadSurrogate) {
      // valid bmp char, but last char was a lead
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
    }

    leadSurrogate = null

    // encode utf8
    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break
      bytes.push(codePoint)
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break
      bytes.push(
        codePoint >> 0x6 | 0xC0,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break
      bytes.push(
        codePoint >> 0xC | 0xE0,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break
      bytes.push(
        codePoint >> 0x12 | 0xF0,
        codePoint >> 0xC & 0x3F | 0x80,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else {
      throw new Error('Invalid code point')
    }
  }

  return bytes
}

function asciiToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; i++) {
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push(str.charCodeAt(i) & 0xFF)
  }
  return byteArray
}

function utf16leToBytes (str, units) {
  var c, hi, lo
  var byteArray = []
  for (var i = 0; i < str.length; i++) {
    if ((units -= 2) < 0) break

    c = str.charCodeAt(i)
    hi = c >> 8
    lo = c % 256
    byteArray.push(lo)
    byteArray.push(hi)
  }

  return byteArray
}

function base64ToBytes (str) {
  return base64.toByteArray(base64clean(str))
}

function blitBuffer (src, dst, offset, length) {
  for (var i = 0; i < length; i++) {
    if ((i + offset >= dst.length) || (i >= src.length)) break
    dst[i + offset] = src[i]
  }
  return i
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"base64-js":59,"ieee754":60,"isarray":61}],59:[function(require,module,exports){
'use strict'

exports.toByteArray = toByteArray
exports.fromByteArray = fromByteArray

var lookup = []
var revLookup = []
var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array

function init () {
  var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
  for (var i = 0, len = code.length; i < len; ++i) {
    lookup[i] = code[i]
    revLookup[code.charCodeAt(i)] = i
  }

  revLookup['-'.charCodeAt(0)] = 62
  revLookup['_'.charCodeAt(0)] = 63
}

init()

function toByteArray (b64) {
  var i, j, l, tmp, placeHolders, arr
  var len = b64.length

  if (len % 4 > 0) {
    throw new Error('Invalid string. Length must be a multiple of 4')
  }

  // the number of equal signs (place holders)
  // if there are two placeholders, than the two characters before it
  // represent one byte
  // if there is only one, then the three characters before it represent 2 bytes
  // this is just a cheap hack to not do indexOf twice
  placeHolders = b64[len - 2] === '=' ? 2 : b64[len - 1] === '=' ? 1 : 0

  // base64 is 4/3 + up to two characters of the original data
  arr = new Arr(len * 3 / 4 - placeHolders)

  // if there are placeholders, only get up to the last complete 4 chars
  l = placeHolders > 0 ? len - 4 : len

  var L = 0

  for (i = 0, j = 0; i < l; i += 4, j += 3) {
    tmp = (revLookup[b64.charCodeAt(i)] << 18) | (revLookup[b64.charCodeAt(i + 1)] << 12) | (revLookup[b64.charCodeAt(i + 2)] << 6) | revLookup[b64.charCodeAt(i + 3)]
    arr[L++] = (tmp >> 16) & 0xFF
    arr[L++] = (tmp >> 8) & 0xFF
    arr[L++] = tmp & 0xFF
  }

  if (placeHolders === 2) {
    tmp = (revLookup[b64.charCodeAt(i)] << 2) | (revLookup[b64.charCodeAt(i + 1)] >> 4)
    arr[L++] = tmp & 0xFF
  } else if (placeHolders === 1) {
    tmp = (revLookup[b64.charCodeAt(i)] << 10) | (revLookup[b64.charCodeAt(i + 1)] << 4) | (revLookup[b64.charCodeAt(i + 2)] >> 2)
    arr[L++] = (tmp >> 8) & 0xFF
    arr[L++] = tmp & 0xFF
  }

  return arr
}

function tripletToBase64 (num) {
  return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F]
}

function encodeChunk (uint8, start, end) {
  var tmp
  var output = []
  for (var i = start; i < end; i += 3) {
    tmp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2])
    output.push(tripletToBase64(tmp))
  }
  return output.join('')
}

function fromByteArray (uint8) {
  var tmp
  var len = uint8.length
  var extraBytes = len % 3 // if we have 1 byte left, pad 2 bytes
  var output = ''
  var parts = []
  var maxChunkLength = 16383 // must be multiple of 3

  // go through the array every three bytes, we'll deal with trailing stuff later
  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
    parts.push(encodeChunk(uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)))
  }

  // pad the end with zeros, but make sure to not forget the extra bytes
  if (extraBytes === 1) {
    tmp = uint8[len - 1]
    output += lookup[tmp >> 2]
    output += lookup[(tmp << 4) & 0x3F]
    output += '=='
  } else if (extraBytes === 2) {
    tmp = (uint8[len - 2] << 8) + (uint8[len - 1])
    output += lookup[tmp >> 10]
    output += lookup[(tmp >> 4) & 0x3F]
    output += lookup[(tmp << 2) & 0x3F]
    output += '='
  }

  parts.push(output)

  return parts.join('')
}

},{}],60:[function(require,module,exports){
exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var nBits = -7
  var i = isLE ? (nBytes - 1) : 0
  var d = isLE ? -1 : 1
  var s = buffer[offset + i]

  i += d

  e = s & ((1 << (-nBits)) - 1)
  s >>= (-nBits)
  nBits += eLen
  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & ((1 << (-nBits)) - 1)
  e >>= (-nBits)
  nBits += mLen
  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity)
  } else {
    m = m + Math.pow(2, mLen)
    e = e - eBias
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
}

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0)
  var i = isLE ? 0 : (nBytes - 1)
  var d = isLE ? 1 : -1
  var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0

  value = Math.abs(value)

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0
    e = eMax
  } else {
    e = Math.floor(Math.log(value) / Math.LN2)
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--
      c *= 2
    }
    if (e + eBias >= 1) {
      value += rt / c
    } else {
      value += rt * Math.pow(2, 1 - eBias)
    }
    if (value * c >= 2) {
      e++
      c /= 2
    }

    if (e + eBias >= eMax) {
      m = 0
      e = eMax
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen)
      e = e + eBias
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen)
      e = 0
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = (e << mLen) | m
  eLen += mLen
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128
}

},{}],61:[function(require,module,exports){
var toString = {}.toString;

module.exports = Array.isArray || function (arr) {
  return toString.call(arr) == '[object Array]';
};

},{}],"travis":[function(require,module,exports){
(function(factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/CoordinatesResponseDTO', 'model/DiaryCommentRequestDTO', 'model/DiaryCommentResponseDTO', 'model/DiaryCoordinatesElementDTO', 'model/DiaryCoordinatesRequestDTO', 'model/DiaryElementsResponseListDTO', 'model/DiaryEntryDTO', 'model/DiaryPictureElementDTO', 'model/DiaryRequestDTO', 'model/DiaryResponseDTO', 'model/DiaryTextElementDTO', 'model/DiaryTextRequestDTO', 'model/ErrorResponse', 'model/LocationCoordinateDTO', 'model/LocationDTO', 'model/LocationResponseDTO', 'model/LocationSearchDTO', 'model/LoginRequestDTO', 'model/LoginResponseDTO', 'model/MainSearchDTO', 'model/ProfileLocationResponseDTO', 'model/ProfileTagDTO', 'model/ProfileTagResponseDTO', 'model/ProfileTextDTO', 'model/ProfileTextResponseDTO', 'model/PushNotificationDTO', 'model/RatingCategoryRequestDTO', 'model/RatingCategoryResponseDTO', 'model/TripActivityRequestDTO', 'model/TripActivityResponseDTO', 'model/TripOverviewResponseDTO', 'model/TripPlannerRequestDTO', 'model/TripPlannerResponseDTO', 'model/TripRequestDTO', 'model/TripResponseDTO', 'model/TripUserResponseDTO', 'model/UserDTO', 'model/UserMainPageEntryDTO', 'model/UserProfileDTO', 'model/UserSearchDTO', 'api/AuthenticationApi', 'api/DiaryApi', 'api/LocationsApi', 'api/PictureApi', 'api/PlannerApi', 'api/RatingApi', 'api/SearchApi', 'api/UserInfoApi', 'api/UserUpdateApi'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('./ApiClient'), require('./model/CoordinatesResponseDTO'), require('./model/DiaryCommentRequestDTO'), require('./model/DiaryCommentResponseDTO'), require('./model/DiaryCoordinatesElementDTO'), require('./model/DiaryCoordinatesRequestDTO'), require('./model/DiaryElementsResponseListDTO'), require('./model/DiaryEntryDTO'), require('./model/DiaryPictureElementDTO'), require('./model/DiaryRequestDTO'), require('./model/DiaryResponseDTO'), require('./model/DiaryTextElementDTO'), require('./model/DiaryTextRequestDTO'), require('./model/ErrorResponse'), require('./model/LocationCoordinateDTO'), require('./model/LocationDTO'), require('./model/LocationResponseDTO'), require('./model/LocationSearchDTO'), require('./model/LoginRequestDTO'), require('./model/LoginResponseDTO'), require('./model/MainSearchDTO'), require('./model/ProfileLocationResponseDTO'), require('./model/ProfileTagDTO'), require('./model/ProfileTagResponseDTO'), require('./model/ProfileTextDTO'), require('./model/ProfileTextResponseDTO'), require('./model/PushNotificationDTO'), require('./model/RatingCategoryRequestDTO'), require('./model/RatingCategoryResponseDTO'), require('./model/TripActivityRequestDTO'), require('./model/TripActivityResponseDTO'), require('./model/TripOverviewResponseDTO'), require('./model/TripPlannerRequestDTO'), require('./model/TripPlannerResponseDTO'), require('./model/TripRequestDTO'), require('./model/TripResponseDTO'), require('./model/TripUserResponseDTO'), require('./model/UserDTO'), require('./model/UserMainPageEntryDTO'), require('./model/UserProfileDTO'), require('./model/UserSearchDTO'), require('./api/AuthenticationApi'), require('./api/DiaryApi'), require('./api/LocationsApi'), require('./api/PictureApi'), require('./api/PlannerApi'), require('./api/RatingApi'), require('./api/SearchApi'), require('./api/UserInfoApi'), require('./api/UserUpdateApi'));
  }
}(function(ApiClient, CoordinatesResponseDTO, DiaryCommentRequestDTO, DiaryCommentResponseDTO, DiaryCoordinatesElementDTO, DiaryCoordinatesRequestDTO, DiaryElementsResponseListDTO, DiaryEntryDTO, DiaryPictureElementDTO, DiaryRequestDTO, DiaryResponseDTO, DiaryTextElementDTO, DiaryTextRequestDTO, ErrorResponse, LocationCoordinateDTO, LocationDTO, LocationResponseDTO, LocationSearchDTO, LoginRequestDTO, LoginResponseDTO, MainSearchDTO, ProfileLocationResponseDTO, ProfileTagDTO, ProfileTagResponseDTO, ProfileTextDTO, ProfileTextResponseDTO, PushNotificationDTO, RatingCategoryRequestDTO, RatingCategoryResponseDTO, TripActivityRequestDTO, TripActivityResponseDTO, TripOverviewResponseDTO, TripPlannerRequestDTO, TripPlannerResponseDTO, TripRequestDTO, TripResponseDTO, TripUserResponseDTO, UserDTO, UserMainPageEntryDTO, UserProfileDTO, UserSearchDTO, AuthenticationApi, DiaryApi, LocationsApi, PictureApi, PlannerApi, RatingApi, SearchApi, UserInfoApi, UserUpdateApi) {
  'use strict';

  /**
   * Client library of travis.<br>
   * The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
   * <p>
   * An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
   * <pre>
   * var Travis = require('index'); // See note below*.
   * var xxxSvc = new Travis.XxxApi(); // Allocate the API class we're going to use.
   * var yyyModel = new Travis.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * <em>*NOTE: For a top-level AMD script, use require(['index'], function(){...})
   * and put the application logic within the callback function.</em>
   * </p>
   * <p>
   * A non-AMD browser application (discouraged) might do something like this:
   * <pre>
   * var xxxSvc = new Travis.XxxApi(); // Allocate the API class we're going to use.
   * var yyy = new Travis.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * </p>
   * @module index
   * @version 0.0.1
   */
  var exports = {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient: ApiClient,
    /**
     * The CoordinatesResponseDTO model constructor.
     * @property {module:model/CoordinatesResponseDTO}
     */
    CoordinatesResponseDTO: CoordinatesResponseDTO,
    /**
     * The DiaryCommentRequestDTO model constructor.
     * @property {module:model/DiaryCommentRequestDTO}
     */
    DiaryCommentRequestDTO: DiaryCommentRequestDTO,
    /**
     * The DiaryCommentResponseDTO model constructor.
     * @property {module:model/DiaryCommentResponseDTO}
     */
    DiaryCommentResponseDTO: DiaryCommentResponseDTO,
    /**
     * The DiaryCoordinatesElementDTO model constructor.
     * @property {module:model/DiaryCoordinatesElementDTO}
     */
    DiaryCoordinatesElementDTO: DiaryCoordinatesElementDTO,
    /**
     * The DiaryCoordinatesRequestDTO model constructor.
     * @property {module:model/DiaryCoordinatesRequestDTO}
     */
    DiaryCoordinatesRequestDTO: DiaryCoordinatesRequestDTO,
    /**
     * The DiaryElementsResponseListDTO model constructor.
     * @property {module:model/DiaryElementsResponseListDTO}
     */
    DiaryElementsResponseListDTO: DiaryElementsResponseListDTO,
    /**
     * The DiaryEntryDTO model constructor.
     * @property {module:model/DiaryEntryDTO}
     */
    DiaryEntryDTO: DiaryEntryDTO,
    /**
     * The DiaryPictureElementDTO model constructor.
     * @property {module:model/DiaryPictureElementDTO}
     */
    DiaryPictureElementDTO: DiaryPictureElementDTO,
    /**
     * The DiaryRequestDTO model constructor.
     * @property {module:model/DiaryRequestDTO}
     */
    DiaryRequestDTO: DiaryRequestDTO,
    /**
     * The DiaryResponseDTO model constructor.
     * @property {module:model/DiaryResponseDTO}
     */
    DiaryResponseDTO: DiaryResponseDTO,
    /**
     * The DiaryTextElementDTO model constructor.
     * @property {module:model/DiaryTextElementDTO}
     */
    DiaryTextElementDTO: DiaryTextElementDTO,
    /**
     * The DiaryTextRequestDTO model constructor.
     * @property {module:model/DiaryTextRequestDTO}
     */
    DiaryTextRequestDTO: DiaryTextRequestDTO,
    /**
     * The ErrorResponse model constructor.
     * @property {module:model/ErrorResponse}
     */
    ErrorResponse: ErrorResponse,
    /**
     * The LocationCoordinateDTO model constructor.
     * @property {module:model/LocationCoordinateDTO}
     */
    LocationCoordinateDTO: LocationCoordinateDTO,
    /**
     * The LocationDTO model constructor.
     * @property {module:model/LocationDTO}
     */
    LocationDTO: LocationDTO,
    /**
     * The LocationResponseDTO model constructor.
     * @property {module:model/LocationResponseDTO}
     */
    LocationResponseDTO: LocationResponseDTO,
    /**
     * The LocationSearchDTO model constructor.
     * @property {module:model/LocationSearchDTO}
     */
    LocationSearchDTO: LocationSearchDTO,
    /**
     * The LoginRequestDTO model constructor.
     * @property {module:model/LoginRequestDTO}
     */
    LoginRequestDTO: LoginRequestDTO,
    /**
     * The LoginResponseDTO model constructor.
     * @property {module:model/LoginResponseDTO}
     */
    LoginResponseDTO: LoginResponseDTO,
    /**
     * The MainSearchDTO model constructor.
     * @property {module:model/MainSearchDTO}
     */
    MainSearchDTO: MainSearchDTO,
    /**
     * The ProfileLocationResponseDTO model constructor.
     * @property {module:model/ProfileLocationResponseDTO}
     */
    ProfileLocationResponseDTO: ProfileLocationResponseDTO,
    /**
     * The ProfileTagDTO model constructor.
     * @property {module:model/ProfileTagDTO}
     */
    ProfileTagDTO: ProfileTagDTO,
    /**
     * The ProfileTagResponseDTO model constructor.
     * @property {module:model/ProfileTagResponseDTO}
     */
    ProfileTagResponseDTO: ProfileTagResponseDTO,
    /**
     * The ProfileTextDTO model constructor.
     * @property {module:model/ProfileTextDTO}
     */
    ProfileTextDTO: ProfileTextDTO,
    /**
     * The ProfileTextResponseDTO model constructor.
     * @property {module:model/ProfileTextResponseDTO}
     */
    ProfileTextResponseDTO: ProfileTextResponseDTO,
    /**
     * The PushNotificationDTO model constructor.
     * @property {module:model/PushNotificationDTO}
     */
    PushNotificationDTO: PushNotificationDTO,
    /**
     * The RatingCategoryRequestDTO model constructor.
     * @property {module:model/RatingCategoryRequestDTO}
     */
    RatingCategoryRequestDTO: RatingCategoryRequestDTO,
    /**
     * The RatingCategoryResponseDTO model constructor.
     * @property {module:model/RatingCategoryResponseDTO}
     */
    RatingCategoryResponseDTO: RatingCategoryResponseDTO,
    /**
     * The TripActivityRequestDTO model constructor.
     * @property {module:model/TripActivityRequestDTO}
     */
    TripActivityRequestDTO: TripActivityRequestDTO,
    /**
     * The TripActivityResponseDTO model constructor.
     * @property {module:model/TripActivityResponseDTO}
     */
    TripActivityResponseDTO: TripActivityResponseDTO,
    /**
     * The TripOverviewResponseDTO model constructor.
     * @property {module:model/TripOverviewResponseDTO}
     */
    TripOverviewResponseDTO: TripOverviewResponseDTO,
    /**
     * The TripPlannerRequestDTO model constructor.
     * @property {module:model/TripPlannerRequestDTO}
     */
    TripPlannerRequestDTO: TripPlannerRequestDTO,
    /**
     * The TripPlannerResponseDTO model constructor.
     * @property {module:model/TripPlannerResponseDTO}
     */
    TripPlannerResponseDTO: TripPlannerResponseDTO,
    /**
     * The TripRequestDTO model constructor.
     * @property {module:model/TripRequestDTO}
     */
    TripRequestDTO: TripRequestDTO,
    /**
     * The TripResponseDTO model constructor.
     * @property {module:model/TripResponseDTO}
     */
    TripResponseDTO: TripResponseDTO,
    /**
     * The TripUserResponseDTO model constructor.
     * @property {module:model/TripUserResponseDTO}
     */
    TripUserResponseDTO: TripUserResponseDTO,
    /**
     * The UserDTO model constructor.
     * @property {module:model/UserDTO}
     */
    UserDTO: UserDTO,
    /**
     * The UserMainPageEntryDTO model constructor.
     * @property {module:model/UserMainPageEntryDTO}
     */
    UserMainPageEntryDTO: UserMainPageEntryDTO,
    /**
     * The UserProfileDTO model constructor.
     * @property {module:model/UserProfileDTO}
     */
    UserProfileDTO: UserProfileDTO,
    /**
     * The UserSearchDTO model constructor.
     * @property {module:model/UserSearchDTO}
     */
    UserSearchDTO: UserSearchDTO,
    /**
     * The AuthenticationApi service constructor.
     * @property {module:api/AuthenticationApi}
     */
    AuthenticationApi: AuthenticationApi,
    /**
     * The DiaryApi service constructor.
     * @property {module:api/DiaryApi}
     */
    DiaryApi: DiaryApi,
    /**
     * The LocationsApi service constructor.
     * @property {module:api/LocationsApi}
     */
    LocationsApi: LocationsApi,
    /**
     * The PictureApi service constructor.
     * @property {module:api/PictureApi}
     */
    PictureApi: PictureApi,
    /**
     * The PlannerApi service constructor.
     * @property {module:api/PlannerApi}
     */
    PlannerApi: PlannerApi,
    /**
     * The RatingApi service constructor.
     * @property {module:api/RatingApi}
     */
    RatingApi: RatingApi,
    /**
     * The SearchApi service constructor.
     * @property {module:api/SearchApi}
     */
    SearchApi: SearchApi,
    /**
     * The UserInfoApi service constructor.
     * @property {module:api/UserInfoApi}
     */
    UserInfoApi: UserInfoApi,
    /**
     * The UserUpdateApi service constructor.
     * @property {module:api/UserUpdateApi}
     */
    UserUpdateApi: UserUpdateApi
  };

  return exports;
}));

},{"./ApiClient":1,"./api/AuthenticationApi":2,"./api/DiaryApi":3,"./api/LocationsApi":4,"./api/PictureApi":5,"./api/PlannerApi":6,"./api/RatingApi":7,"./api/SearchApi":8,"./api/UserInfoApi":9,"./api/UserUpdateApi":10,"./model/CoordinatesResponseDTO":11,"./model/DiaryCommentRequestDTO":12,"./model/DiaryCommentResponseDTO":13,"./model/DiaryCoordinatesElementDTO":14,"./model/DiaryCoordinatesRequestDTO":15,"./model/DiaryElementsResponseListDTO":16,"./model/DiaryEntryDTO":17,"./model/DiaryPictureElementDTO":18,"./model/DiaryRequestDTO":19,"./model/DiaryResponseDTO":20,"./model/DiaryTextElementDTO":21,"./model/DiaryTextRequestDTO":22,"./model/ErrorResponse":23,"./model/LocationCoordinateDTO":24,"./model/LocationDTO":25,"./model/LocationResponseDTO":26,"./model/LocationSearchDTO":27,"./model/LoginRequestDTO":28,"./model/LoginResponseDTO":29,"./model/MainSearchDTO":30,"./model/ProfileLocationResponseDTO":31,"./model/ProfileTagDTO":32,"./model/ProfileTagResponseDTO":33,"./model/ProfileTextDTO":34,"./model/ProfileTextResponseDTO":35,"./model/PushNotificationDTO":36,"./model/RatingCategoryRequestDTO":37,"./model/RatingCategoryResponseDTO":38,"./model/TripActivityRequestDTO":39,"./model/TripActivityResponseDTO":40,"./model/TripOverviewResponseDTO":41,"./model/TripPlannerRequestDTO":42,"./model/TripPlannerResponseDTO":43,"./model/TripRequestDTO":44,"./model/TripResponseDTO":45,"./model/TripUserResponseDTO":46,"./model/UserDTO":47,"./model/UserMainPageEntryDTO":48,"./model/UserProfileDTO":49,"./model/UserSearchDTO":50}]},{},[]);
