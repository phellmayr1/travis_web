// Reload page if back button is pressed
window.onpageshow = function (event) {
    if (event.persisted) {
        window.location.reload();
    }
};

/**
 * Main URL to the REST Server
 */
window.SERVER_URL = "http://travis-sfraungruber.rhcloud.com:80";

/**
 * Name of the key that holds the session ID in session storage
 * @type {string}
 */
window.SESSION_ID = "travisSessionID";

/**
 * Name of the key that holds the user's ID in session storage
 * @type {string}
 */
window.USER_ID = "travisUserID";

/**
 * Name of the key that holds the user's first name in session storage
 * @type {string}
 */
window.USER_FIRST_NAME = "travisUserFirstName";

/**
 * Name of the key that holds the user's last name in session storage
 * @type {string}
 */
window.USER_LAST_NAME = "travisUserLastname";

/**
 * Name of the key that holds the user's travis id in session storage
 * @type {string}
 */
window.USER_ME_ID = "travisUserMeID";

/**
 * Truncates a text to a text with the specified maximum length 'n' and appends a horizontal ellipsis.
 */
String.prototype.trunc = String.prototype.trunc ||
    function (n) {
        return (this.length > n) ? this.substr(0, n - 1) + '\u2026' : this;
    };

/**
 * Gets the value of a GET parameter out of the url.
 *
 * @param name The name of the parameter
 * @param url The url
 * @returns {*} The value of the parameter
 */
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/**
 * Checks, if a given string is null or contains only whitespaces.
 *
 * @param str The string that should be checked.
 * @returns {boolean} True, if empty or contains only spaces, otherwise false.
 */
function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}